import React, { useState, useEffect } from 'react';

export default (value, timeout = 500) => {
   const [state, setState] = useState(value);

   useEffect(() => {
      const handler = setTimeout(() => setState(value), timeout);

      return () => clearTimeout(handler);
   }, [value, timeout]);

   return state;
}

// how to use ?
// import React, { useEffect } from 'react';
// import useDebounce from '/path/to/useDebounce';

// const App = (props) => {
//     const [state, setState] = useState({title: ''});    
//     const debouncedTitle = useDebounce(state.title, 1000);

//     useEffect(() => {
//         // do whatever you want with state.title/debouncedTitle
//     }, [debouncedTitle]);        

//     return (
//         // ...
//     );
// }
// ...
//https://stackoverflow.com/questions/54666401/how-to-use-throttle-or-debounce-with-react-hook
// https://stackoverflow.com/questions/41210867/react-native-using-lodash-debounce