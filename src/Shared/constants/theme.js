import { Dimensions } from "react-native";
const { width, height } = Dimensions.get("window");

export const COLORS = {
    // base colors
    // primary: "#12B2B3",
    primary: '#e97311',
    secondary: "#d0f0ef",

    border: '#CCD0D5',
    // colors
    black: "#000",
    white: "#FFFFFF",
    blue: '#97B2DE',
    background: '#Fbfbfb',
    grayblue: '#9393AA',
    blue: '#D6E8EE',
    text: '#015C92',
    orange: '#ffdd94'
};

export const SIZES = {
    // global sizes
    base: 8,
    font: 14,
    radius: 16,
    padding: 12,

    // font sizes
    largeTitle: 30,
    h1: 24,
    h2: 22,
    h3: 20,
    h4: 16,
    h5: 15,
    body1: 14,
    body2: 12,

    // app dimensions
    width,
    height
};

export const FONTS = {
    largeTitle: { fontFamily: "Mulish-Black", fontSize: SIZES.largeTitle, lineHeight: 55 },
    h1: { fontFamily: "Mulish-Black", fontSize: SIZES.h1, lineHeight: 40, fontWeight: '600' },
    h2: { fontFamily: "Mulish-Bold", fontSize: SIZES.h2, lineHeight: 38, fontWeight: '700' },
    h3: { fontFamily: "Mulish-Bold", fontSize: SIZES.h3, lineHeight: 36, fontWeight: '700' },
    h4: { fontFamily: "Mulish-Bold", fontSize: SIZES.h4, lineHeight: 36, fontWeight: '600' },
    h5: { fontFamily: "Mulish-Medium", fontSize: SIZES.h5, lineHeight: 26, fontWeight: '600' },
    body1: { fontFamily: "Mulish-Regular", fontSize: SIZES.body1, lineHeight: 24 },
    body2: { fontFamily: "Mulish-Regular", fontSize: SIZES.body2, lineHeight: 16 },
};

const appTheme = { COLORS, SIZES, FONTS };

export default appTheme;
