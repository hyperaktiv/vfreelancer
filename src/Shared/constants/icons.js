export const icon_home = require("../../../assets/icons/icon_home.png");
export const icon_search = require("../../../assets/icons/icon_search.png");
export const icon_search2 = require("../../../assets/icons/icon_search2.png");
export const icon_menu = require("../../../assets/icons/icon_menu.png");
export const icon_plus = require("../../../assets/icons/icon_plus.png");
export const icon_notification = require("../../../assets/icons/icon_notification.png");
export const icon_like = require("../../../assets/icons/icon_like.png");
export const icon_clock = require("../../../assets/icons/icon_clock.png");
export const icon_bookmark = require("../../../assets/icons/icon_bookmark.png");
export const icon_back = require("../../../assets/icons/icon_back.png");
export const icon_more = require("../../../assets/icons/icon_more.png");
export const icon_send = require("../../../assets/icons/icon_send.png");
export const icon_edit = require("../../../assets/icons/icon_edit.png");
export const icon_setting = require("../../../assets/icons/icon_setting.png");
export const icon_about = require("../../../assets/icons/icon_about.png");
export const icon_mode = require("../../../assets/icons/icon_mode.png");
export const icon_privacy = require("../../../assets/icons/icon_privacy.png");
export const icon_forward = require("../../../assets/icons/icon_forward.png");
export const icon_help = require("../../../assets/icons/icon_help.png");
export const icon_checked = require("../../../assets/icons/icon_checked.png");
export const icon_share = require("../../../assets/icons/icon_share.png");
export const icon_comment = require("../../../assets/icons/icon_comment.png");
export const icon_message = require("../../../assets/icons/icon_message.png");
export const icon_plus2 = require("../../../assets/icons/icon_plus2.png");
export const icon_trash = require("../../../assets/icons/icon_trash.png");
export const icon_minus = require("../../../assets/icons/icon_minus.png");
export const icon_close = require("../../../assets/icons/icon_close.png");
export const icon_gallery = require("../../../assets/icons/icon_gallery.png");
export const icon_link = require("../../../assets/icons/icon_link.png");
export const icon_signout = require("../../../assets/icons/icon_signout.png");
export const icon_statistics = require("../../../assets/icons/icon_statistics.png");
export const icon_appointment = require("../../../assets/icons/icon_appointment.png");
export const icon_payment = require("../../../assets/icons/icon_payment.png");
export const icon_location = require("../../../assets/icons/icon_location.png");
export const icon_star = require("../../../assets/icons/icon_star.png");
export const icon_filter = require("../../../assets/icons/icon_filter.png");
export const icon_save_money = require("../../../assets/icons/icon_save_money.png");
export const icon_pie_chart = require("../../../assets/icons/icon_pie_chart.png");
export const icon_effective = require("../../../assets/icons/icon_effective.png");
export const icon_time_money = require("../../../assets/icons/icon_time_money.png");
export const icon_job = require("../../../assets/icons/icon_job.png");
export default {
    icon_home,
    icon_search,
    icon_menu,
    icon_plus,
    icon_notification,
    icon_clock,
    icon_like,
    icon_bookmark,
    icon_more,
    icon_back,
    icon_send,
    icon_edit,
    icon_setting,
    icon_about,
    icon_mode,
    icon_privacy,
    icon_forward,
    icon_help,
    icon_checked,
    icon_share,
    icon_comment,
    icon_message,
    icon_plus2,
    icon_search2,
    icon_trash,
    icon_minus,
    icon_close,
    icon_gallery,
    icon_link,
    icon_signout,
    icon_statistics,
    icon_appointment,
    icon_payment,
    icon_location,
    icon_star,
    icon_filter,
    icon_save_money,
    icon_pie_chart,
    icon_effective,
    icon_time_money,
    icon_job
}