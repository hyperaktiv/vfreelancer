export const image_onboarding1 = require("../../../assets/images/image_onboarding1.jpg");
export const logo = require("../../../assets/images/logo.png");
export const google = require("../../../assets/images/google.png");
export const facebook = require("../../../assets/images/facebook.png");
export const logo_name = require("../../../assets/images/logo_name.png");
export default {
    image_onboarding1,
    logo,
    google,
    facebook,
    logo_name
}