import React from 'react'
import { Image, TouchableOpacity } from 'react-native'
import { COLORS } from './constants';

const ButtonIcon = (size = 35, iconName = '') => {
   return (
      <TouchableOpacity style={{
         width: 50,
         height: 50,
         borderRadius: 10,
         borderColor: COLORS.border,
         backgroundColor: 'gray'
      }}>
         <Image
            style={{ width: size, height: size }}
            source={iconName}
         />
      </TouchableOpacity>
   );
}

export default ButtonIcon
