import React from 'react';
import {
    TouchableOpacity,
    ActivityIndicator
} from 'react-native';
import { COLORS } from './constants';

const ButtonLoading = (props) => {
    return (
        <TouchableOpacity 
            // onPress={props.onPress}
            style={[{
                backgroundColor: COLORS.primary,
                height: 50,
                borderRadius: 12,
                alignItems: 'center',
                justifyContent: 'center',
                marginVertical:20
            }, { ...props.style }]}>
                
                        <ActivityIndicator animating={true} size="large" color="#fff"/>
                   
                
            
        </TouchableOpacity>
    )
}
export default ButtonLoading;