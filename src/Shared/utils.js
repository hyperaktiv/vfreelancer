
import moment from "moment";
const formatDate = (dateString) => {
   return moment(dateString).format('DD/MM/YYYY');
};
const formatDateAndTime = (dateString) => {
   return moment(dateString).format('h:mm [ngày] DD/MM');
}
const formatTime = (dateString) => {
   return moment(dateString).format('h:mm');
}
const relativeTime = (date) => {
   return moment(date).fromNow();
}

const getDataFromDoc = (res) => {
   const data = res.data();
   data.id = res.id;
   return data;
}
const getDataFromDocs = (res) => {
   return res.map(getDataFromDoc);
}

const capitalizeFirstLetter = (string) => {
   return string.charAt(0).toUpperCase() + string.slice(1);
}
/**
 * usage ?
 * - import 
 * 
 *    const responses = await firebase.firestore()
 *       .collection('conversations').where('users', 'not-in', 
 *          ['1stgoddeath@gmail.com', 'hyperaktiv99@gmail.com'])
 *       .get();
 *    const users = getDataFromDocs(responses.docs);
 */


export { getDataFromDocs, formatDate, relativeTime, formatDateAndTime, formatTime, capitalizeFirstLetter }