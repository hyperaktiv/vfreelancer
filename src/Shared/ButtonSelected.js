import React from 'react';
import {
    View,
    Text,
    TouchableOpacity
} from 'react-native';
import  Icon  from 'react-native-vector-icons/FontAwesome';
import { COLORS, FONTS } from './constants';

const ButtonSelected = (props) => {
    return (
        <TouchableOpacity onPress={props.onPress}
        style={{
            backgroundColor:'#fff' ,
            marginTop:20,
            height:50,
            borderRadius:12,
            alignItems:'center',
            // justifyContent:'center',
            flexDirection:'row',
            borderColor:props.checked ? COLORS.primary : COLORS.border,
            borderWidth:1
        }}> 
            
            <Icon name={props.checked ? "check-circle" : "circle"} color={props.checked ? COLORS.primary : COLORS.grayblue} style={{ marginHorizontal: 20}} size={20}/>
            <Text style={{...FONTS.body1,color:COLORS.black}}>{props.text}</Text>
            
        </TouchableOpacity>
    )
}
export default ButtonSelected;