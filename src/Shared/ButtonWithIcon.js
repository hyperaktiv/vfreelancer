import React from 'react';
import {
    Text,
    TouchableOpacity
} from 'react-native';
import  Icon  from 'react-native-vector-icons/FontAwesome';
import { COLORS, FONTS } from './constants';

const ButtonWithIcon = (props) => {
    return (
        <TouchableOpacity onPress={props.onPress}
        style={{
            backgroundColor:COLORS.primary,
            marginVertical:20,
            height:50,
            borderRadius:12,
            alignItems:'center',
            justifyContent:'center',
            flexDirection:'row'
        }}> 
            
            <Text style={{...FONTS.body1,color:COLORS.white}}>{props.text}</Text>
            <Icon name={props.iconName} color={COLORS.white} style={{ marginLeft: 20}} size={20}/>
        </TouchableOpacity>
    )
}
export default ButtonWithIcon;