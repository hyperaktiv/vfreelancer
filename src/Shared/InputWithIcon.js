import React from 'react';
import { TextInput, View, Text } from 'react-native';
import { COLORS, FONTS } from './constants';
import Icon from 'react-native-vector-icons/FontAwesome';

const InputWithIcon = (props) => {

    return (
        <View>
            <Text style={{ ...FONTS.body1, color: COLORS.black, marginVertical: 3 }}>{props.label}</Text>
            <View style={{
                flexDirection:'row',
                alignItems:'center',
                borderRadius: 8,
                borderColor: '#F6f6f6',
                borderWidth:1,
                height: 50,
                backgroundColor:'#fff'
            }}>
                <Icon
                    name={props.iconName}
                    size={20}
                    color={COLORS.primary}
                    style={{
                        marginLeft: 10
                    }}
                />
                <TextInput
                placeholder={props.placeholder}
                onChangeText={props.onChangeText}
                secureTextEntry={props.secureTextEntry}
                keyboardType={props.keyboardType}
                value={props.value}
                placeholderTextColor={COLORS.grayblue}
                style={{
                    paddingLeft: 25,
                    ...FONTS.body1,
                    width:310
                }}
            />
                
            </View>

        </View>
    )
}
export default InputWithIcon;