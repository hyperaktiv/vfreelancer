import React from 'react';
import { TextInput, View, Text } from 'react-native';
import { COLORS, FONTS, SIZES } from './constants';

const Input = (props) => {
    return (
        <View>
            <Text style={{ ...FONTS.h4, color: COLORS.black, marginVertical: 3 }}>{props.label}</Text>
            <TextInput
                underlineColorAndroid="transparent"
                autoCapitalize="none"
                editable={props.editable}
                placeholder={props.placeholder}
                onChangeText={props.onChangeText}
                secureTextEntry={props.secureTextEntry}
                value={props.value}
                placeholderTextColor={COLORS.grayblue}
                keyboardType={props.keyboardType}

                style={{
                    borderRadius: 8,
                    borderColor: COLORS.border,
                    borderWidth: 1,
                    height: 50,
                    paddingHorizontal: 20,
                    backgroundColor: '#fff',
                    ...FONTS.body1
                }}
            />
        </View>
    )
}
export default Input;