import React from 'react';
import { Text } from 'react-native';
import { FONTS } from './constants/theme';

const CustomText = (props) => {

   let style = { ...FONTS.body1 };
   if (props.h1) style = { ...FONTS.h1 };
   if (props.h2) style = { ...FONTS.h2 };
   if (props.h3) style = { ...FONTS.h3 };
   if (props.h4) style = { ...FONTS.h4 };
   if (props.body2) style = { ...FONTS.body2 };

   return (
      <Text style={[style, {
         ...props.style
      }]}>{props.children}</Text>
   )
}

export default CustomText
