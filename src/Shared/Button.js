import React from 'react';
import {
    Text,
    TouchableOpacity,
    StyleSheet
} from 'react-native';
import { COLORS, FONTS } from './constants';

const Button = (props) => {
    return (
        <TouchableOpacity onPress={props.onPress}
            style={[styles.btn, { ...props.style }]}>
            <Text style={{ ...FONTS.h5, color: COLORS.white }}>{props.text}</Text>
        </TouchableOpacity>
    )
}
const styles  = StyleSheet.create({
    btn:{
        backgroundColor: COLORS.primary,
        height: 50,
        borderRadius: 12,
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 20
    }
})
export default Button;