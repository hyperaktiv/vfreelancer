import React from 'react'
import { View, Image, TouchableOpacity } from 'react-native';
import { COLORS, SIZES } from './constants/theme';
import CustomText from './CustomText';

const Header = ({ title = '', returnAction = false, style }) => {
   return (
      <View style={[{
         marginTop: 20,
         marginHorizontal: SIZES.padding,
         flexDirection: 'row',
         justifyContent: 'space-between',
         alignItems: 'center',
         borderBottomWidth: 1,
         borderColor: COLORS.border,
      }, { ...style }]}>
         {returnAction == true ? (
            <TouchableOpacity
               onPress={() => navigation.goBack()}
               style={{
                  width: 40,
                  height: 40,
                  borderColor: COLORS.grayblue,
                  borderWidth: 1,
                  borderRadius: 12,
                  justifyContent: 'center',
                  alignItems: 'center'
               }}>
               <Image style={{ width: 20, height: 20, tintColor: COLORS.primary }}
                  source={icon_back} resizeMode="contain" />
            </TouchableOpacity>
         ) : (<View style={{ width: 40, height: 40 }} />)}

         <View style={{}}>
            <CustomText h2 style={{ fontWeight: '700' }}>{title}</CustomText>
         </View>
         <View style={{
            width: 40,
            height: 40,
         }} />
      </View>
   )

}

export default Header
