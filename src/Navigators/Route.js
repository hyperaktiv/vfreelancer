import React, { useContext, useState, useEffect } from 'react';
import { LogBox } from 'react-native';
import { useFonts } from 'expo-font';
import { NavigationContainer, DefaultTheme } from '@react-navigation/native';
import AuthStack from './AuthStack';
import AppStack from './AppStack';
import { auth } from '../firebase';
import { AuthContext } from './AuthProvider';
import Toast from 'react-native-toast-message';
// LogBox.ignoreAllLogs(true);
LogBox.ignoreLogs(['Setting a timer']);
LogBox.ignoreLogs(['VirtualizedLists should never']);
const theme = {
    ...DefaultTheme,
    colors: {
        ...DefaultTheme.colors,
        border: "transparent"
    }
}
const Route = () => {
    const { user, setUser } = useContext(AuthContext);
    const [initializing, setInitializing] = useState(true);

    function onChange(user) {
        setUser(user);
        setInitializing(false)
    }
    // https://firebase.google.com/docs/auth/web/manage-users
    useEffect(() => {
        const unsubscribe = auth.onAuthStateChanged(onChange)

        // unsubscribe to the listener when unmounting
        return () => unsubscribe()
    }, [])

    const [loaded] = useFonts({
        "Mulish-Black": require('../../assets/fonts/Mulish-Black.ttf'),
        "Mulish-Bold": require('../../assets/fonts/Mulish-Bold.ttf'),
        "Mulish-Regular": require('../../assets/fonts/Mulish-Regular.ttf'),
        "Mulish-Light": require('../../assets/fonts/Mulish-Light.ttf'),
        "Mulish-Italic": require('../../assets/fonts/Mulish-Italic.ttf'),
        "Mulish-Medium": require('../../assets/fonts/Mulish-Medium.ttf'),
    })
    if (!loaded) {
        return null;
    }
    if (initializing) {
        return null;
    }
    return (
        <NavigationContainer theme={theme}>
            {
                user ? <AppStack /> : <AuthStack />
            }
            <Toast ref={(ref) => Toast.setRef(ref)} />
        </NavigationContainer>
    )
}
export default Route;