import React, { useContext } from 'react';
import { createStackNavigator } from '@react-navigation/stack';

//screen
import Tabs from './Tabs';
import NotifyScreen from '../screens/notification/NotifyScreen';
import Conversation from '../screens/message/Conversation';
import JobDetailsScreen from '../screens/jobdetail/JobDetailsScreen';
import Payment from '../screens/setting/Payment';
import UserProfileScreen from '../screens/profile/UserProfileScreen';
import EditProfileScreen from '../screens/profile/EditProfileScreen/EditProfileScreen';
import BasicInformation from '../screens/auth/BasicInformation';
import { AuthContext } from './AuthProvider';
import OtherInformation from '../screens/auth/OtherInformation';
import SignUpSuccessful from '../screens/auth/SignUpSuccessful';
import NewChat from '../screens/message/NewChat';
import ProjectManager from '../screens/setting/ProjectManager';
import User from '../screens/profile/User';
const Stack = createStackNavigator();

const AppStack = () => {
    const { user } = useContext(AuthContext);
    return (
        <Stack.Navigator
            screenOptions={{
                headerShown: false,
            }}
            initialRouteName={user.displayName != null ? 'Home' : 'Basicinformation'}
        >
            <Stack.Screen name="Home" component={Tabs} />
            <Stack.Screen name="Notification" component={NotifyScreen} />
            <Stack.Screen name="Conversation" component={Conversation} />
            <Stack.Screen name="Jobdetails" component={JobDetailsScreen} />
            <Stack.Screen name="User" component={User} />
            <Stack.Screen name="Payment" component={Payment} />
            <Stack.Screen name="Userprofile" component={UserProfileScreen} />
            <Stack.Screen name="EditProfileScreen" component={EditProfileScreen} />
            <Stack.Screen name="ProjectManager" component={ProjectManager} />
            <Stack.Screen name="Basicinformation" component={BasicInformation} />
            <Stack.Screen name="Otherinformation" component={OtherInformation} />
            <Stack.Screen name="Signupsuccessful" component={SignUpSuccessful} />
            <Stack.Screen name="Newchat" component={NewChat} />
        </Stack.Navigator>
    )
}
export default AppStack;