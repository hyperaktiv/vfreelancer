import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import JobUpload from '../screens/jobupload/JobUpload';
import JobUploadStep1 from '../screens/jobupload/JobUploadStep1';
import JobUploadStep2 from '../screens/jobupload/JobUploadStep2';
import JobUploadStep3 from '../screens/jobupload/JobUploadStep3';
const Stack = createStackNavigator();
const JobUploadStack = () => {
    return (
        <Stack.Navigator
            screenOptions={{
                headerShown: false
            }}
            initialRouteName="JobUploadStep1"
        >
            <Stack.Screen name="JobUpload" component={JobUpload} />
            <Stack.Screen name="JobUploadStep1" component={JobUploadStep1} />
            <Stack.Screen name="JobUploadStep2" component={JobUploadStep2} />
            <Stack.Screen name="JobUploadStep3" component={JobUploadStep3} />
           
        </Stack.Navigator>
    )
}
export default JobUploadStack;