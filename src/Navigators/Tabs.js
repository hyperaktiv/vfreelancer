import React, { useEffect } from 'react';
import {
    Image,
    View,
    StyleSheet
} from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { COLORS, icons } from '../shared/constants';
//screen
import Home from '../screens/home/Home';
import Setting from '../screens/setting/Setting';
import Message from '../screens/message/Message';
import StatisticScreen from '../screens/statistic/StatisticScreen';
import JobUploadStack from './JobUploadStack';
const Tab = createBottomTabNavigator();
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useState } from 'react';
const tabBarOptions = {
    keyboardHidesTabBar: true,
    showLabel: false,
    style: {
        position: 'absolute',
        backgroundColor: '#ffffff',
        bottom: 0,
        left: 0,
        right: 0,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 10,
        },
        paddingTop:10,
        shadowOpacity: 0.25,
        shadowRadius: 4.5,
        elevation: 7,
        borderTopLeftRadius: 16,
        borderTopRightRadius: 16,
    }
}

const Tabs = () => {
    const [role, setRole] = useState(null);
    const load = async () => {
        const user_role_string = await AsyncStorage.getItem("user_role");
        setRole(user_role_string != null ? JSON.parse(user_role_string) : null);
    }
    useEffect(() => {
        load();
    });

    return (
        <Tab.Navigator
            tabBarOptions={tabBarOptions}
        >
            <Tab.Screen
                name="Home"
                component={Home}
                options={{
                    tabBarIcon: ({ focused }) => (
                        <View style={focused ? styles.btn_focus : styles.btn_un_focus}>
                            <Image
                                source={icons.icon_home}
                                resizeMode="contain"
                                style={focused ? styles.image_focus : styles.image_un_focus}
                            />
                        </View>
                    )
                }}
            />
            <Tab.Screen
                name="Message"
                component={Message}
                options={{
                    tabBarIcon: ({ focused }) => (
                        <View style={focused ? styles.btn_focus : styles.btn_un_focus}>
                            <Image
                                source={icons.icon_message}
                                resizeMode="contain"
                                style={focused ? styles.image_focus : styles.image_un_focus}
                            />
                        </View>
                    )
                }}
            />
            {
                role == 1 && (
                    <Tab.Screen
                        name="JobUploadStep1"
                        component={JobUploadStack}
                        options={{
                            tabBarIcon: ({ focused }) => (
                                <View style={focused ? styles.btn_focus : styles.btn_un_focus}>
                                    <Image
                                        source={icons.icon_plus2}
                                        resizeMode="contain"
                                        style={focused ? styles.image_focus : styles.image_un_focus}
                                    />
                                </View>
                            )
                        }}
                    />
                )
            }

            <Tab.Screen
                name="Static"
                component={StatisticScreen}
                options={{
                    tabBarIcon: ({ focused }) => (
                        <View style={focused ? styles.btn_focus : styles.btn_un_focus}>
                            <Image
                                source={icons.icon_statistics}
                                resizeMode="contain"
                                style={focused ? styles.image_focus : styles.image_un_focus}

                            />
                        </View>
                    )
                }}
            />
            <Tab.Screen
                name="Setting"
                component={Setting}
                options={{
                    tabBarIcon: ({ focused }) => (
                        <View style={focused ? styles.btn_focus : styles.btn_un_focus}>
                            <Image
                                source={icons.icon_setting}
                                resizeMode="contain"
                                style={focused ? styles.image_focus : styles.image_un_focus}
                            />
                        </View>
                    )
                }}
            />
        </Tab.Navigator>
    )
}
export default Tabs;
const styles = StyleSheet.create({
    btn_focus: {
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 12,
        width: 45,
        height: 45,
        backgroundColor: COLORS.orange
    },
    btn_un_focus: {
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 12,
        width: 45,
        height: 45,
        backgroundColor: '#FFF'
    },
    image_focus: {
        tintColor: COLORS.primary,
        width: 22,
        height: 22
    },
    image_un_focus: {
        tintColor: COLORS.grayblue,
        width: 22,
        height: 22
    }
})