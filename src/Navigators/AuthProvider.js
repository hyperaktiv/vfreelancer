import React, { createContext, useState } from 'react';
import Toast from 'react-native-toast-message';
import { auth } from '../firebase';
import * as firebase from 'firebase';
import AsyncStorage from '@react-native-async-storage/async-storage';
//tạo context lưu trữ user
export const AuthContext = createContext();
export const AuthProvider = ({ children }) => {
    const [user, setUser] = useState(null);
    return (
        //provider cung cấp các giá trị lấy từ context
        <AuthContext.Provider
            value={{
                user,
                setUser,
                login: async (email, password) => {
                    try {
                        let array = [];
                        await auth.signInWithEmailAndPassword(email, password)
                            .then(async () => {
                                await firebase.firestore()
                                    .collection('users')
                                    .where('userId', '==', firebase.auth().currentUser.uid)
                                    .get()
                                    .then(async query => {
                                        query.forEach(doc => {
                                            const { role } = doc.data();
                                            array.push({
                                                role
                                            });
                                        })
                                        await AsyncStorage.setItem('user_role', JSON.stringify(array[0].role));
                                        Toast.show({
                                            topOffset: 30,
                                            type: 'success',
                                            text1: 'Đăng nhập thành công!'
                                        });
                                    })
                            })
                    } catch (e) {
                        Toast.show({
                            topOffset: 30,
                            type: 'error',
                            text1: 'Đăng nhập thất bại !',
                            text2: `${e}`
                        });
                    }
                },
                //đăng ký bằng email và password, còn thông tin thì thêm vào bảng khác
                register: async (email, password, role) => {
                    try {
                        await auth.createUserWithEmailAndPassword(email, password)
                            .then(() => {
                                firebase.firestore()
                                    .collection('users')
                                    .add({
                                        userId: firebase.auth().currentUser.uid,
                                        email,
                                        role
                                    })
                                    .then(async (doc) => await AsyncStorage.setItem('new_user_id', JSON.stringify(doc.id)));
                                Toast.show({
                                    topOffset: 30,
                                    type: 'success',
                                    text1: 'Đăng ký thành công!'
                                });
                            })
                            .catch(error => {
                                Toast.show({
                                    topOffset: 30,
                                    type: 'error',
                                    text1: 'Đăng ký thất bại !',
                                    text2: `${error}`
                                });
                            });
                    } catch (e) {
                        console.log(e);
                    }
                },
                logout: async () => {
                    try {
                        await auth.signOut()
                            .then(async () => {
                                await AsyncStorage.removeItem('user_role');
                                Toast.show({
                                    topOffset: 30,
                                    type: 'success',
                                    text1: 'Logout successful !',
                                });
                            });
                    } catch (e) {
                        console.log(e)
                    }
                }
            }}
        >
            {children}
        </AuthContext.Provider>
    )
}