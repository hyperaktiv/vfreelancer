import React, { useState, useEffect } from 'react';
import {
   View,
   ScrollView,
   TouchableOpacity,
   Text,
   SafeAreaView,
   Image,
   TextInput,
   KeyboardAvoidingView,
   Dimensions,
   ActivityIndicator
} from 'react-native';
import Button from '../../../shared/Button';
import Input from '../../../shared/Input';
import { COLORS, SIZES, icons, FONTS } from '../../../shared/constants';
import CustomText from '../../../shared/CustomText';

import MultiSelect from 'react-native-multiple-select';
import * as firebase from 'firebase';
import { auth, firestore } from 'firebase';
import Toast from 'react-native-toast-message';
import * as ImagePicker from 'expo-image-picker';

var width = Dimensions.get("window").width;

const EditProfileScreen = (props) => {

   const [userData, setUserData] = useState(props.route.params.userDetails);
   const [skillItems, setSkillItems] = useState([]);

   const [selectedSkills, setSelectedSkills] = useState(userData?.skill.map(({ id }) => id));
   const [name, setName] = useState(userData?.name);
   const [address, setAddress] = useState(userData?.address);
   const [dateOfBirth, setDateOfBirth] = useState(userData?.dateOfBirth);
   const [introduction, setIntroduction] = useState(userData?.description?.introduction);
   const [education, setEducation] = useState(userData?.description?.education);
   const [experience, setExperience] = useState(userData?.description?.experience);
   const [degree, setDegree] = useState(userData?.description?.degree);
   const [userImage, setUserImage] = useState(userData?.userImage);
  
   //loading when click login button
   const [isLoading, setIsLoading] = useState(false);

   const choosePhotoFromLibrary = async () => {
      let result = await ImagePicker.launchImageLibraryAsync({
         mediaTypes: ImagePicker.MediaTypeOptions.All,
         allowsEditing: true,
         aspect: [1, 1],
         quality: 1,
      });
      if (!result.cancelled) {
         console.log(result.uri);
         setUserImage(result.uri);
      }
   };
   const handleUpdateProfile = async () => {
      if (userImage == null) {
         onSaveData(userImage);
      } else {
         const blob = await new Promise((resolve, reject) => {
            const xhr = new XMLHttpRequest();
            xhr.onload = function () {
               resolve(xhr.response);
            };
            xhr.onerror = function () {
               reject(new TypeError('Network request failed'));
            };
            xhr.responseType = 'blob';
            xhr.open('GET', userImage, true);
            xhr.send(null);
         });
         const ref = firebase.storage().ref().child(new Date().toISOString())
         const snapshot = ref.put(blob);
         snapshot.on(
            firebase.storage.TaskEvent.STATE_CHANGED,
            () => {
               setIsLoading(true)
            },
            (error) => {
               setIsLoading(false);
               blob.close();
               return null;
            },
            () => {
               snapshot.snapshot.ref.getDownloadURL().then((url) => {
                  setIsLoading(false);
                  onSaveData(url);
                  blob.close();
                  return url;
               });
            });

      }
   }
   const onSaveData = async (url) => {

      const userSelectSkills = skillItems.filter(item => selectedSkills.includes(item.id));
      const user = {
         userImage: url,
         name,
         address,
         dateOfBirth,
         description: {
            introduction,
            education,
            degree,
            experience
         },
         skill: userSelectSkills,
      };
      let docID = userData.id;
      await firebase.firestore()
         .collection('users')
         .doc(docID)
         .update(user)
         .then(async () => {
            firebase.auth().currentUser.updateProfile({
               photoURL : url,
               displayName: name
            });
            Toast.show({
               topOffset: 30,
               type: 'success',
               text1: 'Update thành công !'
            });

            props.navigation.replace("Userprofile");
         })
   }
   const onSelectedItemsChange = (chooseSkills) => {
      // Set Selected Items
      setSelectedSkills(chooseSkills);
   };
   const getSkillData = async () => {
      const skills = await firebase.firestore().collection('Children_skill');
      skills.get().then(querySnapshot => {
         const tempDoc = querySnapshot.docs.map((doc) => {
            return { id: doc.id, ...doc.data() }
         })
         setSkillItems(tempDoc);
      });
   }

   useEffect(() => {
      setIsLoading(true);
      getSkillData();
      setIsLoading(false);

      return () => {
         setSelectedSkills([]);
         setSkillItems([]);
      }
   }, []);

   if (isLoading)
      return (
         <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <ActivityIndicator size="large" color={COLORS.primary} />
         </View>
      );
   else
      return (
         <SafeAreaView style={{
            flex: 1,
            backgroundColor: COLORS.background,
         }}>
            <KeyboardAvoidingView style={{
               paddingHorizontal: SIZES.padding,
               paddingBottom: 25
            }}>

               <View style={{
                  flexDirection: 'row',
                  paddingHorizontal: SIZES.padding,
                  justifyContent: 'space-between',
                  alignItems: 'center',
               }}>
                  <TouchableOpacity
                     onPress={() => props.navigation.goBack()}
                     style={{
                        borderWidth: 1,
                        padding: 5,
                        borderColor: COLORS.grayblue,
                        borderRadius: 5
                     }}
                  >
                     <Image
                        source={icons.icon_back}
                        style={{
                           width: 20,
                           height: 20,
                           tintColor: COLORS.black
                        }}
                        resizeMode="contain"
                     />
                  </TouchableOpacity>

                  <Text style={{ ...FONTS.h3, color: COLORS.black }}>Chỉnh sửa Thông tin</Text>

                  {/**Button check to save the information */}
                  <TouchableOpacity
                     style={{
                        borderWidth: 1,
                        padding: 5,
                        borderColor: COLORS.primary,
                        borderRadius: 5
                     }}
                  >
                     <Image
                        source={icons.icon_checked}
                        style={{
                           width: 20,
                           height: 20,
                           tintColor: COLORS.primary
                        }}
                        resizeMode="contain"
                     />
                  </TouchableOpacity>
               </View>

               <ScrollView style={{
                  paddingHorizontal: SIZES.padding,
                  marginBottom: SIZES.padding,
                  borderRadius: 8,
               }}>
                  {/* change avatar image */}
                  <View style={{ marginTop: 12, }}>
                     <SectionEditTitle title={'Ảnh đại diện'} />

                     {/* button to change the avatar */}
                     <TouchableOpacity
                        onPress={() => choosePhotoFromLibrary()}
                        style={{
                           position: 'absolute',
                           zIndex: 10,
                           bottom: 2, right: 120,
                           backgroundColor: '#fff',
                           borderRadius: 10,
                           justifyContent: 'center',
                           alignItems: 'center',
                           borderWidth: 1,
                           borderColor: COLORS.border,
                        }}>
                        <Image style={{
                           width: 26, height: 26,
                           tintColor: COLORS.primary
                        }}
                           source={icons.icon_setting}
                           resizeMode='contain' />
                     </TouchableOpacity>

                     <View style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                     }}>
                        <Image style={{
                           width: 120, height: 120,
                           borderRadius: SIZES.radius + 30,
                           borderWidth: 1,
                           borderColor: COLORS.border
                        }}
                           source={{
                              uri: userImage ? userImage  :'https://randomuser.me/api/portraits/men/78.jpg'
                           }}
                           resizeMode='contain' />
                     </View>
                  </View>

                  <View style={{
                     borderBottomWidth: 1,
                     borderColor: COLORS.border,
                     marginVertical: 12
                  }} />

                  {/* change profile details */}
                  <View style={{ marginVertical: SIZES.padding }}>
                     <SectionEditTitle title={'Chi tiết'} />

                     <View style={{}}>

                        <Input
                           label="Email"
                           value={userData?.email != null ? userData.email : 'abcd@gmail.com'}
                           selectTextOnFocus={false}
                           contextMenuHidden={true}
                           editable={false}
                        />
                        <Input
                           label="Họ tên"
                           placeholder="Nhập họ tên..."
                           value={name}
                           onChangeText={(text) => setName(text)}
                        />
                        <Input
                           label="Địa chỉ"
                           placeholder="Nhập địa chỉ..."
                           defaultValue={address}
                           value={address}
                           onChangeText={(text) => setAddress(text)}
                        />
                        <Input
                           label="Ngày sinh"
                           placeholder="DD/MM/YYYY"
                           defaultValue={dateOfBirth}
                           value={dateOfBirth}
                           onChangeText={(text) => setDateOfBirth(text)}
                        />
                     </View>
                  </View>

                  <View style={{
                     borderBottomWidth: 1,
                     borderColor: COLORS.border,
                     marginVertical: 12
                  }} />

                  {/* change basic information */}
                  <View style={{ marginVertical: SIZES.padding }}>
                     <SectionEditTitle title={'Thông tin cơ bản'} />

                     <View style={{}}>
                        <Text style={{ ...FONTS.h4, color: COLORS.black, marginVertical: 3 }}>Giới thiệu bản thân</Text>
                        <TextInput
                           underlineColorAndroid="transparent"
                           numberOfLines={4}
                           multiline={true}
                           placeholder={"Giới thiệu bản thân"}
                           placeholderTextColor={COLORS.grayblue}
                           value={introduction}
                           onChangeText={(text) => setIntroduction(text)}

                           style={{
                              borderRadius: 8,
                              borderColor: COLORS.border,
                              borderWidth: 1,
                              justifyContent: "flex-start",
                              paddingHorizontal: 20,
                              backgroundColor: '#fff',
                              ...FONTS.body1,
                           }}
                        />
                     </View>

                     <View style={{}}>
                        <Text style={{ ...FONTS.h4, color: COLORS.black, marginVertical: 3 }}>Kỹ năng</Text>

                        <MultiSelect
                           items={skillItems}
                           uniqueKey="id"
                           displayKey="name"
                           onSelectedItemsChange={onSelectedItemsChange}
                           selectedItems={isLoading == true ? [] : selectedSkills}
                           selectText="Chọn kỹ năng"
                           altFontFamily={"Mulish-Regular"}
                           fontSize={14}
                           fontFamily={"Mulish-Regular"}
                           tagRemoveIconColor={COLORS.border}
                           tagBorderColor={COLORS.primary}
                           tagTextColor={COLORS.black}
                           selectedItemTextColor={COLORS.primary}
                           selectedItemIconColor={COLORS.primary}
                           itemTextColor={COLORS.black}
                           itemFontSize={14}
                           searchInputStyle={{
                              color: COLORS.black,
                              fontFamily: "Mulish-Regular",
                              fontWeight: "bold"
                           }}
                           textInputProps={{ editable: false, autoFocus: false }}
                           searchInputPlaceholderText="Chọn kỹ năng"
                           searchIcon={false}
                           submitButtonColor={COLORS.primary}
                           submitButtonText="Submit"
                        />

                     </View>
                  </View>

                  <View style={{
                     borderBottomWidth: 1,
                     borderColor: COLORS.border,
                     marginVertical: 12
                  }} />

                  {/* change experiences information */}
                  <View style={{ marginVertical: SIZES.padding }}>
                     <SectionEditTitle title={'Kinh nghiệm'} />

                     <View style={{}}>
                        <TextInput
                           underlineColorAndroid="transparent"
                           autoCapitalize="none"
                           numberOfLines={4}
                           multiline={true}
                           placeholder={"- Kinh nghiệm cá nhân"}
                           placeholderTextColor={COLORS.grayblue}
                           value={experience}
                           onChangeText={(text) => setExperience(text)}

                           style={{
                              borderRadius: 8,
                              borderColor: COLORS.border,
                              borderWidth: 1,
                              justifyContent: "flex-start",
                              paddingHorizontal: 20,
                              backgroundColor: '#fff',
                              ...FONTS.body1,
                           }}
                        />
                     </View>
                  </View>

                  <View style={{
                     borderBottomWidth: 1,
                     borderColor: COLORS.border,
                     marginVertical: 12
                  }} />

                  {/* change experiences information */}
                  <View style={{ marginVertical: SIZES.padding }}>
                     <SectionEditTitle title={'Học vấn'} />

                     <View style={{}}>
                        <Text style={{ ...FONTS.h4, color: COLORS.black, marginVertical: 3 }}>Trình độ học vấn</Text>
                        <TextInput
                           underlineColorAndroid="transparent"
                           autoCapitalize="none"
                           numberOfLines={4}
                           multiline={true}
                           placeholder={"- Trình độ học vấn, trường đại học, trung tâm,...\n(Gạch đầu dòng)"}
                           placeholderTextColor={COLORS.grayblue}
                           value={education}
                           onChangeText={(text) => setEducation(text)}

                           style={{
                              borderRadius: 8,
                              borderColor: COLORS.border,
                              borderWidth: 1,
                              justifyContent: "flex-start",
                              paddingHorizontal: 20,
                              backgroundColor: '#fff',
                              ...FONTS.body1,
                           }}
                        />
                     </View>

                     <View style={{}}>
                        <Text style={{ ...FONTS.h4, color: COLORS.black, marginVertical: 3 }}>Bằng cấp</Text>
                        <TextInput
                           underlineColorAndroid="transparent"
                           autoCapitalize="none"
                           numberOfLines={4}
                           multiline={true}
                           placeholder={"- Bằng cấp, chứng chỉ, chứng nhận,...\n(Gạch đầu dòng)"}
                           placeholderTextColor={COLORS.grayblue}
                           value={degree}
                           onChangeText={(text) => setDegree(text)}

                           style={{
                              borderRadius: 8,
                              borderColor: COLORS.border,
                              borderWidth: 1,
                              justifyContent: "flex-start",
                              paddingHorizontal: 20,
                              backgroundColor: '#fff',
                              ...FONTS.body1,
                           }}
                        />
                     </View>
                  </View>


                  {/**Button check to save the information */}
                  <ButtonIcon onPress={handleUpdateProfile} />

               </ScrollView>


            </KeyboardAvoidingView>
         </SafeAreaView>
      )
}
export default EditProfileScreen;

const SectionEditTitle = ({ title }) => {
   return (
      <View style={{ flexDirection: 'row' }}>
         <CustomText h3 style={{ textDecorationLine: 'underline' }}>{title}</CustomText>
         {/* <View style={{
            marginLeft: SIZES.padding,
            justifyContent: 'center',
            alignItems: 'center',
            borderWidth: 1,
            padding: 5,
            borderColor: COLORS.primary,
            borderRadius: 5,
            width: 30, height: 30,
         }}>
            <Image style={{
               width: 16, height: 16, tintColor: COLORS.primary
            }}
               source={icons.icon_edit}
               resizeMode='contain' />
         </View> */}
      </View>
   )
}

const ButtonIcon = ({ onPress }) => {
   return (
      <View style={{
         flexDirection: 'row',
         marginHorizontal: 24,
         alignItems: 'center',
         justifyContent: 'center',
         // position: 'absolute',
         bottom: 20,
      }}>
         <Button text='Lưu'
            style={{
               width: width * 0.6,
               backgroundColor: COLORS.primary,
               marginHorizontal: 12,
            }}
            onPress={onPress}
         />
      </View>
   )
}