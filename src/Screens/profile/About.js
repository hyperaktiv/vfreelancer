import React from 'react';
import { View } from 'react-native';
import { COLORS, SIZES } from '../../shared/constants';
import CustomText from '../../shared/CustomText';

const About = (props) => {
   const { description , skill } = props.userData;
   let array = []; 
   skill?.forEach(s=>{
      array.push(s.name);
   })
   return (
      <View style={{
         marginVertical: 12,
         marginHorizontal: 8,
         paddingHorizontal: 12,
         borderRadius: SIZES.radius,
         backgroundColor: '#fff',

         shadowColor: "#000",
         shadowOffset: {
            width: 0,
            height: 2,
         },
         shadowOpacity: 0.25,
         shadowRadius: 3.84,
         elevation: 5,
      }}>
         {/* about me */}
         <View style={{
            paddingVertical: 12,
            borderRadius: SIZES.radius,
         }}>
            <CustomText h3 style={{ textDecorationLine: 'underline' }}>Thông tin cơ bản</CustomText>

            <CustomText h4>Giới thiệu bản thân</CustomText>

            <CustomText>
               {description?.introduction }
            </CustomText>

            <CustomText h4>Kỹ năng</CustomText>

            {skill ? skill.map(s=>(
                  <CustomText 
                  key={s.id}>
                  • {s.name}
                  </CustomText>
            )) : null }
            
         </View>


         {/* experience */}
         <View style={{
            marginTop: 12,
            paddingVertical: 12,
            borderTopWidth: 1,
            borderColor: COLORS.border
         }}>
            <CustomText h3 style={{ textDecorationLine: 'underline' }}>Kinh nghiệm</CustomText>

            <CustomText>
              {description?.experience}
            </CustomText>
         </View>

         {/* education */}
         <View style={{
            marginTop: 12,
            paddingVertical: 12,
            borderTopWidth: 1,
            borderColor: COLORS.border
         }}>
            <CustomText h3 style={{ textDecorationLine: 'underline' }}>Học vấn</CustomText>

            <CustomText h4>Trình độ học vấn</CustomText>

            <CustomText>
               {description?.education}
            </CustomText>

            <CustomText h4 >Bằng cấp</CustomText>

            <CustomText>
              {description?.degree}
            </CustomText>
         </View>

      </View>
   )
}

export default About
