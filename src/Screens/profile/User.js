import React, { useState, useEffect } from 'react'
import {
   View,
   ScrollView,
   Image,
   TouchableOpacity,
   Text,
   SafeAreaView,
   ActivityIndicator
} from 'react-native';

import { COLORS, SIZES, icons, FONTS } from '../../shared/constants';
import CustomText from '../../shared/CustomText';

import { icon_clock, icon_location, icon_star } from '../../shared/constants/icons';
import About from './About';
import Reviews from './Reviews';

import * as firebase from 'firebase';

const User = (props) => {

   const [tabView, setTapView] = useState('about');
   const [userData, setUserData] = useState({});
   const [isLoading, setIsLoading] = useState(false);

   const getUserData = async () => {
      // get userData from firestore
      setIsLoading(true);
      let array = [];
      await firebase.firestore()
         .collection('users')
         .where('userId','==',props.route.params.id)
         .get()
         .then(query=>{
            query.forEach(doc=>{
               const {
                  userImage,
                  name,
                  address,
                  dateOfBirth,
                  description,
                  jobCount,
                  wallet,
                  email,
                  skill
               }  = doc.data();
               array.push({
                  userImage,
                  name,
                  address,
                  dateOfBirth,
                  description,
                  jobCount,
                  wallet,
                  email,
                  skill,
                  id:doc.id,
               })
            });
            setUserData(array[0]);
            setIsLoading(false);
         })

   }

   useEffect(() => {
      getUserData();
   },[]);

   if (isLoading)
      return (
         <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <ActivityIndicator size="large" color={COLORS.primary} />
         </View>
      );
   else
      return (
         <SafeAreaView style={{
            flex: 1,
            backgroundColor: COLORS.background,
            paddingTop: 25
         }}>
            <View style={{
               flexDirection: 'row',
               paddingHorizontal: SIZES.padding,
               justifyContent: 'space-between',
               alignItems: 'center',
            }}>
               <TouchableOpacity
                  onPress={() => props.navigation.goBack()}
                  style={{
                     borderWidth: 1,
                     padding: 5,
                     borderColor: COLORS.grayblue,
                     borderRadius: 5
                  }}
               >
                  <Image
                     source={icons.icon_back}
                     style={{
                        width: 20,
                        height: 20,
                        tintColor: COLORS.black
                     }}
                     resizeMode="contain"
                  />

               </TouchableOpacity>
               <Text style={{ ...FONTS.h3, color: COLORS.black }}>Thông tin</Text>
               <View style={{ width:20, height: 20}}/>
               {/* <TouchableOpacity
                  style={{
                     borderWidth: 1,
                     padding: 5,
                     borderColor: COLORS.primary,
                     borderRadius: 5
                  }}
                  onPress={() => props.navigation.navigate('EditProfileScreen', { userDetails: userData })}
               >
                  <Image
                     source={icons.icon_edit}
                     style={{
                        width: 20,
                        height: 20,
                        tintColor: COLORS.primary
                     }}
                     resizeMode="contain"
                  />
               </TouchableOpacity> */}
            </View>


            <ScrollView>
               {/* basic information */}
               <View style={{
                  marginHorizontal: SIZES.padding,
                  marginTop: 12,
                  paddingBottom: SIZES.padding,
                  borderRadius: 8,
                  backgroundColor: '#fff',

                  shadowColor: "#000",
                  shadowOffset: {
                     width: 0,
                     height: 2,
                  },
                  shadowOpacity: 0.25,
                  shadowRadius: 3.84,
                  elevation: 5,
               }}>
                  <View style={{
                     flexDirection: 'row',
                     marginVertical: 12,
                     paddingHorizontal: SIZES.padding,
                  }}>
                     <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Image style={{ width: 90, height: 90, borderRadius: SIZES.radius }}
                           source={{
                              uri: userData.userImage ? userData.userImage : 'https://randomuser.me/api/portraits/men/78.jpg'
                           }}
                           resizeMode='contain' />
                     </View>

                     <View style={{ flex: 2, marginLeft: 12 }}>
                        <CustomText h4>{userData?.name}</CustomText>
                        {/* <CustomText h5>Freelancer</CustomText> */}

                        <View style={{
                           flexDirection: 'row',
                           marginTop: 5,
                           alignItems: 'center',
                        }}>
                           <Image style={{ width: 16, height: 16, marginRight: 12, tintColor: COLORS.primary }} source={icon_location} />
                           <CustomText>{userData.address}</CustomText>
                        </View>

                        <View style={{
                           flexDirection: 'row',
                           marginTop: 5,
                           alignItems: 'center',
                        }}>
                           <Image style={{ width: 16, height: 16, marginRight: 12, tintColor: COLORS.primary }} source={icon_clock} />
                           <CustomText>Tham gia 5 ngày trước</CustomText>
                        </View>

                        <View style={{
                           flexDirection: 'row',
                           marginTop: 5,
                           alignItems: 'center',
                        }}>
                           <Image style={{ width: 16, height: 16, marginRight: 12 }} source={icon_star} />
                           <CustomText>0,0 nhận xét</CustomText>
                        </View>

                     </View>
                  </View>

                  {/* tab view */}
                  <View style={{ flexDirection: 'row' }}>
                     <TouchableOpacity style={{
                        paddingHorizontal: SIZES.padding
                     }}
                        onPress={() => setTapView('about')} >
                        <CustomText>Giới thiệu</CustomText>
                        {tabView == 'about' && <ShowTab />}
                     </TouchableOpacity>

                     <TouchableOpacity style={{
                        paddingHorizontal: SIZES.padding
                     }}
                        onPress={() => setTapView('posts')} >
                        <CustomText>Bài viết</CustomText>
                        {tabView == 'posts' && <ShowTab />}
                     </TouchableOpacity>

                     <TouchableOpacity style={{
                        paddingHorizontal: SIZES.padding
                     }}
                        onPress={() => setTapView('review')}>
                        <CustomText>Nhận xét</CustomText>
                        {tabView == 'review' && <ShowTab />}
                     </TouchableOpacity>
                  </View>

               </View>

               {tabView == 'about' ? <About userData={userData ? userData : null}/> : (tabView == 'review' ? <Reviews /> : <Text></Text>)}

            </ScrollView>
         </SafeAreaView>
      )
}

export default User

const ShowTab = () => {
   return (
      <View style={{
         borderTopWidth: 2,
         borderColor: COLORS.primary,
         width: 45,
      }} />
   )
}