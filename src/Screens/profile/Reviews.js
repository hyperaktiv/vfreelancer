import React from 'react'
import {
   View,
   Image,
   TouchableOpacity,
   Dimensions
} from 'react-native';
import { COLORS, SIZES } from '../../shared/constants';
import CustomText from '../../shared/CustomText';
import { icon_edit, icon_star, icon_like } from '../../shared/constants/icons';

var width = Dimensions.get('window').width;

const Reviews = () => {
   return (
      <View style={{
         marginVertical: 12,
         marginHorizontal: 8,
         paddingHorizontal: 12,
         borderRadius: SIZES.radius,
         backgroundColor: '#fff',

         shadowColor: "#000",
         shadowOffset: {
            width: 0,
            height: 2,
         },
         shadowOpacity: 0.25,
         shadowRadius: 3.84,
         elevation: 5,
      }}>

         <View style={{
            paddingVertical: 12,
            borderRadius: SIZES.radius,
         }}>
            <View style={{
               flexDirection: 'row'
            }}>
               <View style={{ flex: 1.2, }}>
                  <CustomText h3 style={{ textDecorationLine: 'underline' }}>Đánh giá</CustomText>

                  <View style={{
                     flexDirection: 'row',
                     alignItems: 'center',
                     marginVerical: 7
                  }}>
                     <Image style={{ width: 16, height: 16, marginRight: 12 }} source={icon_star} />
                     <CustomText style={{ color: COLORS.grayblue }}>0,0 (Chưa có nhận xét)</CustomText>
                  </View>
               </View>

               <View style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center'
               }}>
                  <TouchableOpacity style={{
                     flexDirection: 'row',
                     height: 30,
                     width: 150,
                     borderRadius: 8,
                     justifyContent: 'center',
                     alignItems: 'center',
                     backgroundColor: '#1DA1F2',
                  }}>
                     <Image style={{
                        width: 16, height: 16,
                        tintColor: COLORS.white,
                        marginRight: 7
                     }}
                        source={icon_edit}
                     />
                     <CustomText style={{ fontSize: 14, fontWeight: '600', color: COLORS.white }}>Write a review</CustomText>
                  </TouchableOpacity>
               </View>
            </View>

            {/* all the comment box in here */}
            <CommentBox />
            <CommentBox />
            <CommentBox />

         </View>
      </View>
   )
}

export default Reviews

const CommentBox = () => {
   return (
      <>
         {/* a comment view box */}
         <View style={{
            marginTop: SIZES.padding,
            paddingTop: SIZES.padding * 2,
            width: width * 0.9,
            borderTopWidth: 1,
            borderColor: COLORS.border,
         }}>
            {/* image - name - start review */}
            <TouchableOpacity style={{ flexDirection: 'row' }}>
               <View style={{
                  flex: 1,
                  justifyContent: 'center',
               }}>
                  <Image style={{ width: 50, height: 50 }}
                     source={require('./avt-comment.jpg')} />
               </View>

               <View style={{ flex: 4, }}>
                  <CustomText h4>Myra Douglas</CustomText>
                  <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                     <View style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        paddingBottom: 5,
                     }}>
                        <Image style={{ width: 16, height: 16, marginRight: 12 }} source={icon_star} />
                        <CustomText h4 style={{ lineHeight: 22 }}>5.0</CustomText>
                     </View>

                     {/* date on comment */}
                     <CustomText style={{
                        color: COLORS.grayblue,
                        marginRight: SIZES.padding
                     }}>Jan 05, 2021</CustomText>
                  </View>
               </View>
            </TouchableOpacity>

            <CustomText>
               Dr. Martin Wallace provides answers that are inspring and sensible. I know that but what lifestyle and food I need to know that?
               </CustomText>

            <View style={{
               flexDirection: 'row',
               paddingVertical: SIZES.padding,
               justifyContent: 'space-between',
               alignItems: 'center',
               height: 30,

            }}>
               <CustomText style={{ color: COLORS.grayblue }}>Report</CustomText>
               <TouchableOpacity>
                  <Image style={{
                     width: 22, height: 22,
                     tintColor: COLORS.grayblue,
                     marginBottom: 12,
                     marginRight: SIZES.padding
                  }}
                     source={icon_like}
                  />
               </TouchableOpacity>
            </View>
         </View>
      </>
   )
}