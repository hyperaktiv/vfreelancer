import React, { useEffect, useState } from 'react'
import {
   ScrollView,
   View,
   Dimensions,
   SafeAreaView,
   TouchableOpacity,
   Image,
   ActivityIndicator,
   StyleSheet,
   Modal,
   Text,
   Pressable
} from 'react-native';
import { icon_bookmark, icon_share } from '../../shared/constants/icons';
import Button from '../../shared/Button';
import JobDetailsHeader from './JobDetailsHeader';
import { COLORS, FONTS, SIZES, icons } from '../../shared/constants';
import JobDetails from './JobDetails';
import JobDescription from './JobDescription';
import SimilarJobs from './SimilarJobs';
import useHandleScroll from '../../shared/CustomHook/useHandleScroll';
import * as firebase from 'firebase';
import Toast from 'react-native-toast-message';
import UserApplyJob from './UserApplyJob';
import { auth, firestore } from '../../firebase';
var width = Dimensions.get('window').width;
import Input from '../../shared/Input';
const JobDetailsScreen = ({ navigation, route }) => {
   const { handleScroll, showButton } = useHandleScroll();

   const [idUserPost, setIdUserPost] = useState(null);
   const [type, setType] = useState(null);
   const [jobId, setJobId] = useState(null);
   const [jobName, setjobName] = useState(null);
   const [jobDescription, setjobDescription] = useState(null);
   const [jobDeadline, setjobDeadline] = useState(null);
   const [postTime, setpostTime] = useState(null);
   const [skills, setskills] = useState(null);
   const [nameUserPost, setNameUserPost] = useState(null);
   const [jobBudget, setjobBudget] = useState(null);
   const [jobPayment, setjobPayment] = useState(false);
   const [jobImage, setJobImage] = useState(false);
   const [loading, setLoading] = useState(true);
   const [listJob, setListJob] = useState([]);
   const [listUserApplyJob, setListUserApplyJob] = useState([]);
   const [refresh, setRefresh] = useState(false);
   const [visible, setVisible] = useState(false);
   const [budgetApply, setBudgetApply] = useState(null);
   const [noteApply, setNoteApply] = useState(null);
   const fecthData = async () => {
      //lấy thông tin job
      await firestore.collection('posts')
         .doc(route.params.id)
         .get()
         .then((doc) => {
            const { idUserPost,
               type,
               nameUserPost,
               postTime,
               jobName,
               jobDescription,
               jobImage,
               skills,
               jobBudget,
               jobPayment,
               jobDeadline } = doc.data();
            let array = [];
            skills.forEach((doc) =>
               array.push(doc.name)
            );
            setType(type);
            setJobId(doc.id);
            setIdUserPost(idUserPost);
            setNameUserPost(nameUserPost);
            setpostTime(postTime);
            setjobName(jobName);
            setjobDescription(jobDescription);
            setJobImage(jobImage);
            setskills(array.toLocaleString());
            setjobBudget(jobBudget);
            setjobPayment(jobPayment);
            setjobDeadline(jobDeadline);

            //lẫy danh sách user ứng tuyển job này
            let listUser = [];

            firestore.collection('JobApply')
               .where('jobId', '==', route.params.id)
               .get()
               .then((query) => {
                  query.forEach(doc => {
                     const { userApplyPhoto, userApplyId, userApplyName, createdAt } = doc.data();
                     listUser.push({
                        createdAt,
                        userApplyPhoto,
                        userApplyId,
                        userApplyName,
                        createdAt
                     })
                  })
               });
            setListUserApplyJob(listUser);


            //lấy những job gần giống với kỹ năng của user
            let array2 = [];
            firestore.collection('posts')
               .where('skills', 'array-contains-any', skills)
               .get()
               .then((query) => {
                  query.forEach((doc) => {
                     const {
                        postTime,
                        jobName,
                        jobImage,
                        skills,
                        jobBudget } = doc.data();
                     if (route.params.id != doc.id) {
                        array2.push({
                           id: doc.id,
                           postTime,
                           jobName,
                           jobImage,
                           skills: skills.toLocaleString(),
                           jobBudget
                        });
                     }
                     setLoading(false);
                  });
               })
            setListJob(array2);
            setLoading(false);
         });

   }
   useEffect(() => {
      fecthData();
   }, [])

   const submitJob = async () => {
      const isApply = listUserApplyJob.find(user => user.userApplyId == auth.currentUser.uid);
      if (isApply) {
         Toast.show({
            topOffset: 60,
            type: 'error',
            text1: 'Bạn đã ứng tuyển job này rồi',
            text2: 'Vui lòng tìm job khác!'
         })
      } else if (firebase.auth().currentUser.uid == idUserPost) {
         Toast.show({
            topOffset: 60,
            type: 'error',
            text1: 'Job này được bạn đăng. Không thể ứng tuyển.',
            text2: 'Vui lòng tìm job khác!'
         })
      }
      else if (type == 'close' || type == 'inprogress') {
         Toast.show({
            topOffset: 60,
            type: 'error',
            text1: 'Job này đã có người nhận hoặc đã kết thúc.',
            text2: 'Vui lòng tìm job khác!'
         })
      }
      else {
         await firebase.firestore()
            .collection('JobApply')
            .add({
               budget_apply: budgetApply,
               note_apply: noteApply,
               jobId,
               jobName,
               idUserPost: idUserPost,
               userApplyName: firebase.auth().currentUser.displayName,
               userApplyId: firebase.auth().currentUser.uid,
               createdAt: new Date(),
               type: 'notapprove'
            })
            .then(async () => {
               await firebase.firestore()
                  .collection('notifications')
                  .add({
                     userId: idUserPost,
                     message: `${firebase.auth().currentUser.displayName} vừa ứng tuyển việc làm bạn đăng.`,
                     created_at: new Date(),
                     seen: false
                  })
                  .then(() => {
                     setVisible(false);
                     Toast.show({
                        topOffset: 60,
                        type: 'success',
                        text1: 'Yêu cầu nhận job này đã được gửi',
                        text2: 'Vui lòng đợi phản hồi từ khách hàng!'
                     })
                  })

            })
      }
   }
   const ButtonIcon = () => {
      return (
         <View style={styles.bottom_container}>
            <TouchableOpacity style={styles.btn_bottom}>
               <Image
                  resizeMode="contain"
                  style={styles.icon_btn_bottom}
                  source={icon_bookmark}
               />
            </TouchableOpacity>

            <Button
               onPress={() => setVisible(true)}
               text='ỨNG TUYỂN'
               style={{
                  width: width * 0.6,
                  backgroundColor: COLORS.primary,
                  marginHorizontal: 12,
               }} />

            <TouchableOpacity
               style={styles.btn_bottom}>
               <Image
                  style={styles.icon_btn_bottom}
                  resizeMode="contain"
                  source={icon_share}
               />
            </TouchableOpacity>
         </View>
      )
   }
   if (loading) {
      return (
         <SafeAreaView style={{
            flex: 1,
            backgroundColor: COLORS.background
         }}>
            <JobDetailsHeader navigation={navigation} />
            <ActivityIndicator size="large" animating={true} color={COLORS.primary} />
         </SafeAreaView>
      )
   } else {
      return (
         <SafeAreaView style={{
            flex: 1,
            backgroundColor: visible ? '#00000070' : COLORS.background
         }}>
            <JobDetailsHeader navigation={navigation} />
            <ScrollView onScroll={handleScroll}
               scrollEventThrottle={0}
               alwaysBounceHorizontal={false}
               alwaysBounceVertical={false}
            >
               <Modal
                  transparent={true}
                  animationType="slide"
                  visible={visible}
                  onRequestClose={() => setVisible(false)}
               >
                  <View style={styles.centeredView}>

                     <View style={styles.modalView}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 20 }} >
                           <View style={{ width: 10 }} />
                           <Text style={{ ...FONTS.h4, color: COLORS.black }}>XÁC NHẬN</Text>
                           <TouchableOpacity
                              onPress={() => setVisible(false)}
                              style={styles.btn_close}
                           >
                              <Image
                                 source={icons.icon_close}
                                 style={styles.icon}
                                 resizeMode="contain"
                              />

                           </TouchableOpacity>
                        </View>
                        <Input
                           label="Ngân sách mong muốn ($):"
                           placeholder="..."
                           value={budgetApply}
                           onChangeText={(text) => setBudgetApply(text)}
                        />
                        <Input
                           label="Ghi chú:"
                           placeholder="..."
                           value={noteApply}
                           onChangeText={(text) => setNoteApply(text)}
                        />
                        <Button
                           onPress={() => submitJob()}
                           text='ĐỒNG Ý'
                           style={{
                              width: width * 0.8,
                              backgroundColor: COLORS.primary,
                              marginHorizontal: 12,
                           }} />
                     </View>
                  </View>
               </Modal>
               <View />
               <JobDetails
                  visible={visible}
                  jobName={jobName}
                  jobImage={jobImage}
                  jobBudget={jobBudget}
                  postTime={postTime ? postTime : null}
                  skills={skills}
               />
               <UserApplyJob
                  visible={visible}
                  listUserApplyJob={listUserApplyJob}
                  navigation={navigation}
               />
               <JobDescription
                  visible={visible}
                  jobDescription={jobDescription}
                  skills={skills}
                  jobName={jobName}
                  jobBudget={jobBudget}
                  postTime={postTime}
                  jobDeadline={jobDeadline}
                  jobPayment={jobPayment}
                  nameUserPost={nameUserPost}
                  idUserPost={idUserPost}
                  navigation={navigation}
               />
               <SimilarJobs
                  visible={visible}
                  navigation={navigation}
                  listJob={listJob}
               />
            </ScrollView>
            {showButton && !visible ? <ButtonIcon /> : null}
         </SafeAreaView>
      )
   }
}


export default JobDetailsScreen

const styles = StyleSheet.create({
   bottom_container: {
      flexDirection: 'row',
      marginHorizontal: 24,
      alignItems: 'center',
      justifyContent: 'center',
      position: 'absolute',
      bottom: 10,
   },
   btn_bottom: {
      backgroundColor: COLORS.background,
      width: 50,
      height: 50,
      justifyContent: 'center',
      alignItems: 'center',
      borderWidth: 1,
      borderColor: COLORS.border,
      borderRadius: SIZES.radius
   },
   icon_btn_bottom: {
      width: 20,
      height: 20,
      tintColor: COLORS.primary
   },
   centeredView: {
      flex: 1,

      justifyContent: "center",
      alignItems: "center",
      marginTop: 22
   },
   modalView: {
      margin: 20,
      width: SIZES.width - SIZES.padding * 2,
      height: 400,
      backgroundColor: "white",
      borderRadius: 20,
      paddingHorizontal: SIZES.padding,
      paddingVertical: SIZES.padding,
      shadowColor: "#000",
      shadowOffset: {
         width: 0,
         height: 2
      },
      shadowOpacity: 0.25,
      shadowRadius: 4,
      elevation: 5
   },
   button: {
      borderRadius: 20,
      padding: 10,
      elevation: 2
   },
   buttonOpen: {
      backgroundColor: "#F194FF",
   },
   buttonClose: {
      backgroundColor: "#2196F3",
   },
   textStyle: {
      color: "white",
      fontWeight: "bold",
      textAlign: "center"
   },
   modalText: {
      marginBottom: 15,
      textAlign: "center"
   },
   icon: {
      width: 20,
      height: 20,
      tintColor: COLORS.black
   },
   btn_close: {
      borderWidth: 1,
      padding: 5,
      borderColor: COLORS.grayblue,
      borderRadius: 5,
      justifyContent: 'center',
      alignItems: 'center'
   }
})