import React from 'react'
import { TouchableOpacity, View, Text, StyleSheet } from 'react-native'
import { COLORS, FONTS, SIZES } from '../../shared/constants'
import CustomText from '../../shared/CustomText'

const JobDescription = ({
   jobDescription,
   skills,
   jobPayment,
   nameUserPost,
   jobBudget,
   jobDeadline,
   idUserPost,
   navigation,
   visible
}) => {

   return (
      <View style={visible ? styles.container_modal : styles.container}>
         {/* // Job summary */}
         <View style={{
            marginTop: 10
         }}>
            <CustomText h4 style={{ fontWeight: '800', textDecorationLine: 'underline', }}>Mô tả công việc</CustomText>
            <CustomText>
               {jobDescription}
            </CustomText>
         </View>
         {/* required */}
         <View style={{
            marginTop: 10
         }}>
            <CustomText h4>Kỹ năng yêu cầu:</CustomText>
            <View style={styles.job_field}>
               <CustomText>{skills}</CustomText>
            </View>
         </View>
         <View style={styles.job_field}>
            <CustomText h4>Ngân sách dự kiến:</CustomText>
            <CustomText style={{ color: COLORS.primary }}>${jobBudget}</CustomText>
         </View>
         <View style={styles.job_field}>
            <CustomText h4>Hình thức chi trả:</CustomText>
            <CustomText style={{ color: COLORS.black }}>{jobPayment}</CustomText>
         </View>
         <View style={styles.job_field}>
            <CustomText h4>Thời gian hoàn thành:</CustomText>
            <CustomText>{jobDeadline}</CustomText>
         </View>
         <View style={styles.job_field}>
            <CustomText h4>Đăng bởi:</CustomText>
            <TouchableOpacity onPress={() => navigation.navigate("User", {
               id: idUserPost
            })}>
               <Text style={{ ...FONTS.body1, color: COLORS.primary }}>{nameUserPost}</Text>
            </TouchableOpacity>
         </View>

      </View>
   )
}

export default JobDescription;
const styles = StyleSheet.create({
   container: {
      paddingBottom: SIZES.padding,
      marginVertical: 5,
      marginHorizontal: SIZES.padding - 4,
      paddingHorizontal: 12,
      borderRadius: SIZES.radius,
      backgroundColor: '#fff',
      shadowColor: "#000",
      shadowOffset: {
         width: 0,
         height: 1,
      },
      shadowOpacity: 0.20,
      shadowRadius: 1.41,
      elevation: 2,
   },
   container_modal: {
      paddingBottom: SIZES.padding,
      marginVertical: 5,
      marginHorizontal: SIZES.padding - 4,
      paddingHorizontal: 12,
      borderRadius: SIZES.radius,
      backgroundColor: '#FFFFFF00',

   },
   job_field: {
      marginTop: 10,
      flexDirection: 'row',
      justifyContent: 'space-between',
   }
})