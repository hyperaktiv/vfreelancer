import React from 'react'
import { View, Image, Text, Dimensions, StyleSheet } from 'react-native';
import { COLORS, FONTS, images, SIZES } from '../../shared/constants';
import { capitalizeFirstLetter, formatDateAndTime } from '../../shared/utils';
var width = Dimensions.get('window').width;

const JobDetails = ({ jobName, jobImage, jobBudget, skills, postTime, visible }) => {
   return (
      <View style={visible ? styles.container_modal : styles.container}>
         {/* company image */}
         <View style={{
            flexDirection: 'row',
            alignItems: 'center'
         }}>
            {
               jobImage == undefined || jobImage == null ? (
                  <Image
                     source={images.logo}
                     style={styles.logo}
                     resizeMode="contain"

                  />
               ) : (
                  <Image
                     style={styles.logo}
                     source={require('../../../assets/images/logo.png')}
                     resizeMode="contain" />
               )
            }
            <Text style={{
               ...FONTS.h4,
               color: COLORS.black,
               marginLeft: 10
            }}>{capitalizeFirstLetter(jobName)}</Text>
         </View>
         <View style={{ flex: 1 }}>
            <View style={{
               flex: 1,
               flexDirection: 'row',
               marginTop: 5,
               flexWrap: 'wrap',
               alignItems: 'flex-start'
            }}>
               <Text style={{ ...FONTS.body1, color: COLORS.grayblue }}>Kỹ năng: </Text>
               <Text style={{
                  ...FONTS.body1,
                  color: COLORS.black,

               }}>{skills}</Text>
            </View>
            <View style={{
               flexDirection: 'row',
               marginTop: 5
            }}>

               <Text style={{ ...FONTS.body1, color: COLORS.grayblue }}>Ngân sách:</Text>
               <Text
                  style={{
                     ...FONTS.body1,
                     color: COLORS.primary
                  }}>
                  ${jobBudget}
               </Text>
            </View>
            <View style={{ marginTop: 5 }}>
               {postTime ? (<Text style={{ ...FONTS.body1, color: COLORS.grayblue }}>Đăng lúc {formatDateAndTime(postTime.toDate())} </Text>) : null}
            </View>
         </View>
      </View>
   )
}
export default JobDetails;
const styles = StyleSheet.create({
   container: {
      width: width - SIZES.padding * 2,
      flex: 1,
      marginBottom: 5,
      borderRadius: 8,
      paddingVertical: 10,
      backgroundColor: '#fff',
      shadowColor: "#000",
      shadowOffset: {
         width: 0,
         height: 1,
      },
      shadowOpacity: 0.20,
      shadowRadius: 1.41,
      elevation: 2,
      paddingHorizontal: 20,
      alignSelf: 'center'
   },
   container_modal: {
      width: width - SIZES.padding * 2,
      flex: 1,
      marginBottom: 5,
      borderRadius: 8,
      paddingVertical: 10,
      backgroundColor: '#FFFFFF00',
      paddingHorizontal: 20,
      alignSelf: 'center'
   },
   logo: {
      width: 50,
      height: 50,
      borderRadius: 25
   }
})
