import React from 'react'
import {
   View,
   TouchableOpacity,
   Image
} from 'react-native';
import { COLORS, images, icons } from '../../shared/constants';

const ImageWidth = 200;
const scaleImg = 561 / 61;

const JobDetailsHeader = ({ navigation }) => {
   return (
      <View style={{
         flexDirection: 'row',
         marginBottom: 12,
         justifyContent: 'space-between',
         alignItems: 'center',
         marginHorizontal: 20
      }}>
         {/* icon back to home screen */}
         <TouchableOpacity
            onPress={() => navigation.goBack()}
            style={{
               borderWidth: 1,
               padding: 5,
               borderColor: COLORS.grayblue,
               borderRadius: 5
            }}
         >
            <Image
               source={icons.icon_back}
               style={{
                  width: 20,
                  height: 20,
                  tintColor: COLORS.black
               }}
               resizeMode="contain"
            />

         </TouchableOpacity>
         <Image style={{
            width: ImageWidth,
            height: ImageWidth / scaleImg
         }}
            source={images.logo_name} resizeMode="contain" />

         <View style={{width:50,height:50}}/>
      </View>
   )
}
export default JobDetailsHeader
