import React from 'react'
import { View, TouchableOpacity, Text, Dimensions, StyleSheet } from 'react-native';
import { COLORS, FONTS, images, SIZES } from '../../shared/constants';
import { formatDateAndTime } from '../../shared/utils';
var width = Dimensions.get('window').width;

const UserApplyJob = ({ listUserApplyJob, navigation, visible }) => {
    return (
        <View style={!visible ? styles.container : styles.container_modal}>
            <Text style={{
                ...FONTS.h4,
                color: COLORS.black,
            }}>Freelancers ứng tuyển</Text>
            <View>
                {listUserApplyJob.length == 0 ? (
                    <Text style={{ ...FONTS.body1, color: COLORS.black }}>Chưa có freelancer ứng tuyển job này.</Text>
                ) :
                    listUserApplyJob.map(user => (
                        <View style={{
                            flexDirection: 'row'
                        }} key={user.userApplyId}>

                            <View style={{
                                width: SIZES.width - 2 * SIZES.padding - 90
                            }}>
                                <TouchableOpacity
                                    onPress={() => navigation.navigate("User", { id: user.userApplyId })}
                                    style={{
                                        flexDirection: 'row',
                                        justifyContent: 'space-between',
                                        alignItems: 'center',
                                    }}>
                                    <Text style={{ ...FONTS.h5, color: COLORS.primary }}>• {user.userApplyName}</Text>
                                    {user.createdAt && <Text style={{ ...FONTS.body1, color: COLORS.grayblue }}>Lúc {formatDateAndTime(user.createdAt.toDate())}</Text>}
                                </TouchableOpacity>
                            </View>
                        </View>
                    ))
                }
            </View>

        </View>
    )
}
export default UserApplyJob;
const styles = StyleSheet.create({
    container: {
        width: width - SIZES.padding * 2,
        flex: 1,
        borderRadius: 8,
        paddingVertical: 10,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,

        paddingHorizontal: 20,
        alignSelf: 'center',

    },
    container_modal: {
        width: width - SIZES.padding * 2,
        flex: 1,
        borderRadius: 8,
        paddingVertical: 10,
        backgroundColor: '#FFFFFF00',

        paddingHorizontal: 20,
        alignSelf: 'center',

    }
})
