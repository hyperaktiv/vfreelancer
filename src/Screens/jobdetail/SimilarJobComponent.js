import React from 'react'
import {
    View,
    Image,
    Text,
    TouchableOpacity,
    Dimensions,
    StyleSheet
} from 'react-native';
import { COLORS, FONTS, images } from '../../shared/constants';
import Icon from 'react-native-vector-icons/FontAwesome';
import { capitalizeFirstLetter, formatDateAndTime } from '../../shared/utils';

var width = Dimensions.get('window').width;

const SimilarJobComponent = ({ navigation, item }) => {
    const { jobImage, jobName, id, postTime, skills, jobBudget } = item;
    return (
        <TouchableOpacity
            style={styles.container}
            onPress={() => navigation.replace("Jobdetails", { id })}
        >
            {/* company image */}
            <View style={{
                flexDirection: 'row',
                alignItems: 'center'
            }}>
                {
                    jobImage == undefined || jobImage == null ? (
                        <Image
                            source={images.logo}
                            style={styles.image}
                            resizeMode="contain"

                        />
                    ) : (
                        <Image
                            style={styles.image}
                            source={require('../../../assets/images/logo.png')}
                            resizeMode="contain" />
                    )
                }
                {/* job name */}
                <Text style={{
                    marginLeft: 20,
                    ...FONTS.h4,
                    color: COLORS.black,
                }}>{capitalizeFirstLetter(jobName)}</Text>
            </View>

            <View style={{ flex: 1 }}>
                <View style={{
                    flexDirection: 'row',
                    marginTop: 5,
                }}>
                    <Text style={{ ...FONTS.body1, color: COLORS.grayblue }}>Ngân sách:</Text>
                    <Text style={{
                        ...FONTS.body1,
                        color: COLORS.primary,
                    }}> ${jobBudget}
                    </Text>
                </View>

                <View style={{
                    flexDirection: 'row',
                    marginTop: 5,
                    alignItems: 'center'
                }}>
                    <Icon name="clock-o" size={15} color={COLORS.grayblue} />
                    {postTime ? (<Text style={{
                        ...FONTS.body2,
                        color: COLORS.grayblue,
                        marginLeft: 12
                    }}>Đăng lúc {formatDateAndTime(postTime.toDate())} </Text>) : null}
                </View>
            </View>
        </TouchableOpacity>
    )
}

export default SimilarJobComponent;

const styles = StyleSheet.create({
    container: {
        width: width * 0.8,
        //marginVertical: 5,
        borderRadius: 8,
        padding: 10,
        backgroundColor: '#fff',
        marginHorizontal: 10,
        marginVertical: 25,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,

        paddingHorizontal: 20,
        flex: 1,
        alignSelf: 'center'
    },
    image: {
        width: 30,
        height: 30,
        borderRadius: 25
    }
})
