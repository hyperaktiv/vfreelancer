import React from 'react'
import {  FlatList } from 'react-native';

import SimilarJobComponent from './SimilarJobComponent';

const SimilarJobs = ({ navigation, listJob }) => {
   const renderItem = ({ item }) => {
      return (
         <SimilarJobComponent
            navigation={navigation}
            item={item}/>
      )
   }
   return (
      <FlatList
         style={{
            marginBottom: 100,
         }}
         data={listJob}
         renderItem={renderItem}
         keyExtractor={item => item.id}
         extraData
         horizontal
      />
   )
}

export default SimilarJobs
