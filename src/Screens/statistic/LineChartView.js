import React from 'react';
import { View } from 'react-native';
import { LineChart } from 'react-native-chart-kit';


const data = {
   labels: ['Jan', 'Feb', 'Mar', 'April', 'May', 'June'],
   datasets: [
      {
         data: [0, 0.6, 0.4, 0.0, 7.2, 5.2],
         color: (opacity = 1) => `rgba(134, 65, 244, ${opacity})`, // optional
         strokeWidth: 3, // optional
      },
   ],
};

const LineChartView = () => {
   const height = 220;
   const width = 330;

   const chartConfig = {
      backgroundColor: '#e26a00',
      backgroundGradientFromOpacity: 0,
      backgroundGradientToOpacity: 0,
      color: (opacity = 1) => `#2699FB`,
      strokeWidth: 2,
   };

   return (
      <View style={{
         elevation: 4,
         backgroundColor: '#fff',
         borderRadius: 10,
         marginTop: 15,
      }}>
         <LineChart
            data={data}
            width={width}
            height={height}
            chartConfig={chartConfig}
            style={{ marginBottom: 25, marginTop: 25 }}
         />
      </View>
   );
}

export default LineChartView
