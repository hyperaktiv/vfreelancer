import {
   View,
   TouchableOpacity,
   Image,
   Text
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';
import { COLORS, icons, SIZES, FONTS } from '../../shared/constants';
import React, { useState, useEffect, useContext, useCallback } from 'react';
import { AuthContext } from '../../navigators/AuthProvider';
const StatisticHeader = () => {
   const { user } = useContext(AuthContext);
   return (
      <View style={{
         paddingBottom: 15,
         paddingHorizontal: SIZES.padding,
         flexDirection: 'row',
         justifyContent: 'space-between',
         alignItems: 'center'
      }}>
         <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Image
               source={{
                  uri: user.photoURL ? user.photoURL : 'https://randomuser.me/api/portraits/men/78.jpg'
               }}
               style={{
                  width: 30,
                  height: 30,
                  borderRadius: 50
               }}
               resizeMode="contain"
            />
            <Text style={{ ...FONTS.h3, color: COLORS.black, marginLeft: 20, textAlign: 'center' }}>THỐNG KÊ</Text>
         </View>
      </View>

   )
}

export default StatisticHeader
