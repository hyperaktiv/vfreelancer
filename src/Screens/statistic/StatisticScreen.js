import React, { useState, useEffect } from 'react'
import {
   View,
   ScrollView,
   SafeAreaView,
   Text,
   ActivityIndicator
} from 'react-native'
import { COLORS, FONTS, SIZES } from '../../shared/constants';
import {
   icon_save_money,
   icon_pie_chart,
   icon_effective,
   icon_time_money,
} from '../../shared/constants/icons';
import CustomText from '../../shared/CustomText';
import StatisticHeader from './StatisticHeader';

import CollapseButton from './CollapseButton';
import ProgressBar from './ProgressBar';
import LineChartView from './LineChartView';
import PieChartView from './PieChartView';
import ProgressRingChart from './ProgressRingChart';
import * as firebase from 'firebase';
import { set } from 'react-native-reanimated';

const StatisticScreen = () => {

   const [loading, setLoading] = useState(false);
   const [totalIncome, setTotalIncome] = useState(0);
   const [monthIncome, setMonthIncome] = useState(0);
   const [wallet, setWallet] = useState(0);
   const [jobCompleted, setJobCompleted] = useState(0);
   const [jobCompletetOnTime, setJobCompletedOnTime] = useState(0);
   const [totalJob, setTotalJob] = useState(0);
   const fetchData = async () => {
      setLoading(true);
      await firebase.firestore()
         .collection('income')
         .where('userId', '==', firebase.auth().currentUser.uid)
         .get()
         .then(async query => {
            let _wallet = 0;
            let _totalIncome = 0;
            let monthIncome = 0;
            
            query.forEach(doc => {
               const { income, createdAt, type } = doc.data();
               _wallet = _wallet + income;
               if (type !== 'card') {
                  _totalIncome = _totalIncome + income;
               }
            });
            let _jobCompleted = 0;
            let _jobCompletedOnTime = 0;
            let _totalJob = 0;
            await firebase.firestore()
               .collection('JobApply')
               .get()
               .then(query => {
                  query.forEach(doc => {
                     const { idUserPost, userApplyId, result, type } = doc.data();
                     if (firebase.auth().currentUser.uid == idUserPost || firebase.auth().currentUser.uid == userApplyId) {
                        _totalJob = _totalJob + 1;
                        if (type == 'done') {
                           _jobCompleted = _jobCompleted + 1;
                        }
                        if (result == 'ontime') {
                           _jobCompletedOnTime = _jobCompletedOnTime + 1;
                        }
                     }
                  })
               });

            setWallet(_wallet);
            setTotalIncome(_totalIncome);
            setTotalJob(_totalJob);
            setJobCompleted(_jobCompleted);
            setJobCompletedOnTime(_jobCompletedOnTime);
            setLoading(false);
         });

   }
   useEffect(() => {
      fetchData();
   }, [])

   return (
      <SafeAreaView style={{
         flex: 1,
         backgroundColor: COLORS.background,
         paddingHorizontal: SIZES.padding,
      }}>


         <StatisticHeader />

         {
            loading ? (<ActivityIndicator animating size="large" color={COLORS.primary} />)
               : (
                  <ScrollView contentContainerStyle={{
                     paddingBottom: 100
                  }}>
                     {/* income button view */}
                     <CollapseButton title="Tổng thu nhập" isOpen={true} icon={icon_save_money}>

                        {/* total income */}
                        <View style={{
                           height: 50,
                           justifyContent: 'space-between',
                           alignItems: 'center',
                           marginHorizontal: SIZES.padding,
                           flexDirection: 'row'
                        }}>
                           <Text style={{ ...FONTS.h4, color: 'black' }}>SỐ DƯ VÍ:</Text>
                           <Text style={{ ...FONTS.h4, color: COLORS.primary }}>$ {wallet}</Text>
                        </View>
                        <View style={{
                           borderTopWidth: 1,
                           borderColor: COLORS.border, marginVertical: 12
                        }} />
                        <View style={{
                           height: 50,
                           justifyContent: 'space-between',
                           alignItems: 'center',
                           marginHorizontal: SIZES.padding,
                           flexDirection: 'row'
                        }}>
                           <Text style={{ ...FONTS.h4, color: 'black' }}>TỔNG THU NHẬP:</Text>
                           <Text style={{ ...FONTS.h4, color: COLORS.primary }}>$ {totalIncome}</Text>
                        </View>
                        <View style={{
                           borderTopWidth: 1,
                           borderColor: COLORS.border, marginVertical: 12
                        }} />
                        <View style={{
                           height: 50,
                           justifyContent: 'space-between',
                           alignItems: 'center',
                           marginHorizontal: SIZES.padding,
                           flexDirection: 'row'
                        }}>
                           <Text style={{ ...FONTS.h4, color: 'black' }}>THU NHẬP 30 NGÀY QUA:</Text>
                           <Text style={{ ...FONTS.h4, color: COLORS.primary }}>$ 0.0</Text>
                        </View>


                     </CollapseButton>

                     {/* income following time */}
                     <View style={{ height: 10, backgroundColor: COLORS.background }} />
                     <CollapseButton title="Thu nhập theo thời gian" icon={icon_time_money}>
                        {/* text define */}
                        <View style={{
                           flexDirection: 'row',
                           alignItems: 'center',
                           paddingTop: 10
                        }}>
                           <View style={{
                              height: 5, width: 5,
                              borderRadius: 4,
                              backgroundColor: COLORS.primary,
                              marginRight: 12,
                           }} />
                           <CustomText h5 style={{ textTransform: 'uppercase', }}>số tiền kiếm được ($-USD)</CustomText>
                        </View>
                        {/* chart view */}
                        <LineChartView />

                     </CollapseButton>

                     {/* work effective */}
                     <View style={{ height: 10, backgroundColor: COLORS.background }} />
                     <CollapseButton title="Hiệu quả công việc" icon={icon_effective}>
                        <View style={{
                           padding: SIZES.padding / 2,
                           paddingBottom: SIZES.padding,
                           borderBottomWidth: 1,
                           borderColor: COLORS.border
                        }}>
                           <CustomText h5 style={{ textTransform: 'uppercase', paddingVertical: 5 }}>CÔNG VIỆC HOÀN THÀNH</CustomText>
                           <ProgressBar step={jobCompleted != 0 ? jobCompleted : 1} steps={totalJob != 0 ? totalJob : 1} height={26} />
                        </View>
                        <View style={{
                           padding: SIZES.padding / 2,
                           paddingBottom: SIZES.padding,
                           borderBottomWidth: 1,
                           borderColor: COLORS.border
                        }}>
                           <CustomText h5 style={{ textTransform: 'uppercase', paddingVertical: 5 }}>CÔNG VIỆC ĐÚNG HẠN</CustomText>
                           <ProgressBar step={jobCompletetOnTime != 0 ? jobCompletetOnTime : 1} steps={jobCompleted != 0 ? jobCompleted : 1} height={26} />
                        </View>

                     </CollapseButton>


                     {/* <View style={{ height: 10, backgroundColor: COLORS.background }} /> */}
                     {/* skill assessment */}
                     {/* <CollapseButton title="Đánh giá kỹ năng" isOpen={true} icon={icon_pie_chart}> */}
                     {/* <PieChartView /> */}
                     {/* <ProgressRingChart /> */}
                     {/* </CollapseButton> */}

                  </ScrollView>
               )
         }
      </SafeAreaView>
   )
}
export default StatisticScreen
