import React from 'react'
import { View } from 'react-native';
import { ProgressChart } from 'react-native-chart-kit';

const ProgressRingChart = () => {
   const height = 220;
   const width = 330;
   return (
      <View style={{
         elevation: 4,
         backgroundColor: '#fff',
         borderRadius: 10,
         marginTop: 15,
      }}>
         <ProgressChart
            data={{
               labels: ['Swim', 'Bike', 'Run'], // optional
               data: [0.4, 0.6, 0.8],
            }}
            width={width}
            height={height}
            strokeWidth={10}
            radius={32}
            chartConfig={{
               backgroundGradientFrom: '#fb8c00',
               backgroundGradientTo: '#ffa726',
               color: (opacity = 1) => `rgba(5, 155, 33, ${opacity})`,
            }}
            hideLegend={false}
            style={{
               marginVertical: 8,
               borderRadius: 16,
            }}
         />
      </View>
   )
}

export default ProgressRingChart
