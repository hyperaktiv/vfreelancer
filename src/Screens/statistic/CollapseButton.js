import React, { useState, useRef } from 'react';
import {
   View,
   TouchableOpacity,
   Image,
   Animated,
   Easing,
} from 'react-native';

import { COLORS, SIZES } from '../../shared/constants'
import CustomText from '../../shared/CustomText';
import { icon_back } from '../../shared/constants/icons';

const CollapseButton = ({ title, children, isOpen = false, icon }) => {

   const [open, setOpen] = useState(false);
   const animatedController = useRef(new Animated.Value(0)).current;
   const [bodySectionHeight, setBodySectionHeight] = useState(null);

   const bodyHeight = animatedController.interpolate({
      inputRange: [0, 1],
      outputRange: isOpen == false ? [0, bodySectionHeight] : [bodySectionHeight, 0],
   });
   const arrowAngle = animatedController.interpolate({
      inputRange: [0, 1],
      outputRange: isOpen == false ? ['270deg', '90deg'] : ['90deg', '270deg'],
   });

   const toggleBodyView = () => {
      if (open) {
         Animated.timing(animatedController, {
            duration: 300,
            toValue: 0,
            easing: Easing.bezier(0.4, 0.0, 0.2, 1),
            useNativeDriver: false,
         }).start();
      } else {
         Animated.timing(animatedController, {
            duration: 300,
            toValue: 1,
            easing: Easing.bezier(0.4, 0.0, 0.2, 1),
            useNativeDriver: false,
         }).start();
      }
      setOpen(!open);
   };

   return (
      <>
         {/* header */}
         <TouchableOpacity style={{
            flexDirection: 'row',
            justifyContent: 'center',
            backgroundColor: '#fff',
            borderRadius: SIZES.radius - 8,
            borderWidth: 1,
            borderColor: COLORS.border,
            height: 60,
         }}
            onPress={() => toggleBodyView()}
         >
            <View style={{
               flex: 1.4,
               alignItems: 'center',
               justifyContent: 'center',
               width: 30
            }}>
               <Image style={{ width: 28, height: 28, tintColor: COLORS.primary }} source={icon} />
            </View>

            <View style={{
               flex: 5,
               marginTop: 5,
               justifyContent: 'center',
            }}>
               <CustomText h4>{title}</CustomText>
            </View>

            <Animated.View style={{
               flex: 1,
               justifyContent: 'center',
               alignItems: 'center',
               width: 30,
               transform: [{ rotateZ: arrowAngle }]
            }}>
               <Image style={{ width: 16, height: 16, tintColor: COLORS.primary }} source={icon_back} />
            </Animated.View>

         </TouchableOpacity>

         {/* body */}
         <Animated.View style={[{
            backgroundColor: '#EFEFEF',
            overflow: 'hidden',
         }, , {
            height: bodyHeight,
         }]}>
            <View style={{
               paddingTop: 0,
               paddingHorizontal: SIZES.padding - 8,
               paddingBottom: SIZES.padding - 4,
               // position: 'absolute',
               bottom: 0,
               width: '100%'
            }}
               onLayout={event =>
                  setBodySectionHeight(event.nativeEvent.layout.height)
               }
            >
               {children}
            </View>
         </Animated.View>
      </>
   )
}

// how to use
//<CollapseButton title="Thu nhập theo thời gian" isOpen={true}>
//    ... component you want
//</CollapseButton>


export default CollapseButton
