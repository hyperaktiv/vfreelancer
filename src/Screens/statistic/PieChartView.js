import React from 'react'
import { View } from 'react-native';
import { PieChart } from 'react-native-chart-kit';


const pieData = [
   {
      name: 'Seoul',
      population: 21500000,
      color: 'rgba(131, 167, 234, 1)',
      legendFontColor: '#7F7F7F',
      legendFontSize: 15,
   },
   {
      name: 'Toronto',
      population: 2800000,
      color: '#F00',
      legendFontColor: '#7F7F7F',
      legendFontSize: 15,
   },
   {
      name: 'Beijing',
      population: 527612,
      color: 'red',
      legendFontColor: '#7F7F7F',
      legendFontSize: 15,
   },
   {
      name: 'New York',
      population: 8538000,
      color: 'orange',
      legendFontColor: '#7F7F7F',
      legendFontSize: 15,
   },
   {
      name: 'Moscow',
      population: 11920000,
      color: 'rgb(0, 0, 255)',
      legendFontColor: '#7F7F7F',
      legendFontSize: 15,
   },
];

const PieChartView = () => {
   const height = 220;
   const width = 330;

   return (
      <View style={{
         elevation: 4,
         backgroundColor: '#fff',
         borderRadius: 10,
         marginTop: 15,
      }}>
         <PieChart
            data={pieData}
            height={height}
            width={width}
            chartConfig={{
               backgroundColor: '#e26a00',
               backgroundGradientFrom: '#fb8c00',
               backgroundGradientTo: '#ffa726',
               color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
            }}
            paddingLeft={'15'}
            backgroundColor={'#fff'}
            accessor="population"
            style={{
               marginVertical: 8,
               borderRadius: 16,
            }}
         />
      </View>
   )
}

export default PieChartView
