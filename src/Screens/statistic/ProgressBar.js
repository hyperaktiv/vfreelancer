import React, { useRef, useState, useEffect } from 'react';
import { Text, View, Animated } from 'react-native';
import { COLORS, SIZES } from '../../shared/constants';

const ProgressBar = ({ step, steps, height }) => {
   
   const [width, setWidth] = useState(0);
   const animatedValue = useRef(new Animated.Value(-100)).current;
   const reactive = useRef(new Animated.Value(-100)).current;

   useEffect(() => {
      Animated.timing(animatedValue, {
         toValue: reactive,
         duration: 300,
         useNativeDriver: true,
      }).start();
   }, []);

   useEffect(() => {
      // -width + width * step/steps
      reactive.setValue(-width + (width * step) / steps);
   }, [step, width]);

   return (
      <>
         <View style={{
            height,
            backgroundColor: COLORS.background,
            borderRadius: 4,
            overflow: 'hidden',
            width: '85%',
         }}
            onLayout={(event) => {
               const newWidth = event.nativeEvent.layout.width;
               setWidth(newWidth);
            }}
         >
            <Animated.View
               style={{
                  height,
                  width: '100%',
                  backgroundColor: COLORS.primary,
                  borderRadius: 4,
                  position: 'absolute',
                  left: 0,
                  top: 0,
                  transform: [{
                     translateX: animatedValue
                  }]
               }}
            />
         </View>

         {step == 1 || step == 0 ? (
            <Text style={{
               fontSize: 12,
               fontWeight: '900',
               marginBottom: 4,
               position: 'absolute',
               right: 5, bottom: height / 2,
            }}>
                {step}/{steps}
            </Text>
         ) : (
            <Text style={{
               fontSize: 12,
               fontWeight: '900',
               marginBottom: 4,
               position: 'absolute',
               right: 5, bottom: height / 2,
            }}>
               {step}/{steps}
               
            </Text>
         )
         }
      </>
   );
};

// how to use? 
{/* <ProgressBar step={2} steps={10} height={30} /> */ }

export default ProgressBar