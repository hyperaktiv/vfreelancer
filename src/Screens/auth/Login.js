import React, { useState, useContext } from 'react';
import {
    View,
    Text,
    SafeAreaView,
    Image,
    TouchableOpacity,
    KeyboardAvoidingView,
    StyleSheet
} from 'react-native';

import { COLORS, FONTS, SIZES, images } from '../../shared/constants';
import Input from '../../shared/Input';
import Button from '../../shared/Button';
import { AuthContext } from '../../navigators/AuthProvider';
import Toast from 'react-native-toast-message';
import ButtonLoading from '../../shared/ButtonLoading';
const Login = (props) => {
    const [email, setEmail] = useState(null);
    const [password, setPassword] = useState(null);
    const { login } = useContext(AuthContext);
    const [isLoading, setIsLoading] = useState(false);
    const handleLogin = () => {
        setIsLoading(true);
        if (email == null || password == null) {
            Toast.show({
                topOffset: 30,
                type: 'error',
                text1: 'Vui lòng nhập đầy đủ thông tin',
            });
            setIsLoading(false);
        } else {
            login(email, password);
            setIsLoading(false);
        }
    }
    return (
        <SafeAreaView style={styles.container}>
            <View style={{
                height: SIZES.height * 0.2,
                alignItems: 'center',
                justifyContent: 'center',
            }}>
                <Image
                    source={images.logo}
                    style={styles.logo}
                    resizeMode="contain"
                />
                <Text style={{ ...FONTS.h1, color: COLORS.black, }}>Chào mừng trở lại!</Text>
            </View>
            <KeyboardAvoidingView style={{
                padding: SIZES.padding
            }}>
                <Input
                    value={email}
                    onChangeText={(text) => setEmail(text)}
                    keyboardType="email-address"
                    label="Email"
                    placeholder="nqduc74@gmail.com"
                />
                <Input
                    label="Mật khẩu"
                    placeholder="Nhập mật khẩu"
                    secureTextEntry={true}
                    value={password}
                    onChangeText={(text) => setPassword(text)}
                />
                {
                    !isLoading
                        ? (
                            <Button
                                onPress={handleLogin}
                                text="Đăng nhập"

                            />
                        )
                        : (
                            <ButtonLoading />
                        )
                }

                <TouchableOpacity>
                    <Text style={{ ...FONTS.body1, color: COLORS.grayblue, textAlign: 'center' }}>Quên mật khẩu?</Text>
                </TouchableOpacity>
                <Text style={{ ...FONTS.body2, color: COLORS.grayblue, textAlign: 'center', marginTop: 20 }}>Đăng nhập bằng tài khoản mạng xã hội</Text>
                <View style={styles.social_container}>
                    <TouchableOpacity style={[{
                        backgroundColor: '#3b5999',
                    }, styles.btn_social]}>
                        <Image
                            source={images.facebook}
                            style={styles.btn_social_icon}
                            resizeMode="contain"
                        />
                        <Text style={{ ...FONTS.h5, color: COLORS.white }}>Facebook</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[{
                        backgroundColor: '#4181eb',
                    }, styles.btn_social]}>
                        <Image
                            source={images.google}
                            style={styles.btn_social_icon}
                            resizeMode="contain"
                        />
                        <Text style={{ ...FONTS.h5, color: COLORS.white }}>Google</Text>
                    </TouchableOpacity>
                </View>
                <TouchableOpacity
                    onPress={() => props.navigation.navigate("Signup")}
                    style={{ flexDirection: 'row', marginVertical: 20, justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ ...FONTS.body1, color: COLORS.grayblue, textAlign: 'center' }}>Bạn chưa có tài khoản?</Text>
                    <Text style={{ ...FONTS.body1, color: COLORS.primary, textAlign: 'center' }}> Đăng ký</Text>
                </TouchableOpacity>
            </KeyboardAvoidingView>
        </SafeAreaView>
    )
}

export default Login;
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.background,
        paddingTop: 100
    },
    logo: {
        width: 150,
        height: 150,
    },
    social_container: {
        flexDirection: 'row',
        marginTop: 20,
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    btn_social: {
        alignItems: 'center',
        width: 170,
        height: 50,
        borderRadius: 16,
        justifyContent: 'center',
        flexDirection: 'row'
    },
    btn_social_icon: {
        width: 20,
        height: 20,
        tintColor: COLORS.white,
        marginHorizontal: 10
    }
})