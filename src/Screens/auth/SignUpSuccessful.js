import React, { useState, useContext } from 'react';
import {
    View,
    Text,
    SafeAreaView,
} from 'react-native';
import { COLORS, FONTS, SIZES } from '../../shared/constants';
import ButtonWithIcon from '../../shared/ButtonWithIcon';
const SignUpSuccessful = (props) => {
    return (
        <SafeAreaView style={{
            flex: 1,
            paddingTop: 40
        }}>
           <View style={{
                flex: 1,
                padding: SIZES.padding,
                marginTop: 200
            }}>
                <Text style={{ ...FONTS.h2, color: COLORS.black, textAlign: 'center' }}>Chào mừng {props.route.params.name} đến với vFreelancer</Text>
                <View style={{
                    paddingHorizontal: 50
                }}>
                    <ButtonWithIcon
                        text="Trang chủ"
                        iconName="arrow-right"
                        onPress={() => props.navigation.navigate("Home")} />
                </View>
            </View>


        </SafeAreaView>
    )
}
export default SignUpSuccessful;