import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    SafeAreaView,
    TouchableOpacity,
    Image,
    FlatList,
    ActivityIndicator,
    StyleSheet
} from 'react-native';
import { COLORS, FONTS, icons, SIZES } from '../../shared/constants';
import ButtonWithIcon from '../../shared/ButtonWithIcon';
import Icon from 'react-native-vector-icons/FontAwesome'
import Toast from 'react-native-toast-message';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { firestore, auth } from '../../firebase';

const OtherInformation = (props) => {

    const [selected, setSelected] = useState(null);
    const { name, address, dateOfBirth } = props.route.params;
    const [parentSkillList, setParentSkillList] = useState([]);
    const [childrenSkillList, setChildrenSkillList] = useState([]);

    const [loading, setLoading] = useState(false);
    const [loading2, setLoading2] = useState(false);
    const [loading3, setLoading3] = useState(false);
    const [refresh, setRefresh] = useState(false);

    const [skills, setSkills] = useState([]);
    const [allSkills, setAllSkills] = useState([]);
    const [newUserId, setNewUserId] = useState(null);
    const fetchSkillList = async () => {
        try {
            setLoading(true);
            const new_user_id = await AsyncStorage.getItem("new_user_id");
            setNewUserId(new_user_id != null ? JSON.parse(new_user_id) : null);
            const listParentSkill = [];
            const listChildrenSkill = [];
            await firestore.collection('TypeOfSkill')
                .get()
                .then((query) => {
                    query.forEach(doc => {
                        //lấy ra tên phân loại kỹ năng ( type ở trong TypeOfSkill )
                        const { type } = doc.data();
                        listParentSkill.push({
                            id: doc.id,
                            type
                        });
                    })
                });
            await firestore.collection('Children_skill')
                .get()
                .then((query) => {
                    query.forEach(doc => {
                        //lấy ra tên phân loại kỹ năng ( type ở trong TypeOfSkill )
                        const { name, parent_skill_id } = doc.data();
                        listChildrenSkill.push({
                            id: doc.id,
                            name,
                            isSelected: false,
                            parent_skill_id
                        });
                    })
                })
            setLoading(false);
            setParentSkillList(listParentSkill);
            setChildrenSkillList(listChildrenSkill)
        } catch (e) {
            console.log(e);
        }
    }

    const _getChildrenSkill = (id) => {
        setLoading2(true);
        setSelected(id);
        const _list = childrenSkillList;
        const list = [];
        _list.forEach((item) => {
            if (item.parent_skill_id == id) {
                list.push(item)
            }
        })
        setLoading2(false);
        setAllSkills(list);
    }

    const selectedItem = (skill) => {
        skill.isSelected = !skill.isSelected;
        let array = [];
        const index = childrenSkillList.findIndex(
            item => item.id === skill.id
        );

        childrenSkillList[index] === skill;
        array = childrenSkillList;
        setChildrenSkillList(array);
        setRefresh(!refresh);
    }
    useEffect(() => {
        fetchSkillList();

    }, []);
    const saveInformation = async () => {
        setLoading3(true);
        await auth.currentUser
            .updateProfile({
                displayName: name,
                photoUrl: ''
            })
            .then(async () => {
                await firestore
                    .collection("users")
                    .doc(newUserId)
                    .update({
                        email: auth.currentUser.email,
                        name,
                        dateOfBirth,
                        address,
                        skill: skills,
                        userId: auth.currentUser.uid,
                        created_at: Date.now()
                    })
                    .then(async () => {
                        setLoading3(false);
                        await AsyncStorage.removeItem('new_user_id');
                    })
            })
            .catch(e => console.log(e))
    }
    const handleButton = () => {

        let array = [];
        let _array = [];
        array = childrenSkillList;

        if (array != null) {
            array.forEach((item) => {
                if (item.isSelected == true) {
                    _array.push(item)
                }
            })
        }
        else {
            Toast.show({
                topOffset: 30,
                type: 'error',
                text1: 'Vui lòng chọn kỹ năng của bạn!',
            });
        }

        if (_array.length == 0) {
            Toast.show({
                topOffset: 30,
                type: 'error',
                text1: 'Vui lòng chọn lĩnh vực của bạn',
            });
        } else {
            setSkills([]);
            _array.forEach(doc => {
                skills.push(doc);
            });
            saveInformation();
            Toast.show({
                topOffset: -60,
                type: 'success',
                text1: 'Cập nhật thông tin thành công!'
            })
            props.navigation.navigate("Signupsuccessful", { name });

        }
    };
    const renderItem = ({ item }) => {
        return (
            <TouchableOpacity
                key={item.id}
                style={selected == item.id ? styles.item_parent_selected : styles.item_parent_un_selected}
                onPress={() => _getChildrenSkill(item.id)}
            >
                <Text style={{ ...FONTS.body1, color: COLORS.black, textAlign: 'center', marginRight: 8 }}>{item.type}</Text>
            </TouchableOpacity>
        )
    }
    const _renderItem = ({ item }) => {
        return (
            <TouchableOpacity
                key={item.id}
                style={styles.item_children}
                onPress={() => selectedItem(item)}
            >
                <Text style={{ ...FONTS.body1, color: COLORS.black, marginRight: 8 }}>{item.name}</Text>
                <Icon name={item.isSelected ? "check" : "plus"} size={15} color={COLORS.primary} />
            </TouchableOpacity>
        )
    }
    return (
        <SafeAreaView style={styles.container}>
            <TouchableOpacity
                onPress={() => props.navigation.goBack()}
                style={styles.btn_goBack}
            >
                <Image
                    source={icons.icon_back}
                    style={styles.btn_icon}
                    resizeMode="contain"
                />
            </TouchableOpacity>
            <View style={styles.body}>
                <Text style={{ ...FONTS.body1, color: COLORS.black }}>Bước 2/2</Text>
                <Text style={{ ...FONTS.h1, color: COLORS.black }}>Kỹ năng của bạn</Text>
                <Text style={{ ...FONTS.body1, color: COLORS.grayblue }}>Chọn kỹ năng của bạn để được đề xuất công việc phù hợp.</Text>
                <Text style={{ ...FONTS.body1, color: COLORS.black, marginVertical: 5 }}>Chọn phân loại</Text>
                <View style={{ height: 60 }}>
                    {
                        loading ? (
                            <ActivityIndicator size="large" color={COLORS.primary} animating={true} />
                        ) : (
                            <FlatList
                                showsHorizontalScrollIndicator={false}
                                horizontal
                                data={parentSkillList}
                                renderItem={renderItem}
                            />
                        )
                    }
                </View>
                {
                    selected ? (<Text style={{ ...FONTS.body1, color: COLORS.black, marginVertical: 5 }}>Chọn lĩnh vực</Text>) : (null)
                }
                {
                    loading2 ? (
                        <ActivityIndicator size="large" color={COLORS.primary} animating={true} />
                    ) : (
                        <FlatList
                            data={allSkills}
                            renderItem={_renderItem}
                            showsVerticalScrollIndicator={false}
                            extraData={refresh}
                        />
                    )
                }

                <View style={{
                    bottom: 0
                }}>
                    {
                        loading3 ? (
                            <ActivityIndicator size="large" color={COLORS.primary} animating={true} />
                        ) : (
                            <ButtonWithIcon
                                text="Hoàn tất"
                                iconName="check"
                                onPress={handleButton}
                            />
                        )
                    }

                </View>
            </View>
        </SafeAreaView>
    )
}
export default OtherInformation;
const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 40,
        backgroundColor: COLORS.background
    },
    body: {
        flex: 1,
        paddingHorizontal: SIZES.padding,
        marginTop: 10
    },
    btn_goBack: {
        width: 40,
        height: 40,
        borderRadius: 12,
        borderWidth: 1,
        borderColor: COLORS.grayblue,
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: SIZES.padding
    },
    btn_icon: {
        width: 22,
        height: 22,
        tintColor: COLORS.black
    },
    item_parent_selected: {
        borderWidth: 1,
        borderColor: COLORS.primary,
        backgroundColor: COLORS.white,
        borderRadius: 8,
        paddingHorizontal: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: 5,
        marginVertical: 5,
        height: 40
    },
    item_parent_un_selected: {
        borderWidth: 1,
        borderColor: COLORS.border,
        backgroundColor: COLORS.white,
        borderRadius: 8,
        paddingHorizontal: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: 5,
        marginVertical: 5,
        height: 40
    },
    item_children: {
        borderWidth: 1,
        borderColor: COLORS.border,
        backgroundColor: COLORS.white,
        borderRadius: 8,
        paddingHorizontal: 20,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginHorizontal: 5,
        marginVertical: 5,
        height: 40
    }
})