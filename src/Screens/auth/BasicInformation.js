import React, { useState, useContext } from 'react';
import {
    View,
    Text,
    SafeAreaView,
    TouchableOpacity,
    Image,
    KeyboardAvoidingView,
    ScrollView,
    StyleSheet
} from 'react-native';
import { COLORS, FONTS, icons, SIZES } from '../../shared/constants';
import Input from '../../shared/Input';
import InputWithIcon from '../../shared/InputWithIcon';
import ButtonWithIcon from '../../shared/ButtonWithIcon';
import Toast from 'react-native-toast-message';
import { AuthContext } from '../../navigators/AuthProvider';
const UpdateProfile = (props) => {
    const [name, setName] = useState(null);
    const [dateOfBirth, setDateOfBirth] = useState(null);
    const [address, setAddress] = useState(null);

    const handleButton = () => {
        if (name == null || dateOfBirth == null || address == null) {
            Toast.show({
                topOffset: 30,
                type: 'error',
                text1: 'Vui lòng nhập đầy đủ thông tin',
            });
        } else {
            props.navigation.navigate("Otherinformation", {
                name,
                dateOfBirth,
                address
            });
        }
    };

    const { logout } = useContext(AuthContext);
    return (
        <SafeAreaView style={{
            flex: 1,
            paddingTop: 40
        }}>
            <TouchableOpacity
                onPress={() => logout()}
                style={styles.btn_go_back}
            >
                <Image
                    source={icons.icon_back}
                    style={styles.btn_go_back_icon}
                    resizeMode="contain"
                />
            </TouchableOpacity>
            <View style={{
                flex: 1,
                padding: SIZES.padding
            }}>
                <Text style={{ ...FONTS.body1, color: COLORS.black }}>Bước 1/2</Text>
                <Text style={{ ...FONTS.h1, color: COLORS.black }}>Thông tin của bạn</Text>
                <Text style={{ ...FONTS.body1, color: COLORS.grayblue }}>Hoàn tất hồ sơ của bạn để đem đến sự uy tín đối với khách hàng.</Text>
                <KeyboardAvoidingView
                    style={{ flex: 1 }}
                >
                    <ScrollView style={{
                        marginTop: 20
                    }}
                        showsVerticalScrollIndicator={false}
                        bounces={false}
                    >
                        <Input
                            label="Họ và tên"
                            placeholder="Nguyễn Quang Đức"
                            value={name}
                            onChangeText={(text) => setName(text)}
                        />
                        <InputWithIcon
                            label="Ngày tháng năm sinh"
                            placeholder="15/08/1999"
                            iconName="calendar"
                            value={dateOfBirth}
                            onChangeText={(text) => setDateOfBirth(text)}
                        />
                        <InputWithIcon
                            label="Địa chỉ"
                            placeholder="Phùng Hưng, Hà Đông, HN"
                            iconName="map-marker"
                            value={address}
                            onChangeText={(text) => setAddress(text)}
                        />
                        <ButtonWithIcon
                            onPress={handleButton}
                            text="Tiếp tục"
                            iconName="chevron-right"
                        />
                    </ScrollView>
                </KeyboardAvoidingView>
            </View>

        </SafeAreaView>
    )
}
export default UpdateProfile;
const styles = StyleSheet.create({
    btn_go_back: {
        width: 40,
        height: 40,
        borderRadius: 12,
        borderWidth: 1,
        borderColor: COLORS.grayblue,
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: SIZES.padding
    },
    btn_go_back_icon: {
        width: 22,
        height: 22,
        tintColor: COLORS.black
    }
})
