import React, { useState, useContext } from 'react';
import {
    View,
    Text,
    SafeAreaView,
    TouchableOpacity,
    Image,
    KeyboardAvoidingView,
    ScrollView,
    StyleSheet
} from 'react-native';
import { COLORS, FONTS, icons, SIZES, images } from '../../shared/constants';
import Input from '../../shared/Input';
import Button from '../../shared/Button';
import { AuthContext } from '../../navigators/AuthProvider';
import Toast from 'react-native-toast-message';
import ButtonLoading from '../../shared/ButtonLoading';
const SignUp = (props) => {
    const [email, setEmail] = useState(null);
    const [password, setPassword] = useState(null);
    const [confirmPwd, setConfirmPwd] = useState(null);
    const [role, setRole] = useState(0);
    //loading when click login button
    const [isLoading, setIsLoading] = useState(false);
    const { register } = useContext(AuthContext);

    const handleRegister = () => {
        setIsLoading(true);
        if (password !== confirmPwd) {
            Toast.show({
                topOffset: 30,
                type: 'error',
                text1: 'Mật khẩu xác nhận không trùng khớp'
            })

            setIsLoading(false);
        }
        if (password == null || confirmPwd == null || email == null) {
            setIsLoading(false);
            Toast.show({
                topOffset: 30,
                type: 'error',
                text1: 'Vui lòng nhập đầy đủ thông tin',
            });
            setIsLoading(false);

        } else {
            register(email, password,role);
            setIsLoading(false);
        }
    }

    return (
        <SafeAreaView style={styles.container}>
            <TouchableOpacity
                onPress={() => props.navigation.navigate("Login")}
                style={styles.btn_goBack}
            >
                <Image
                    source={icons.icon_back}
                    style={styles.btn_icon}
                    resizeMode="contain"
                />
            </TouchableOpacity>
            <View style={{
                flex: 1,
                padding: SIZES.padding
            }}>
                <Text style={{ ...FONTS.h1, color: COLORS.black }}>Đăng ký</Text>
                <TouchableOpacity style={{ flexDirection: 'row' }}
                    onPress={() => props.navigation.navigate("Login")}
                >
                    <Text style={{ ...FONTS.body1, color: COLORS.grayblue }}>Bạn đã có tài khoản?</Text>
                    <Text style={{ ...FONTS.body1, color: COLORS.primary }}>  Đăng nhập</Text>
                </TouchableOpacity>
                <KeyboardAvoidingView style={{ flex: 1 }}>
                    <ScrollView style={{ marginTop: 20 }}
                        showsVerticalScrollIndicator={false}
                        bounces={false}
                    >
                        <Input
                            value={email}
                            onChangeText={(text) => setEmail(text)}
                            keyboardType="email-address"
                            label="Email"
                            placeholder="nqduc74@gmail.com"
                        />
                        <Input
                            label="Mật khẩu"
                            placeholder="Nhập mật khẩu"
                            secureTextEntry={true}
                            value={password}
                            onChangeText={(text) => setPassword(text)}
                        />
                        <Input
                            label="Xác nhận mật khẩu"
                            placeholder="Nhập mật khẩu xác nhận"
                            secureTextEntry={true}
                            value={confirmPwd}
                            onChangeText={(text) => setConfirmPwd(text)}
                        />
                        <View style={{
                            marginBottom: 20
                        }}>
                            <Text style={{ ...FONTS.h4, color: COLORS.black }}>Bạn muốn làm gì?</Text>
                            <View style={{
                                flexDirection: 'row'
                            }}>
                                <TouchableOpacity
                                    onPress={() => setRole(0)}
                                    style={[styles.btn_role, {
                                        backgroundColor: role === 0? COLORS.primary : COLORS.background
                                    }]}>
                                    <Text style={{ ...FONTS.body1 }}>Kiếm tiền freelance</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    onPress={() => setRole(1)}
                                    style={[styles.btn_role, {
                                        backgroundColor: role === 1 ? COLORS.primary : COLORS.background
                                    }]}>
                                    <Text style={{ ...FONTS.body }}>Thuê freelancer</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        {
                            !isLoading ? (
                                <Button
                                    onPress={handleRegister}
                                    text="Đăng ký"
                                />
                            ) : (
                                <ButtonLoading />
                            )
                        }

                        <Text style={{ ...FONTS.body2, color: COLORS.grayblue, textAlign: 'center' }}>Đăng ký bằng tài khoản mạng xã hội</Text>
                        <View style={styles.role}>
                            <TouchableOpacity style={styles.btn_facebook}>
                                <Image
                                    source={images.facebook}
                                    style={styles.social_icon}
                                    resizeMode="contain"
                                />
                                <Text style={{ ...FONTS.h5, color: COLORS.white }}>Facebook</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.btn_google}>
                                <Image
                                    source={images.google}
                                    style={styles.social_icon}
                                    resizeMode="contain"
                                />
                                <Text style={{ ...FONTS.h5, color: COLORS.white }}>Google</Text>
                            </TouchableOpacity>

                        </View>

                        <View style={{
                            marginTop: 20
                        }}>
                            <Text style={{ ...FONTS.body1, color: COLORS.grayblue, textAlign: 'center' }}>Bằng cách nhấp chuột bằng Bạn đồng ý với</Text>
                            <Text style={{ ...FONTS.body1, color: COLORS.primary, textAlign: 'center' }}> Thỏa thuận người dùng và chính sách riêng tư.</Text>

                        </View>

                    </ScrollView>
                </KeyboardAvoidingView>
            </View>

        </SafeAreaView >
    )
}
export default SignUp;
const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingVertical: 40
    },
    btn_goBack: {
        width: 40,
        height: 40,
        borderRadius: 12,
        borderWidth: 1,
        borderColor: COLORS.grayblue,
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: SIZES.padding
    },
    btn_icon: {
        width: 22,
        height: 22,
        tintColor: COLORS.black
    },
    btn_facebook: {
        alignItems: 'center',
        backgroundColor: '#3b5999',
        width: 170,
        height: 50,
        borderRadius: 16,
        justifyContent: 'center',
        flexDirection: 'row'
    },
    btn_google: {
        alignItems: 'center',
        backgroundColor: '#4181eb',
        width: 170,
        height: 50,
        borderRadius: 16,
        justifyContent: 'center',
        flexDirection: 'row'
    },
    social_icon: {
        width: 20,
        height: 20,
        tintColor: COLORS.white,
        marginHorizontal: 10
    },
    btn_role: {
        width: 150,
        height: 40,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: COLORS.primary,
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 20,
    },
    role: {
        flexDirection: 'row',
        marginTop: 20,
        justifyContent: 'space-between',
        alignItems: 'center'
    }
})