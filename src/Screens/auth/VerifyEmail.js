import React, { useEffect } from 'react';
import {
    View,
    Text,
    SafeAreaView,
    TouchableOpacity,
    Image
} from 'react-native';
import { COLORS, FONTS, icons, SIZES, images } from '../../shared/constants';
import ButtonWithIcon from '../../shared/ButtonWithIcon';
import { AuthContext } from '../../navigators/AuthProvider';
import { useContext } from 'react';


const VerifyEmail = () => {
    const { verifyEmail, logout } = useContext(AuthContext);
    return (
        <SafeAreaView style={{
            flex: 1,
            paddingTop: 40
        }}>
            <TouchableOpacity
                onPress={() => logout()}
                style={{
                    width: 40,
                    height: 40,
                    borderRadius: 12,
                    borderWidth: 1,
                    borderColor: COLORS.grayblue,
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginLeft: SIZES.padding
                }}
            >
                <Image
                    source={icons.icon_back}
                    style={{
                        width: 22,
                        height: 22,
                        tintColor: COLORS.black
                    }}
                    resizeMode="contain"
                />
            </TouchableOpacity>
            <View style={{
                flex: 1,
                padding: SIZES.padding
            }}>
                <Text style={{ ...FONTS.h1, color: COLORS.black, textAlign: 'center' }}>Xác thực Email</Text>

                <Text style={{ ...FONTS.body1, color: COLORS.grayblue, top: 30, textAlign: 'center' }}>Vui lòng kiểm tra hộp thư email của bạn để hoàn tất đăng ký.</Text>


                <View style={{
                    marginTop: 20,
                    paddingHorizontal: SIZES.padding * 2
                }}>

                    <ButtonWithIcon
                        text="Xác nhận"
                        iconName="chevron-right"
                        onPress={() => verifyEmail()}
                    />
                    <TouchableOpacity style={{
                        flexDirection: 'row'
                    }}>
                        <Text style={{ ...FONTS.body1, color: COLORS.grayblue }}>Bạn chưa nhận được thư? </Text>
                        <Text style={{ ...FONTS.body1, color: COLORS.primary }}> Gửi lại </Text>
                    </TouchableOpacity>
                </View>
            </View>

        </SafeAreaView>
    )
}
export default VerifyEmail;