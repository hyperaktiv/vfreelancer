import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    FlatList,
    Image,
    TouchableOpacity,
    SafeAreaView,
    ActivityIndicator,
    Animated,
    Alert
} from 'react-native';
import { FONTS, COLORS, SIZES, icons } from '../../shared/constants';
import * as firebase from 'firebase';
import { auth, firestore } from 'firebase';
import Swipeable from 'react-native-gesture-handler/Swipeable';
import Toast from 'react-native-toast-message';
const ProjectManager = (props) => {
    const [tabSelected, setTabSelected] = useState('upload');
    const [jobUpload, setJobUpload] = useState([]);
    const [jobApply, setJobApply] = useState([]);
    const [loading, setLoading] = useState(false);
    //lấy danh sách những người ứng tuyển cho job  này
    const fetchJobUpload = async () => {
        try {
            setLoading(true);
            let jobUploadArray = [];
            await firebase.firestore()
                .collection('JobApply')
                .where("idUserPost", "==", firebase.auth().currentUser.uid)
                .get()
                .then(query => {
                    query.forEach(doc => {
                        const { jobId, jobName, userApplyId, userApplyName, type, budget_apply, note_apply } = doc.data();
                        jobUploadArray.push({
                            id: doc.id,
                            jobId,
                            jobName,
                            userApplyId,
                            userApplyName,
                            type,
                            note_apply,
                            budget_apply
                        });
                    });
                });
            let jobApplyArray = [];
            await firebase.firestore()
                .collection('JobApply')
                .where("userApplyId", "==", firebase.auth().currentUser.uid)
                .get()
                .then(query => {
                    query.forEach(doc => {
                        const { idUserPost, jobId, jobName, userApplyId, userApplyName, type, budget_apply, note_apply } = doc.data();
                        jobApplyArray.push({
                            id: doc.id,
                            jobId,
                            jobName,
                            userApplyId,
                            userApplyName,
                            type,
                            idUserPost,
                            note_apply,
                            budget_apply
                        });
                    });
                })
            setLoading(false);
            setJobUpload(jobUploadArray);
            setJobApply(jobApplyArray);
        } catch (e) {
            console.log(e);
        }
    }
    useEffect(() => {
        fetchJobUpload();
    }, [])
    const approveUser = async (userApplyId, jobId, type, id) => {
        if (type == 'approved' || type == 'done') {
            Toast.show({
                topOffset: 60,
                type: 'error',
                text1: 'Job này đã được duyệt rồi!'
            })
        } else {
            await firebase.firestore()
                .collection('posts')
                .doc(jobId)
                .update({
                    type: 'inprogress',
                    userApplyId
                })
                .then(() => {
                    firebase.firestore()
                        .collection('JobApply')
                        .doc(id)
                        .update({
                            type: 'approved'
                        })
                        .then(async () => {
                            await firebase.firestore()
                                .collection('notifications')
                                .add({
                                    userId: userApplyId,
                                    message: `${firebase.auth().currentUser.displayName} đã phê duyệt đơn ứng tuyển của bạn.`,
                                    created_at: new Date(),
                                    seen: false
                                })
                                .then(() => {
                                    Toast.show({
                                        topOffset: 60,
                                        type: 'success',
                                        text1: 'Phê duyệt thành công!'
                                    })
                                })
                        })

                });
        }
    }
    const renderItem = ({ item }) => {
        return (
            <Swipeable
                renderRightActions={(progress, dragX) => {
                    const scale = dragX.interpolate({
                        inputRange: [-100, 0],
                        outputRange: [1, 0],
                        extrapolate: 'clamp'
                    });
                    return (
                        <View style={{
                            flexDirection: 'row'
                        }}>
                            <TouchableOpacity
                                style={{
                                    shadowColor: "#000",
                                    shadowOffset: {
                                        width: 0,
                                        height: 1,
                                    },
                                    shadowOpacity: 0.20,
                                    shadowRadius: 1.41,
                                    elevation: 2,
                                    backgroundColor: '#fff',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    width: 80,
                                    borderRadius: 8,
                                    marginVertical: 8,
                                    marginHorizontal: 5,

                                }}
                                onPress={() => approveUser(item.userApplyId, item.jobId, item.type, item.id)}>
                                <Animated.Text style={{ ...FONTS.h5, color: COLORS.primary, transform: [{ scale: scale }] }}>Duyệt</Animated.Text>

                            </TouchableOpacity>
                        </View>
                    )
                }}
            >
                <View style={{
                    alignSelf: 'center',
                    backgroundColor: '#fff',
                    borderRadius: 5,
                    width: SIZES.width - SIZES.padding * 2 - 4,
                    shadowColor: "#000",
                    shadowOffset: {
                        width: 0,
                        height: 1,
                    },
                    shadowOpacity: 0.20,
                    shadowRadius: 1.41,
                    elevation: 2,
                    marginVertical: 5,
                    paddingHorizontal: SIZES.padding,
                    paddingVertical: 5
                }}>
                    <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                        <Text style={{ ...FONTS.body1, color: COLORS.black }}>Tên dự án: </Text>
                        <Text style={{ ...FONTS.body1, color: COLORS.black }}>{item.jobName.toUpperCase()}</Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={{ ...FONTS.body1, color: COLORS.black }}>{item.type == 'notapprove' ? 'Người ứng tuyển: ' : 'Người làm: '}</Text>
                        <Text
                            onPress={() => props.navigation.navigate("User", { id: item.userApplyId })}
                            style={{ ...FONTS.body1, color: COLORS.primary }}>{item.userApplyName}</Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={{ ...FONTS.body1, color: COLORS.black }}>Ngân sách mong muốn:</Text>
                        <Text style={{ ...FONTS.body1, color: COLORS.primary }}> {item.budget_apply}$</Text>
                    </View>
                    <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                        <Text style={{ ...FONTS.body1, color: COLORS.black }}>Ghi chú: </Text>
                        <Text numberOfLines={5} style={{ ...FONTS.body1, color: COLORS.primary }}> {item.note_apply}</Text>
                    </View>

                </View>
            </Swipeable>
        )
    }
    const JobUpload = () => {
        return (
            <FlatList
                data={jobUpload}
                keyExtractor={item => item.jobId}
                extraData
                renderItem={renderItem}
            />
        )
    }
    const handleProject = (type, id, idUserPost) => {
        if (type == 'notapprove') {
            Toast.show({
                topOffset: 60,
                type: 'error',
                text1: 'Bạn chưa được chấp nhận làm dự án này!'
            })
        } if (type == 'done') {
            Toast.show({
                topOffset: 60,
                type: 'error',
                text1: 'Dự án này đã hoàn thành!'
            });
        } else {
            Alert.alert(
                "Bạn đã hoàn thành dự án?",
                "Thông  báo với chủ dự án.",
                [
                    {
                        text: "Yes",
                        onPress: async () => {

                            await firebase.firestore().collection('JobApply')
                                .doc(id)
                                .update({
                                    type: 'done',
                                    result: 'ontime'
                                })
                                .then(async () => {
                                    await firebase.firestore()
                                        .collection('notifications')
                                        .add({
                                            userId: idUserPost,
                                            message: `${firebase.auth().currentUser.displayName} đã hoàn thành dự án của bạn.`,
                                            created_at: new Date(),
                                            seen: false
                                        })
                                        .then(() => {
                                            Toast.show({
                                                topOffset: 60,
                                                type: 'success',
                                                text1: 'Chúc mừng bạn hoàn thành dự án!'
                                            })
                                        })
                                })
                        }
                    },
                    {
                        text: "Cancel",
                        onPress: () => Alert.alert("Hãy nhớ hoàn thành dự án đúng hạn!"),
                        style: "cancel",
                    },
                ],
                {
                    cancelable: true
                }
            )
        }
    }
    const _renderItem = ({ item }) => {
        console.log(item.idUserPost)
        return (
            <Swipeable
                renderRightActions={(progress, dragX) => {
                    const scale = dragX.interpolate({
                        inputRange: [-100, 0],
                        outputRange: [1, 0],
                        extrapolate: 'clamp'
                    });
                    return (
                        <View style={{
                            flexDirection: 'row'
                        }}>
                            <TouchableOpacity
                                style={{
                                    shadowColor: "#000",
                                    shadowOffset: {
                                        width: 0,
                                        height: 1,
                                    },
                                    shadowOpacity: 0.20,
                                    shadowRadius: 1.41,
                                    elevation: 2,
                                    backgroundColor: '#fff',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    width: 80,
                                    borderRadius: 8,
                                    marginVertical: 8,
                                    marginHorizontal: 5,

                                }}
                                onPress={() => handleProject(item.type, item.id, item.idUserPost)}>
                                <Animated.Text style={{ ...FONTS.h5, color: COLORS.primary, transform: [{ scale: scale }] }}>Xong</Animated.Text>

                            </TouchableOpacity>
                        </View>
                    )
                }}
            >
                <View style={{
                    alignSelf: 'center',
                    backgroundColor: '#fff',
                    borderRadius: 5,
                    width: SIZES.width - SIZES.padding * 2 - 4,
                    shadowColor: "#000",
                    shadowOffset: {
                        width: 0,
                        height: 1,
                    },
                    shadowOpacity: 0.20,
                    shadowRadius: 1.41,
                    elevation: 2,
                    marginVertical: 5,
                    paddingHorizontal: SIZES.padding,
                    paddingVertical: 5
                }}>
                    <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                        <Text style={{ ...FONTS.body1, color: COLORS.black }}>Tên dự án: </Text>
                        <Text style={{ ...FONTS.body1, color: COLORS.black }}>{item.jobName.toUpperCase()}</Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={{ ...FONTS.body1, color: COLORS.black }}>{item.type == 'notapprove' ? 'Người ứng tuyển: ' : 'Người làm: '}</Text>
                        <Text style={{ ...FONTS.body1, color: COLORS.primary }}>{item.userApplyName}</Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={{ ...FONTS.body1, color: COLORS.black }}>Ngân sách mong muốn:</Text>
                        <Text style={{ ...FONTS.body1, color: COLORS.primary }}> {item.budget_apply}$</Text>
                    </View>
                    <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                        <Text style={{ ...FONTS.body1, color: COLORS.black }}>Ghi chú: </Text>
                        <Text numberOfLines={5} style={{ ...FONTS.body1, color: COLORS.primary }}> {item.note_apply}</Text>
                    </View>

                </View>
            </Swipeable>
        )
    }
    const JobApply = () => {
        return (
            <FlatList
                data={jobApply}
                keyExtractor={item => item.jobId}
                extraData
                renderItem={_renderItem}
            />
        )
    }
    return (

        <SafeAreaView style={{
            flex: 1,
            backgroundColor: COLORS.background,
            paddingTop: 25
        }}>
            <View style={{
                flexDirection: 'row',
                paddingHorizontal: SIZES.padding,
                marginBottom: 10,
                justifyContent: 'space-between',
                alignItems: 'center',
            }}>
                <TouchableOpacity
                    onPress={() => props.navigation.goBack()}
                    style={{
                        borderWidth: 1,
                        padding: 5,
                        borderColor: COLORS.grayblue,
                        borderRadius: 5
                    }}
                >
                    <Image
                        source={icons.icon_back}
                        style={{
                            width: 20,
                            height: 20,
                            tintColor: COLORS.black
                        }}
                        resizeMode="contain"
                    />

                </TouchableOpacity>
                <Text style={{ ...FONTS.h3, color: COLORS.black, textAlign: 'center' }}>Quản lý dự án</Text>
                <TouchableOpacity
                    style={{
                        borderWidth: 1,
                        padding: 5,
                        borderColor: COLORS.primary,
                        borderRadius: 5
                    }}
                >
                    <Image
                        source={icons.icon_setting}
                        style={{
                            width: 20,
                            height: 20,
                            tintColor: COLORS.primary
                        }}
                        resizeMode="contain"
                    />
                </TouchableOpacity>
            </View>
            {/* body */}
            {
                loading ? (
                    <ActivityIndicator animating size="large" color={COLORS.primary} />
                ) : (
                    <View style={{
                        paddingHorizontal: SIZES.padding,
                        paddingVertical: 0
                    }}>
                        <View style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            backgroundColor: '#fafafa',
                        }}>
                            <TouchableOpacity
                                onPress={() => setTabSelected('upload')}
                                style={{
                                    width: (SIZES.width - SIZES.padding * 3) / 2,
                                    height: 40,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    borderColor: tabSelected == 'upload' ? COLORS.primary : COLORS.background,
                                    borderBottomWidth: 3,
                                }}>
                                <Text style={{ ...FONTS.h5, color: COLORS.black }}>ĐÃ ĐĂNG</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => setTabSelected('apply')}
                                style={{
                                    width: (SIZES.width - SIZES.padding * 3) / 2,
                                    height: 40,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    borderColor: tabSelected == 'apply' ? COLORS.primary : COLORS.background,
                                    borderBottomWidth: 3,
                                }}>
                                <Text style={{ ...FONTS.h5, color: COLORS.black }}>ĐÃ ỨNG TUYỂN</Text>
                            </TouchableOpacity>
                        </View>

                        <View
                            style={{
                                marginTop: 20
                            }}>
                            {
                                tabSelected == 'upload' ? <JobUpload /> : <JobApply />
                            }
                        </View>
                    </View>
                )
            }

        </SafeAreaView>
    )
}
export default ProjectManager;