import React from 'react';
import {
    View,
    Text,
    Image,
    Animated,
    TouchableOpacity

} from 'react-native';
import { COLORS, FONTS, icons } from '../../shared/constants';
import Swipeable from 'react-native-gesture-handler/Swipeable';
const Card = ({ navigation }) => {

    const rightSwipe = (progress, dragX) => {
        const scale = dragX.interpolate({
            inputRange: [-100, 0],
            outputRange: [1, 0],
            extrapolate: 'clamp'
        });
        return (
            <View style={{
                flexDirection: 'row'
            }}>
                <TouchableOpacity
                    style={{
                        shadowColor: "#000",
                        shadowOffset: {
                            width: 0,
                            height: 1,
                        },
                        shadowOpacity: 0.20,
                        shadowRadius: 1.41,
                        elevation: 2,
                        backgroundColor: '#fff',
                        justifyContent: 'center',
                        alignItems: 'center',
                        width: 80,
                        borderRadius: 8,
                        marginVertical: 8,
                        marginHorizontal: 5,
                        
                    }}
                    onPress={() => console.log('delete')}>
                        <Animated.Text style={{...FONTS.body1,color:COLORS.black,transform:[{scale:scale}]}}>Duyệt</Animated.Text>
                        <Animated.Image
                        source={icons.icon_trash}
                        style={{
                            transform: [{ scale: scale }],
                            width: 25,
                            height: 25,
                            tintColor: 'red'
                        }} />
                </TouchableOpacity>
                <TouchableOpacity
                    style={{
                        shadowColor: "#000",
                        shadowOffset: {
                            width: 0,
                            height: 1,
                        },
                        shadowOpacity: 0.20,
                        shadowRadius: 1.41,
                        elevation: 2,
                        
                        backgroundColor: '#fff',
                        justifyContent: 'center',
                        alignItems: 'center',
                        width: 80,
                        borderRadius: 8,
                        marginVertical: 8,
                        marginHorizontal: 5
                    }}
                    onPress={() => console.log('delete')}>
                        <Animated.Text style={{...FONTS.body1,color:COLORS.black,transform:[{scale:scale}]}}>Delete</Animated.Text>
                    <Animated.Image
                        source={icons.icon_trash}
                        style={{
                            transform: [{ scale: scale }],
                            width: 25,
                            height: 25,
                            tintColor: 'red'
                        }} />
                </TouchableOpacity>
            </View>


        )
    }
    return (
        <Swipeable
            renderRightActions={rightSwipe}
        >
            <TouchableOpacity
                style={{
                    marginHorizontal: 3,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    paddingHorizontal: 20,
                    paddingVertical: 10,
                    backgroundColor: '#fff',
                    borderRadius: 8,
                    marginVertical: 8,
                    shadowColor: "#000",
                    shadowOffset: {
                        width: 0,
                        height: 1,
                    },
                    shadowOpacity: 0.20,
                    shadowRadius: 1.41,
                    elevation: 2,
                }}
                onPress={() => navigation.navigate("Conversation")}>

                <View style={{
                    flexDirection: 'row'
                }}>
                    <Image
                        source={{
                            uri: 'https://taichinh.online/wp-content/uploads/2017/02/the-mastercard.png'
                        }}
                        resizeMode="contain"
                        style={{
                            width: 70,
                            height: 55,
                            borderRadius: 12
                        }}
                    />
                    <View style={{
                        marginLeft: 10,
                        justifyContent: 'center',
                        alignItems: 'flex-start'
                    }}>
                        <Text style={{ ...FONTS.h5, color: COLORS.black }}>Master Card</Text>
                        <Text style={{ ...FONTS.body1, color: COLORS.text }}>xxxx - xxxx - xxxx - 1234</Text>
                    </View>
                </View>
            </TouchableOpacity>
        </Swipeable>
    )
}
export default Card;