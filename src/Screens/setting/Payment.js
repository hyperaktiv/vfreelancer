import React, { useState } from 'react';
import {
    View,
    Text,
    FlatList,
    Image,
    TouchableOpacity
} from 'react-native';
import ButtonWithIcon from '../../shared/ButtonWithIcon';
import { FONTS, COLORS, SIZES, icons } from '../../shared/constants';
import Card from './Card';
const Payment = (props) => {
    const [listCard] = useState([
        {
            "card":"duc",
            "id":"2"
        },
        {
            "card":"duc",
            "id":"1"
        },
        {
            "card":"duc",
            "id":"3"
        }
    ]);
    

    return (

        <View style={{
            flex: 1,
            backgroundColor: COLORS.background,
            paddingTop: 30
        }}>
            <View style={{
                flexDirection: 'row',
                paddingHorizontal: SIZES.padding,
                marginVertical: 10,
                justifyContent: 'space-between',
                alignItems: 'center',
            }}>
                <TouchableOpacity
                    onPress={() => props.navigation.goBack()}
                    style={{
                        borderWidth: 1,
                        padding: 5,
                        borderColor: COLORS.grayblue,
                        borderRadius: 5
                    }}
                >
                    <Image
                        source={icons.icon_back}
                        style={{
                            width: 20,
                            height: 20,
                            tintColor: COLORS.black
                        }}
                        resizeMode="contain"
                    />

                </TouchableOpacity>
                <Text style={{ ...FONTS.h3, color: COLORS.black, textAlign: 'center' }}>Phương thức thanh toán</Text>
                <TouchableOpacity
                    style={{
                        borderWidth: 1,
                        padding: 5,
                        borderColor: COLORS.primary,
                        borderRadius: 5
                    }}
                >
                    <Image
                        source={icons.icon_plus2}
                        style={{
                            width: 20,
                            height: 20,
                            tintColor: COLORS.primary
                        }}
                        resizeMode="contain"
                    />
                </TouchableOpacity>
            </View>
            {/* body */}
            <View style={{
                paddingHorizontal: SIZES.padding,
                paddingVertical: 0
            }}>
                

                {
                    listCard
                        ? (
                            <FlatList
                                data={listCard}
                                renderItem={(item, index) => {
                                    return (
                                        <Card navigation={props.navigation} key={item.id} />
                                    )
                                }}
                                style={{
                                    marginBottom: 40
                                }}
                                showsVerticalScrollIndicator={false}
                            />
                        )
                        : (
                            <View style={{
                                flex: 1,
                                marginHorizontal: 30
                            }}>
                                <Image
                                    source={icons.icon_payment}
                                    style={{
                                        width: 200,
                                        height: 200,
                                        tintColor: COLORS.grayblue,
                                        alignSelf:'center'
                                    }}
                                />
                                <Text style={{ ...FONTS.h5, color: COLORS.black, textAlign: 'center' }}>Bạn chưa có phương thức thanh toán nào.</Text>
                                <Text style={{ ...FONTS.body1, color: COLORS.grayblue, textAlign: 'center' }}>Vui lòng thêm phương thức thanh toán để tiến hành giao dịch. Cảm ơn!</Text>
                                    <ButtonWithIcon 
                                        text="Thêm thanh toán mới"
                                        iconName="credit-card"
                                        // onPress={}
                                    />
                            </View>
                        )
                }

            </View>
        </View>
    )
}
export default Payment;