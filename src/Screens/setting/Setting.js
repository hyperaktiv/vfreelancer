import React, { useState, useContext, useEffect, useCallback } from 'react';
import {
    SafeAreaView,
    Text,
    View,
    Image,
    TouchableOpacity,
    ScrollView,
    Switch,
    RefreshControl,
    StyleSheet
} from 'react-native';
import { COLORS, FONTS, SIZES, icons, images } from '../../shared/constants';
import { AuthContext } from '../../navigators/AuthProvider';

const Setting = (props) => {

    const [isEnabled, setIsEnabled] = useState(false);
    const toggleSwitch = () => setIsEnabled(previousState => !previousState);
    const { logout, user } = useContext(AuthContext);
    const [refreshing, setRefreshing] = useState(false);
    const wait = async (timeout) => {
        return await (resolve => setTimeout(resolve, timeout));
    }
    const onRefresh = useCallback(() => {
        setRefreshing(true);
        wait(3000).then(() => {
            setRefreshing(false)
        });
    }, []);
    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: COLORS.background }}>
            {/* phần user */}
            <View style={styles.user_container}>
                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center'
                }}>
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Image
                            source={{
                                uri: user.photoURL ? user.photoURL : 'https://randomuser.me/api/portraits/men/78.jpg'
                            }}
                            style={styles.user_image}
                            resizeMode="contain"
                        />
                    </View>
                    <View style={{ marginLeft: -30, justifyContent: 'center', alignItems: 'flex-start' }}>
                        <Text style={{ ...FONTS.body2, color: COLORS.black }}>{user.email}</Text>
                        <Text style={{ ...FONTS.h5, color: COLORS.black }}>{user.displayName}</Text>
                        <Text style={{ ...FONTS.body1, color: COLORS.primary, fontStyle: 'italic' }}>Đã xác thực</Text>
                    </View>
                    <TouchableOpacity
                        onPress={() => props.navigation.navigate("Userprofile")}
                        style={styles.btn_go_profile}
                    >
                        <Image
                            source={icons.icon_forward}
                            style={styles.icon_btn_go_profile}
                            resizeMode="contain"
                        />
                    </TouchableOpacity>
                </View >
            </View >

            {/* từ cài đặt - > đăng xuất */}
            < ScrollView
                refreshControl={
                    <RefreshControl
                        refreshing={refreshing}
                        onRefresh={onRefresh}
                    />
                }
                contentContainerStyle={{ paddingBottom: 50 }}>
                <View style={styles.section}>
                    <TouchableOpacity
                        onPress={() => props.navigation.navigate("Payment")}
                        style={[styles.sectiton_element, {
                            borderBottomColor: '#f3f3f3',
                            borderBottomWidth: 1,
                        }]}
                    >

                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Image
                                source={icons.icon_payment}
                                style={styles.sectiton_element_image}
                                resizeMode="contain"
                            />
                            <Text style={{ ...FONTS.h5, color: COLORS.black }}>Phương thức thanh toán</Text>
                        </View>
                        <Image
                            source={icons.icon_forward}
                            style={styles.sectiton_element_icon}
                            resizeMode="contain"
                        />
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => props.navigation.navigate("ProjectManager")}
                        style={[styles.sectiton_element, {
                            borderBottomColor: '#f3f3f3',
                            borderBottomWidth: 1,
                        }]}
                    >

                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Image
                                source={icons.icon_job}
                                style={styles.sectiton_element_image}
                                resizeMode="contain"
                            />
                            <Text style={{ ...FONTS.h5, color: COLORS.black }}>Quản lý dự án</Text>
                        </View>
                        <Image
                            source={icons.icon_forward}
                            style={styles.sectiton_element_icon}
                            resizeMode="contain"
                        />
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={[styles.sectiton_element, {
                            borderBottomColor: '#f3f3f3',
                            borderBottomWidth: 1,
                        }]}
                    >
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Image
                                source={icons.icon_mode}
                                style={styles.sectiton_element_image}
                                resizeMode="contain"
                            />
                            <Text style={{ ...FONTS.h5, color: COLORS.black }}>Chế độ</Text>
                        </View>
                        <Switch
                            trackColor={{ false: "#767577", true: COLORS.primary }}
                            thumbColor={isEnabled ? COLORS.primary : "#f4f3f4"}
                            ios_backgroundColor="#3e3e3e"
                            onValueChange={toggleSwitch}
                            value={isEnabled}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.sectiton_element}
                    >
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Image
                                source={icons.icon_about}
                                style={styles.sectiton_element_image}
                                resizeMode="contain"
                            />
                            <Text style={{ ...FONTS.h5, color: COLORS.black }}>Về chúng tôi</Text>
                        </View>
                        <Image
                            source={icons.icon_forward}
                            style={styles.sectiton_element_icon}
                            resizeMode="contain"
                        />
                    </TouchableOpacity>
                </View>
                <View style={[styles.section, { height: 180 }]}>
                    <TouchableOpacity
                        style={[styles.sectiton_element, {
                            borderBottomColor: '#f3f3f3',
                            borderBottomWidth: 1,
                        }]}
                    >
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Image
                                source={icons.icon_help}
                                style={styles.sectiton_element_image}
                                resizeMode="contain"
                            />
                            <Text style={{ ...FONTS.h5, color: COLORS.black }}>Liên hệ & Hỗ trợ</Text>
                        </View>
                        <Image
                            source={icons.icon_forward}
                            style={styles.sectiton_element_icon}
                            resizeMode="contain"
                        />
                    </TouchableOpacity>
                    <TouchableOpacity
                        // onPress={}
                        style={[styles.sectiton_element, {
                            borderBottomColor: '#f3f3f3',
                            borderBottomWidth: 1,
                        }]}
                    >
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Image
                                source={icons.icon_privacy}
                                style={styles.sectiton_element_image}
                                resizeMode="contain"
                            />
                            <Text style={{ ...FONTS.h5, color: COLORS.black }}>Chính sách riêng tư</Text>
                        </View>
                        <Image
                            source={icons.icon_forward}
                            style={styles.sectiton_element_icon}
                            resizeMode="contain"
                        />

                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => logout()}
                        style={styles.sectiton_element}
                    >
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Image
                                source={icons.icon_signout}
                                style={styles.sectiton_element_image}
                                resizeMode="contain"
                            />
                            <Text style={{ ...FONTS.h5, color: COLORS.black }}>Đăng xuất</Text>
                        </View>
                        <Image
                            source={icons.icon_forward}
                            style={styles.sectiton_element_icon}
                            resizeMode="contain"
                        />
                    </TouchableOpacity>
                </View>
            </ScrollView >
        </SafeAreaView >
    )
}
export default Setting;
const styles = StyleSheet.create({
    user_container: {
        marginHorizontal: SIZES.padding,
        backgroundColor: '#fff',
        borderRadius: 16,
        paddingHorizontal: 20,
        paddingVertical: 20,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
    },
    user_image: {
        width: 75,
        height: 75,
        borderRadius: 12
    },
    btn_go_profile: {
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 12,
        backgroundColor: COLORS.primary,
        width: 35,
        height: 35,
    },
    icon_btn_go_profile: {
        width: 20,
        height: 20,
        tintColor: '#fff',
    },
    section: {
        marginTop: 30,
        marginHorizontal: SIZES.padding,
        backgroundColor: '#fff',
        height: 240,
        borderRadius: 16,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
    },
    sectiton_element: {
        flexDirection: 'row',
        height: 60,
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    sectiton_element_icon: {
        width: 10,
        height: 10,
        tintColor: COLORS.primary,
        marginHorizontal: 20
    },
    sectiton_element_image: {
        width: 20,
        height: 20,
        tintColor: COLORS.primary,
        marginHorizontal: 20
    }
})