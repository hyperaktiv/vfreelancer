import React, { useState, useEffect } from 'react';
import {
    Text,
    View,
    SafeAreaView,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView
} from 'react-native';
import Toast from 'react-native-toast-message';
import { COLORS, SIZES, FONTS } from '../../shared/constants';
import ButtonSelected from '../../shared/ButtonSelected';
import Input from '../../shared/Input';
import * as firebase from 'firebase';
import { auth, firestore } from '../../firebase';
const JobUploadStep3 = ({ navigation, route }) => {
    const { jobName, jobDescription, jobImage, skills } = route.params;
    const [jobBudget, setJobBudget] = useState(null);
    const [jobPayment, setJobPayment] = useState(null);
    const [jobDeadline, setJobDeadline] = useState(null);
    const [loading, setLoading] = useState(false);

    const saveData = async (url) => {
        await firestore.collection('posts')
            .add({
                //id của người đăng bài
                idUserPost: auth.currentUser.uid,
                //tên của người đăng bài
                nameUserPost: auth.currentUser.displayName,
                postTime: new Date(),
                jobName,
                jobDescription,
                jobImage: url,
                skills: skills,
                jobBudget,
                jobPayment,
                jobDeadline,
                type: 'open'
            })
            .then(() =>

                navigation.navigate("JobUpload", {
                    idUserPost: auth.currentUser.uid,
                    nameUserPost: auth.currentUser.displayName,
                    jobName,
                    jobDescription,
                    jobImage: url,
                    skills: skills,
                    jobBudget,
                    jobPayment,
                    jobDeadline
                }))
            .catch(e => console.log(e))
    }
    const goNext = async () => {
        if (jobBudget == null || jobPayment == null || jobDeadline == null) {
            Toast.show({
                topOffset: 60,
                type: 'error',
                text1: 'Vui lòng hoàn thành thông tin!'
            })
        } else {
            if (jobImage == null) {
                saveData(null);
            } else {
                const blob = await new Promise((resolve, reject) => {
                    const xhr = new XMLHttpRequest();
                    xhr.onload = function () {
                        resolve(xhr.response);
                    };
                    xhr.onerror = function () {
                        reject(new TypeError('Network request failed'));
                    };
                    xhr.responseType = 'blob';
                    xhr.open('GET', jobImage, true);
                    xhr.send(null);
                });
                const ref = firebase.storage().ref().child(new Date().toISOString())
                const snapshot = ref.put(blob);
                snapshot.on(
                    firebase.storage.TaskEvent.STATE_CHANGED,
                    () => {
                        setLoading(true)
                    },
                    (error) => {
                        setLoading(false);
                        blob.close();
                        return null;
                    },
                    () => {
                        snapshot.snapshot.ref.getDownloadURL().then((url) => {
                            setLoading(false);
                            saveData(url);
                            blob.close();
                            return url;
                        });
                    });

            }
        }

    }
    return (
        <SafeAreaView style={{
            flex: 1,
            backgroundColor: COLORS.background
        }}>
            <View style={{
                flex: 1,
                paddingHorizontal: SIZES.padding
            }}>
                {/* header */}

                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                }}>
                    <TouchableOpacity
                        onPress={() => navigation.goBack()}

                        style={{
                            marginHorizontal: 10
                        }}>
                        <Text style={{ ...FONTS.h4, color: COLORS.primary }}>Back</Text>
                    </TouchableOpacity>
                    <Text style={{ ...FONTS.h4, color: COLORS.black }}>Bước 3/3</Text>
                    <TouchableOpacity
                        onPress={goNext}
                        style={{
                            marginHorizontal: 10
                        }}>
                        <Text style={{ ...FONTS.h4, color: COLORS.primary }}>Done</Text>
                    </TouchableOpacity>
                </View>
                {/* upload photo */}
                <KeyboardAvoidingView style={{
                    flex: 1
                }}>
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                        contentContainerStyle={{
                            paddingBottom: 80
                        }}
                    >
                        <View style={{ flex: 1, marginBottom: 20 }}>
                            <View style={{
                                marginTop: 10
                            }}>
                                <Text style={{ ...FONTS.h5, color: COLORS.black }}>HÌNH THỨC CHI TRẢ</Text>
                                <ButtonSelected
                                    checked={jobPayment == 'Trả theo giá cố định' ? true : false}
                                    onPress={() => setJobPayment('Trả theo giá cố định')}
                                    text="Trả theo giá cố định"
                                />
                                <ButtonSelected
                                    checked={jobPayment == 'Trả theo giờ' ? true : false}
                                    onPress={() => setJobPayment('Trả theo giờ')}
                                    text="Trả theo giờ"
                                />
                            </View>
                            <View style={{
                                marginTop: 10
                            }}>
                                <Text style={{ ...FONTS.h5, color: COLORS.black, bottom: -10 }}>NGÂN SÁCH DỰ KIẾN</Text>
                                <Input
                                    placeholder="Ngân sách dự kiến của bạn"
                                    value={jobBudget}
                                    onChangeText={(text) => setJobBudget(text)}
                                />
                            </View>
                            <View style={{
                                marginTop: 10
                            }}>
                                <Text style={{ ...FONTS.h5, color: COLORS.black, bottom: -10 }}>THỜI GIAN HOÀN THÀNH DỰ KIẾN</Text>
                                <Input
                                    placeholder="Thời gian hoàn thành dự kiến"
                                    onChangeText={(text) => setJobDeadline(text)}
                                    value={jobDeadline}
                                />
                            </View>

                        </View>
                    </ScrollView>
                </KeyboardAvoidingView>
            </View>
        </SafeAreaView>
    )
};
export default JobUploadStep3;
