import React , { useEffect, useState }from 'react';
import {
    Text,
    View,
    SafeAreaView,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
} from 'react-native';
import { COLORS, FONTS, SIZES } from '../../shared/constants';
import JobDetails from '../jobdetail/JobDetails';
import JobDescription from '../jobdetail/JobDescription';
import Toast from 'react-native-toast-message';

const JobUpload = ({ navigation, route }) => {
    const { jobName,
        idUserPost,
        nameUserPost,
        jobDescription,
        jobImage,
        skills,
        jobBudget,
        jobPayment,
        jobDeadline} = route.params;
        const postTime = new Date();
        let array = [];
        skills.forEach((doc) =>
            array.push(doc.name)
        );
    useEffect(()=>{
        Toast.show({
            topOffset:60,
            type:'success',
            text1:'Đăng tin thành công!'
        })
    },[]);
    
    return (
        <SafeAreaView style={{
            flex: 1,
            backgroundColor: COLORS.background
        }}>
            <View style={{
                flex: 1,
                
            }}>
                <View style={{
                    paddingHorizontal: SIZES.padding,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    marginVertical: 15
                }}>
                    <Text style={{ ...FONTS.h4, color: COLORS.black }}>THÔNG TIN DỰ ÁN</Text>
                    <TouchableOpacity
                        onPress={() => navigation.navigate("JobUploadStep1")}
                        style={{
                            marginHorizontal: 10
                        }}>
                        <Text style={{ ...FONTS.h5, color: COLORS.primary }}>New</Text>
                    </TouchableOpacity>
                </View>
                {/* upload photo */}
                <KeyboardAvoidingView style={{
                    flex: 1
                }}>
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                        contentContainerStyle={{
                            paddingBottom: 80
                        }}
                    >
                        <JobDetails 
                            jobName={jobName}
                            skills={array}
                            jobBudget={jobBudget}
                            jobImage={jobImage}
                            //postTime={postTime}
                            
                        />
                        <JobDescription 
                            jobDescription={jobDescription}
                            skills={array}
                            jobBudget={jobBudget}
                            jobDeadline={jobDeadline}
                            jobPayment={jobPayment}
                            nameUserPost={nameUserPost}
                        />
                    </ScrollView>

                </KeyboardAvoidingView>

            </View>
        </SafeAreaView>
    )
}
export default JobUpload;