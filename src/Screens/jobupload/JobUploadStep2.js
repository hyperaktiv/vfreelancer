import React, { useState, useEffect } from 'react';
import {
    Text,
    View,
    SafeAreaView,
    TouchableOpacity,
    FlatList,
    ActivityIndicator,
    StyleSheet
} from 'react-native';
import Toast from 'react-native-toast-message';
import { COLORS, SIZES, FONTS } from '../../shared/constants';
import Icon from 'react-native-vector-icons/FontAwesome'
import * as firebase from 'firebase';
const JobUploadStep2 = ({ navigation, route }) => {
    const { jobName, jobDescription, jobImage } = route.params;

    const [selected, setSelected] = useState(null);
    const [parentSkillList, setParentSkillList] = useState([]);
    const [childrenSkillList, setChildrenSkillList] = useState([]);

    const [loading, setLoading] = useState(false);
    const [loading2, setLoading2] = useState(false);
    const [refresh, setRefresh] = useState(false);
    const [skills, setSkills] = useState([]);
    const [allSkills, setAllSkills] = useState(null);
    const fetchSkillList = async () => {
        try {
            setLoading(true);
            const listParentSkill = [];
            const listChildrenSkill = [];
            await firebase.firestore()
                .collection('TypeOfSkill')
                .get()
                .then((query) => {
                    query.forEach(doc => {
                        //lấy ra tên phân loại kỹ năng ( type ở trong TypeOfSkill )
                        const { type } = doc.data();

                        listParentSkill.push({
                            id: doc.id,
                            type
                        });
                    })
                });
            await firebase.firestore()
                .collection('Children_skill')
                .get()
                .then((query) => {
                    query.forEach(doc => {
                        //lấy ra tên phân loại kỹ năng ( type ở trong TypeOfSkill )
                        const { name, parent_skill_id } = doc.data();

                        listChildrenSkill.push({
                            id: doc.id,
                            name,
                            isSelected: false,
                            parent_skill_id
                        });
                    })
                })
            setLoading(false);
            setParentSkillList(listParentSkill);
            setChildrenSkillList(listChildrenSkill)
        } catch (e) {
            console.log(e);
        }
    }

    const _getChildrenSkill = (id) => {

        setLoading2(true);
        setSelected(id);
        const _list = childrenSkillList;
        const list = [];
        _list.forEach((item) => {
            if (item.parent_skill_id == id) {
                list.push(item)
            }
        })
        setLoading2(false);
        setAllSkills(list);
    }

    const selectedItem = (skill) => {
        skill.isSelected = !skill.isSelected;
        let array = [];
        const index = childrenSkillList.findIndex(
            item => item.id === skill.id
        );
        childrenSkillList[index] === skill;
        array = childrenSkillList;
        setChildrenSkillList(array);
        setRefresh(!refresh);
    }
    useEffect(() => {
        fetchSkillList();
    }, []);

    const goNext = () => {

        let array = [];
        let _array = [];
        array = childrenSkillList;
        if (array != null) {
            array.forEach((item) => {
                if (item.isSelected == true) {
                    _array.push(item)
                }
            })
        }
        else {
            Toast.show({
                topOffset: 30,
                type: 'error',
                text1: 'Vui lòng chọn kỹ năng của bạn!',
            });
        }

        if (_array.length == 0) {
            Toast.show({
                topOffset: 30,
                type: 'error',
                text1: 'Vui lòng chọn lĩnh vực của bạn',
            });
        } else {
            setSkills(_array);
            navigation.navigate("JobUploadStep3", {
                jobName,
                jobDescription,
                jobImage,
                skills: _array
            });
        }
    };
    const renderItem = ({ item }) => {
        return (
            <TouchableOpacity
                key={item.id}
                style={selected == item.id ? styles.btn_category_active : styles.btn_category_inactive}
                onPress={() => _getChildrenSkill(item.id)}
            >
                <Text style={{ ...FONTS.body1, color: COLORS.black, textAlign: 'center', marginRight: 8 }}>{item.type}</Text>
            </TouchableOpacity>
        )
    }
    const _renderItem = ({ item }) => {
        return (
            <TouchableOpacity
                key={item.id}
                style={styles.btn_child_category}
                onPress={() => selectedItem(item)}
            >
                <Text style={{ ...FONTS.body1, color: COLORS.black, marginRight: 8 }}>{item.name}</Text>
                <Icon name={item.isSelected ? "check" : "plus"} size={15} color={COLORS.primary} />
            </TouchableOpacity>
        )
    }
    return (
        <SafeAreaView style={{
            flex: 1,
            backgroundColor: COLORS.background
        }}>
            <View style={{
                flex: 1,
                paddingHorizontal: SIZES.padding
            }}>
                {/* header */}

                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                }}>
                    <TouchableOpacity
                        onPress={() => navigation.goBack()}

                        style={{
                            marginHorizontal: 10
                        }}>
                        <Text style={{ ...FONTS.h4, color: COLORS.primary }}>Back</Text>
                    </TouchableOpacity>
                    <Text style={{ ...FONTS.h4, color: COLORS.black }}>Bước 2/3</Text>
                    <TouchableOpacity
                        onPress={goNext}
                        style={{
                            marginHorizontal: 10
                        }}>
                        <Text style={{ ...FONTS.h4, color: COLORS.primary }}>Next</Text>
                    </TouchableOpacity>
                </View>
                <View style={{
                    flex: 1
                }}>
                    <Text style={{ ...FONTS.body1, color: COLORS.grayblue }}>Chọn kỹ năng yêu cầu của bạn để được đề xuất ứng viên phù hợp.</Text>
                    <Text style={{ ...FONTS.body1, color: COLORS.black, marginVertical: 5 }}>Chọn phân loại</Text>
                    <View style={{ height: 60 }}>
                        {
                            loading ? (
                                <ActivityIndicator size="large" color={COLORS.primary} animating={true} />
                            ) : (
                                <FlatList
                                    showsHorizontalScrollIndicator={false}
                                    horizontal
                                    data={parentSkillList}
                                    renderItem={renderItem}
                                />
                            )
                        }

                    </View>
                    {
                        selected ? (
                            <Text style={{ ...FONTS.body1, color: COLORS.black, marginVertical: 5 }}>Chọn lĩnh vực</Text>
                        )
                            : (
                                null
                            )
                    }
                    {
                        loading2 ? (
                            <ActivityIndicator size="large" color={COLORS.primary} animating={true} />
                        ) : (
                            <FlatList
                                data={allSkills}
                                renderItem={_renderItem}
                                showsVerticalScrollIndicator={false}
                                extraData={refresh}
                            />
                        )
                    }
                </View>
            </View>
        </SafeAreaView>
    )
};
export default JobUploadStep2;
const styles = StyleSheet.create({
    btn_category_active: {
        borderWidth: 1,
        borderColor: COLORS.primary,
        backgroundColor: COLORS.white,
        borderRadius: 8,
        paddingHorizontal: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: 5,
        marginVertical: 5,
        height: 40
    },
    btn_category_inactive: {
        borderWidth: 1,
        borderColor: COLORS.border,
        backgroundColor: COLORS.white,
        borderRadius: 8,
        paddingHorizontal: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: 5,
        marginVertical: 5,
        height: 40
    },
    btn_child_category: {
        borderWidth: 1,
        borderColor: COLORS.border,
        backgroundColor: COLORS.white,
        borderRadius: 8,
        paddingHorizontal: 20,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginHorizontal: 5,
        marginVertical: 5,
        height: 40
    }
})
