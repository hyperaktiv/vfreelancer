import React, { useState, useEffect } from 'react';
import {
    Text,
    View,
    SafeAreaView,
    TouchableOpacity,
    Image,
    KeyboardAvoidingView,
    ScrollView,
    TextInput,
    StyleSheet
} from 'react-native';
import Toast from 'react-native-toast-message';
import { COLORS, SIZES, icons, FONTS } from '../../shared/constants';
import Input from '../../shared/Input';
import * as ImagePicker from 'expo-image-picker';
const JobUploadStep1 = (props) => {

    const [jobName, setJobName] = useState(null);
    const [jobDescription, setJobDescription] = useState(null);
    const [jobImage, setJobImage] = useState(null);
    // const [uploading, setUploading] = useState(false);

    useEffect(() => {
        (async () => {
            if (Platform.OS !== 'web') {
                const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
                if (status !== 'granted') {
                    console.log('Sorry, we need camera roll permissions to make this work!');
                }
            }
        })();
    }, []);
    const choosePhotoFromLibrary = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: true,
            aspect: [1, 1],
            quality: 1,
        });
        if (!result.cancelled) {
            console.log(result.uri);
            setJobImage(result.uri);
        }
    };

    const goNext = () => {
        if (jobName == null || jobDescription == null) {
            Toast.show({
                topOffset: 60,
                type: 'error',
                text1: 'Vui lòng điền đầy đủ thông tin'
            })
        } else {
            props.navigation.navigate("JobUploadStep2", {
                jobName,
                jobDescription,
                jobImage: jobImage
            });
        }
    }

    return (
        <SafeAreaView style={{
            flex: 1,
            backgroundColor: COLORS.background
        }}>
            <View style={{
                flex: 1,
                paddingHorizontal: SIZES.padding
            }}>
                {/* header */}

                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                }}>
                    <View style={{ width: 10 }} />
                    <Text style={{ ...FONTS.h4, color: COLORS.black }}>Bước 1/3</Text>
                    <TouchableOpacity
                        onPress={goNext}
                        style={{ marginHorizontal: 10 }}>
                        <Text style={{ ...FONTS.h4, color: COLORS.primary }}>Next</Text>
                    </TouchableOpacity>
                </View>
                {/* upload photo */}
                <KeyboardAvoidingView style={{
                    flex: 1
                }}>
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                        contentContainerStyle={{
                            paddingBottom: 80
                        }}
                    >
                        <View style={{ flex: 1, marginBottom: 20 }}>
                            <TouchableOpacity
                                onPress={() => choosePhotoFromLibrary()}
                                style={styles.btn_add_cover}
                            >
                                {
                                    jobImage ? (
                                        <Image
                                            source={{ uri: jobImage }}
                                            style={styles.image}
                                            resizeMode="contain"
                                        />) : (
                                        <View>
                                            <Image
                                                source={icons.icon_gallery}
                                                style={styles.image_add}
                                                resizeMode="contain"
                                            />
                                            <Text style={{ ...FONTS.h5, color: COLORS.black }}>Thêm ảnh bìa</Text>
                                        </View>
                                    )
                                }

                            </TouchableOpacity>

                            {/* body */}

                            <Input
                                label="TÊN DỰ ÁN"
                                placeholder="Thiết kế giao diện website"
                                value={jobName}
                                onChangeText={(text) => setJobName(text)}
                            />
                            <Text style={{ ...FONTS.h5, color: COLORS.black, marginVertical: 3 }}>MÔ TẢ</Text>
                            <TextInput
                                value={jobDescription}
                                onChangeText={(text) => setJobDescription(text)}
                                placeholder="Hãy bắt đầu bằng một chút thông tin về bạn hay dịch vụ của bạn, và bao gồm khái quát về công việc bạn cần hoàn thành."
                                multiline
                                maxLength={3500}
                                numberOfLines={12}
                                style={styles.text_input}
                            />
                        </View>
                    </ScrollView>
                </KeyboardAvoidingView>
            </View>
        </SafeAreaView>
    )
};
export default JobUploadStep1;
const styles = StyleSheet.create({
    image: {
        width: SIZES.width - SIZES.padding * 2 - 10,
        height: 145,
        alignSelf: 'center',
        borderRadius: 16
    },
    image_add: {
        width: 100,
        height: 40,
        tintColor: COLORS.grayblue
    },
    text_input: {
        borderRadius: 8,
        borderColor: COLORS.border,
        borderWidth: 1,
        paddingHorizontal: 20,
        backgroundColor: '#fff',
        ...FONTS.body1,
        paddingVertical: 10,
        textAlignVertical: 'top'
    },
    btn_add_cover: {
        width: SIZES.width - SIZES.padding * 2,
        height: 150,
        borderWidth: 2,
        borderColor: COLORS.grayblue,
        borderStyle: 'dashed',
        position: 'relative',
        borderRadius: 16,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 15
    }
})