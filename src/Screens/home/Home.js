import React, { useEffect, useState, useCallback } from 'react'
import {
   View,
   Image,
   TouchableOpacity,
   SafeAreaView,
   FlatList,
   ActivityIndicator,
   Keyboard,
   RefreshControl
} from 'react-native'

import Icon from 'react-native-vector-icons/FontAwesome';
import { COLORS, SIZES, icons, images } from '../../shared/constants';
import JobComponent from './JobComponent';
import SearchComponent from './SearchComponent';
const ImageWidth = 200;
const scaleImg = 561 / 61;
import SearchedJob from './SearchedJob';
import { auth, firestore } from '../../firebase';
const Home = (props) => {
   const user = auth.currentUser;
   const [listJob, setListJob] = useState([]);
   const [allJob, setAllJob] = useState([]);
   const [searchedJob, setSearchedJob] = useState();
   const [focus, setFocus] = useState(false);
   const [key, setKey] = useState('');
   const [loading, setLoading] = useState(true);
   const [refreshing, setRefreshing] = useState(false);
   const fetchSkillUser = async () => {
      await firestore.collection('users')
         .get()
         .then(async query => {
            let newArray = [];
            query.forEach(doc => {
               const { skill, userId } = doc.data();
               if (userId == user.uid) {
                  newArray = skill;
               }
            });
            let array = [];
            await firestore.collection('posts')
               // .where('skills', 'array-contains-any', newArray)
               .orderBy('postTime', 'desc')
               .get()
               .then((query) => {
                  query.forEach((doc) => {
                     const { skills, postTime, jobBudget, jobImage, jobName, idUserPost } = doc.data();
                     let _array = [];
                     skills.forEach((doc) =>
                        _array.push(doc.name)
                     );
                     if (idUserPost != auth.currentUser.uid) {
                        array.push({
                           id: doc.id,
                           postTime,
                           jobName,
                           jobImage,
                           skills: _array.toLocaleString(),
                           jobBudget,
                        })
                     }
                  })
               })
               .catch(e => console.log(e));
            setListJob(array);
            setLoading(false);
            setRefreshing(false);
         });
   }
   const fetchAllJob = async () => {
      let allJob = [];
      await firestore.collection('posts')
         .orderBy('postTime', 'desc')
         .get()
         .then(query => {
            query.forEach((doc) => {
               const { skills, postTime, jobBudget, jobImage, jobName } = doc.data();
               let array = [];
               skills.forEach((doc) =>
                  array.push(doc.name)
               );
               allJob.push({
                  id: doc.id,
                  postTime,
                  jobName,
                  jobImage,
                  skills: array,
                  jobBudget,
               });
            });
            setAllJob(allJob);
            setRefreshing(false);
            setLoading(false);
         })
   }
   useEffect(() => {
      fetchSkillUser();
      fetchAllJob();
   }, []);
   const onRefresh = () => {
      setRefreshing(true);
      fetchSkillUser();
      fetchAllJob()
   };
   const onBlur = () => {
      setKey('');
      setFocus(false);
      Keyboard.dismiss();
   }
   const openList = () => {
      setFocus(true);
   }
   const searchJob = (text) => {
      setKey(text);
      setSearchedJob(
         allJob.filter(i =>
            i.jobName.toLowerCase().includes(text.toLowerCase())
            || i.skills.toLocaleString().toLowerCase().includes(text.toLowerCase())
         )
      );
   }
   const renderItem = ({ item }) => {
      return (
         <JobComponent
            navigation={props.navigation}
            item={item}
         />
      )
   }
   return (
      <SafeAreaView style={{
         flex: 1,
         backgroundColor: COLORS.background
      }}>
         <View style={{
            paddingHorizontal: SIZES.padding,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center'
         }}>
            <View style={{ width: 25 }} />
            <Image style={{ width: ImageWidth, height: ImageWidth / scaleImg }}
               source={images.logo_name} resizeMode="contain" />
            <TouchableOpacity style={{
               justifyContent: 'center',
               alignItems: 'center'
            }}
               onPress={() => props.navigation.navigate("Notification")}
            >
               <Image
                  style={{ width: 30, height: 30, tintColor: COLORS.primary }}
                  resizeMode="contain"
                  source={icons.icon_notification} />
            </TouchableOpacity>
         </View>

         <View style={{ paddingHorizontal: SIZES.padding }}>
            <SearchComponent
               onChangeText={(text) => searchJob(text)}
               onBlur={onBlur}
               focus={focus}
               onFocus={openList}
               defaultValue={key}
            />
         </View>
         {
            focus == true ? (
               <SearchedJob
                  navigation={props.navigation}
                  listJob={searchedJob}
               />
            ) : (
               <View style={{
                  flex: 1,
                  paddingHorizontal: SIZES.padding,
                  // paddingBottom: 100
               }}>
                  {loading ? (
                     <ActivityIndicator animating size="large" color={COLORS.primary} />
                  ) : (
                     <FlatList
                        refreshControl={
                           <RefreshControl
                              refreshing={refreshing}
                              onRefresh={onRefresh}
                           />
                        }
                        data={listJob}
                        extraData
                        renderItem={renderItem}
                        keyExtractor={item => item.id}
                     />
                  )}
               </View>
            )
         }
      </SafeAreaView>
   )
}
export default Home
