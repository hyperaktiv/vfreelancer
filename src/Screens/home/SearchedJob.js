import React from 'react';
import {
    View,
    Text,
    FlatList
} from 'react-native';
import { COLORS, FONTS, SIZES } from '../../shared/constants';
import JobComponent from './JobComponent';
const SearchedJob = ({ navigation, listJob }) => {
    const renderItem = ({item}) => {
        return (
            <JobComponent 
                navigation={navigation}
                item={item}
            />
        )
    }
    return (
        <View style={{
            flex: 1,
            paddingHorizontal: SIZES.padding
        }}>
            <Text style={{...FONTS.h4,color:COLORS.black}}>Kết quả tìm kiếm:</Text>
            {
                listJob ? (
                    <FlatList 
                        contentContainerStyle={{
                            paddingBottom:100
                        }}
                        style={{
                            flex:1
                        }}
                        data={listJob}
                        keyExtractor={item=>item.id}
                        renderItem={renderItem}
                        
                    />
                ) : null
            }
        </View>
    )
};
export default SearchedJob;