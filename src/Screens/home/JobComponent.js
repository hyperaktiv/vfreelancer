import React from 'react'
import {
   View,
   Image,
   Text,
   TouchableOpacity,
   Dimensions,
   StyleSheet
} from 'react-native';
import { COLORS, FONTS, images, SIZES } from '../../shared/constants';
import Icon from 'react-native-vector-icons/FontAwesome';
import { capitalizeFirstLetter, formatDateAndTime } from '../../shared/utils';
var width = Dimensions.get('window').width;

const JobComponent = ({ navigation, item, inDetails = false }) => {
   const { jobImage, jobName, id, postTime, skills, jobBudget } = item;

   return (
      <TouchableOpacity style={[styles.view, {
         width: inDetails == false ? width - SIZES.padding * 2 - 4 : width * 0.8,
      }]}
         onPress={() => navigation.navigate("Jobdetails", { id: id })}
      >
         <View style={{
            flexDirection: 'row',
            alignItems: 'center'
         }}>
            {
               jobImage == undefined || jobImage == null ? (
                  <Image
                     source={images.logo}
                     style={{
                        width: 30,
                        height: 30,
                        borderRadius: 25
                     }}
                     resizeMode="contain"
                  />
               ) : (
                  <Image
                     style={{
                        width: 30,
                        height: 30,
                        borderRadius: 25
                     }}
                     source={require('../../../assets/images/logo.png')}
                     resizeMode="contain" />
               )
            }
            <Text style={{
               marginLeft: 20,
               ...FONTS.h4,
               color: COLORS.black,
            }}>{capitalizeFirstLetter(jobName)}</Text>
         </View>
         <View style={{ flex: 1 }}>
            <View style={{
               flex: 1,
               flexDirection: 'row',
               marginTop: 5,
               flexWrap: 'wrap',
               alignItems: 'flex-start'
            }}>
               <Text style={{ ...FONTS.body1, color: COLORS.grayblue }}>Kỹ năng: </Text>
               <Text style={{
                  ...FONTS.body1,
                  color: COLORS.black,
               }}>{skills.toLocaleString()}</Text>
            </View>
            {/* location text */}
            <View style={{
               flexDirection: 'row',
               marginTop: 5,
            }}>
               <Text style={{ ...FONTS.body1, color: COLORS.grayblue }}>Ngân sách:</Text>
               <Text style={{
                  ...FONTS.body1,
                  color: COLORS.primary,
               }}> ${jobBudget}
               </Text>
            </View>
            <View style={{
               flexDirection: 'row',
               marginTop: 5,
               alignItems: 'center'
            }}>
               <Icon name="clock-o" size={15} color={COLORS.grayblue} />
               <Text style={{
                  ...FONTS.body2,
                  color: COLORS.grayblue,
                  marginLeft: 12
               }}>Đăng lúc {formatDateAndTime(postTime.toDate())}</Text>
            </View>
         </View>
      </TouchableOpacity>
   )
}

export default JobComponent
const styles = StyleSheet.create({
   view: {
      marginVertical: 5,
      borderRadius: 8,
      paddingVertical: 10,
      backgroundColor: '#fff',


      shadowColor: "#000",
      shadowOffset: {
         width: 0,
         height: 1,
      },
      shadowOpacity: 0.20,
      shadowRadius: 1.41,
      elevation: 2,

      paddingHorizontal: 20,
      flex: 1,
      alignSelf: 'center'
   }
})