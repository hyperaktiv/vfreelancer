import React from 'react'
import { Image, TextInput, View, Dimensions, TouchableOpacity } from 'react-native'
import { COLORS, FONTS, SIZES, icons } from '../../shared/constants';


var width = Dimensions.get('window').width;

const SearchComponent = (props) => {
   return (
      <View style={{
         backgroundColor: '#fff',
         marginVertical: 20
      }}>
         <View style={{
            flexDirection: 'row',
            alignItems: 'center'
         }}>
            <View style={{
               flexDirection: 'row',
               width: width - SIZES.padding * 2,
               height: 50,
               alignItems: 'center',
               borderRadius: 5,
               backgroundColor: '#fff',
               shadowColor: "#000",
               shadowOffset: {
                  width: 0,
                  height: 1,
               },
               shadowOpacity: 0.20,
               shadowRadius: 1.41,
               elevation: 2,
            }}>
               <Image
                  style={{ marginLeft: 15, width: 26, height: 26, tintColor: COLORS.primary }}
                  source={icons.icon_search} />
               <TextInput
                  placeholder={'Tìm kiếm việc làm'}
                  onFocus={props.onFocus}
                  onChangeText={props.onChangeText}
                  defaultValue={props.defaultValue}
                  placeholderTextColor={COLORS.grayblue}
                  style={{
                     // marginBottom: 15,
                     paddingLeft: 10,
                     height: 35,
                     width: width * 0.7,
                     ...FONTS.body1,
                     color: COLORS.black,
                  }}
               />
               {
                  props.focus ? (
                     <TouchableOpacity onPress={props.onBlur}>
                        <Image 
                           source={icons.icon_close}
                           style={{
                              width:15,
                              height:15,
                              tintColor:COLORS.primary,
                              

                           }}
                           resizeMode="contain"
                        />
                     </TouchableOpacity>
                  ) : null
               }
            </View>
         </View>
      </View>
   )
}

export default SearchComponent
