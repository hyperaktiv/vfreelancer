import React, { useState, useEffect, useContext, useCallback } from 'react';
import {
    SafeAreaView,
    Text,
    View,
    Image,
    TouchableOpacity,
    FlatList,
    ActivityIndicator,
    RefreshControl,
    StyleSheet
} from 'react-native';
import { COLORS, FONTS, icons, SIZES } from '../../shared/constants';
import MessageItem from './MessageItem';
import * as firebase from 'firebase';
import { AuthContext } from '../../navigators/AuthProvider';

const Message = (props) => {
    const { user } = useContext(AuthContext);
    const [listMessage, setListMessage] = useState(null);
    const [loading, setLoading] = useState(true);
    const [refreshing, setRefreshing] = useState(false);
    //lấy toàn bộ tin nhắn của người dùng từ server
    const fetchMessages = async () => {
        let arrayMess = [];
        try {
            await firebase.firestore()
                .collection('chats')
                .get()
                .then((query) => {
                    query.forEach((doc) => {
                        //user_one
                        //user_two
                        const { lastedMessage, user_one_id, user_one_name, user_two_id, user_two_name, user_one_image, user_two_image } = doc.data();
                        if (user_one_id == firebase.auth().currentUser.uid
                            || user_two_id == firebase.auth().currentUser.uid) {
                            arrayMess.push({
                                lastedMessage,
                                chatId: doc.id,
                                senderName: firebase.auth().currentUser.uid === user_one_id ? user_two_name : user_one_name,
                                senderId: firebase.auth().currentUser.uid === user_one_id ? user_two_id : user_one_id,
                                senderImage: firebase.auth().currentUser.uid === user_one_id ? user_two_image : user_one_image,
                                whoSender: firebase.auth().currentUser.uid === user_one_id ? 'userTwo' : 'userOne'
                            })
                        }
                    })
                })
                .catch(e => console.log(e));
            setLoading(false);
            setListMessage(arrayMess);
            setRefreshing(false);
        } catch (e) { console.log(e) };
    }
    useEffect(() => {
        fetchMessages();
    }, [])
    const onRefresh = () => {
        setRefreshing(true);
        fetchMessages();
    };
    const renderItem = ({ item }) => {
        return (
            <MessageItem item={item} navigation={props.navigation} />
        )
    }
    return (
        <SafeAreaView style={{
            flex: 1,
            backgroundColor: COLORS.background
        }}>
            <View style={styles.page_header}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Image
                        source={{
                            uri: user.photoURL ? user.photoURL : 'https://randomuser.me/api/portraits/men/78.jpg'
                        }}
                        style={styles.image}
                        resizeMode="contain"
                    />
                    <Text style={styles.page_header_title}>TIN NHẮN</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                    <TouchableOpacity
                        onPress={() => props.navigation.navigate("Newchat")}
                    >
                        <Image
                            source={icons.icon_plus2}
                            style={styles.page_header_icon}
                        />
                    </TouchableOpacity>
                </View>
            </View>
            <View style={{
                paddingHorizontal: SIZES.padding,
                paddingVertical: 0
            }}>
                {
                    loading ? (
                        <ActivityIndicator size="large" animating={true} color={COLORS.primary} />
                    ) : (
                        <FlatList
                            refreshControl={
                                <RefreshControl
                                    refreshing={refreshing}
                                    onRefresh={onRefresh}
                                />
                            }
                            data={listMessage}
                            renderItem={renderItem}
                            style={{
                                marginBottom: 40
                            }}
                            showsVerticalScrollIndicator={false}
                            keyExtractor={(item) => item.chatId}
                        />
                    )
                }
            </View>
        </SafeAreaView>
    )
}
export default Message;
const styles = StyleSheet.create({
    image: {
        width: 30,
        height: 30,
        borderRadius: 50
    },
    page_header: {
        paddingHorizontal: SIZES.padding,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    page_header_title: {
        ...FONTS.h3,
        color: COLORS.black,
        marginLeft: 20,
        textAlign: 'center'
    },
    page_header_icon: {
        width: 20,
        height: 20,
        tintColor: COLORS.primary,
        marginHorizontal: 15
    }
})

