import React from 'react';
import {
    View,
    Text,
    Image
} from 'react-native';
import { COLORS, FONTS } from '../../shared/constants';
import { formatTime } from '../../shared/utils';

const Received = ({data}) => {
    const time = formatTime(data.createdAt.toDate());
    return (
        <View style={{
            flexDirection:'row',
            marginTop:20,
            width:250,
            
        }}>
            <Image 
                source={{uri :data.senderImage}}
                style={{
                    width:35,
                    height:35,
                    borderRadius:50
                }}
                resizeMode="contain"
            />
            <View style={{ marginLeft:10}}>
            <View style={{
                backgroundColor:'#2675ec',
                maxWidth:220,
                alignItems:'center',
                justifyContent:'center',
                paddingHorizontal:20,
                paddingVertical:10,
                borderTopRightRadius:25,
                borderBottomLeftRadius:25,
                borderBottomRightRadius:25,
            }}>
                <Text style={{...FONTS.body1,color:COLORS.white}}>{data.message}</Text>
                
            </View>
            <Text style={{...FONTS.body2,color:COLORS.grayblue,alignSelf:'flex-start',marginTop:5}}>{time}</Text>
            </View>
            
            

        </View>
    )
}
export default Received;