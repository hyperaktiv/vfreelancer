import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    Image,
    SafeAreaView,
    ActivityIndicator,
    FlatList,
    TextInput,
    Keyboard,
    StyleSheet
} from 'react-native';
import { COLORS, SIZES, icons, FONTS } from '../../shared/constants';
import Toast from 'react-native-toast-message';
import * as firebase from 'firebase';
import Icon from 'react-native-vector-icons/FontAwesome';
const NewChat = (props) => {

    const [listUser, setListUser] = useState([]);
    const [loading, setLoading] = useState(false);
    const [focus, setFocus] = useState(false);
    const [key, setKey] = useState('');
    const [searchedUser, setSearchedUser] = useState([]);
    const fetchListUser = async () => {
        setLoading(true);
        let array = [];
        await firebase.firestore()
            .collection('users')
            .get()
            .then((query) => {
                query.forEach((doc) => {
                    const { email, name, userId, userImage } = doc.data();
                    array.push({
                        id: doc.id,
                        email,
                        userId,
                        name,
                        userImage
                    })
                })
            });
        setListUser(array);
        setLoading(false);
    }
    useEffect(() => {
        fetchListUser();
    }, [])
    const renderItem = ({ item }) => {
        const _handleNavigate = async () => {
            await firebase.firestore()
                .collection('chats')
                .get()
                .then(async (query) => {
                    let isExists = false;
                    let _chatID = '';
                    query.forEach((doc) => {
                        const { user_one_id, user_two_id } = doc.data();
                        if (((user_one_id == item.userId) && (user_two_id == firebase.auth().currentUser.uid))
                            || ((user_two_id == item.userId) && (user_one_id == firebase.auth().currentUser.uid))
                        ) {
                            isExists = true;
                            _chatID = doc.id;
                        }
                    });
                    if (isExists == true) {
                        props.navigation.navigate("Conversation", {
                            senderName: item.name,
                            image: item.userImage ? item.userImage : 'https://randomuser.me/api/portraits/men/78.jpg',
                            chatId: _chatID,
                            senderId: item.userId,
                            whoSender: 'userOne'
                        })

                    } else {
                        console.log(item.name);
                        await firebase.firestore()
                            .collection('chats')
                            .add({
                                createdAt: new Date(),
                                lastedMessage: `Bắt đầu nói chuyện với ${item.name}`,
                                user_one_id: firebase.auth().currentUser.uid,
                                user_one_name: firebase.auth().currentUser.displayName,
                                user_one_image: firebase.auth().currentUser.photoURL ? firebase.auth().currentUser.photoURL : 'https://randomuser.me/api/portraits/men/78.jpg',
                                user_two_id: item.userId,
                                user_two_name: item.name,
                                user_two_image: item.userImage ? item.userImage : 'https://randomuser.me/api/portraits/men/78.jpg',
                            })
                            .then((docRefId) => props.navigation.navigate("Conversation", {
                                senderName: item.name,
                                image: item.userImage ? item.userImage : 'https://randomuser.me/api/portraits/men/78.jpg',
                                chatId: docRefId.id,
                                senderId: item.userId,
                                whoSender: 'userOne'
                            }));
                    }
                })


        }

        return (
            <TouchableOpacity
                onPress={() => _handleNavigate()}
                style={{
                    flexDirection: 'row',
                    marginVertical: 10,
                    alignItems: 'center',
                    marginLeft: 10,
                }}
            >
                <Image
                    source={{
                        uri: item.userImage ? item.userImage : 'https://randomuser.me/api/portraits/men/65.jpg'
                    }}
                    resizeMode="contain"
                    style={{
                        width: 30,
                        height: 30,
                        borderRadius: 25,
                        marginRight: 10
                    }}

                />
                <Text style={{ ...FONTS.h5, color: COLORS.black }}>{item.name}</Text>
            </TouchableOpacity>
        )
    }
    const _renderItem = ({ item }) => {

        const handleNavigate = async () => {
            let isExists = false;
            let _chatID = '';
            await firebase.firestore()
                .collection('chats')
                .get()
                .then(async (query) => {

                    query.forEach((doc) => {
                        const { user_one_id, user_two_id } = doc.data();
                        if (((user_one_id == item.userId) && (user_two_id == firebase.auth().currentUser.uid))
                            || ((user_two_id == item.userId) && (user_one_id == firebase.auth().currentUser.uid))
                        ) {
                            isExists = true;
                            _chatID = doc.id;
                        }
                    });
                    if (isExists == true) {
                        props.navigation.navigate("Conversation", {
                            senderName: item.name,
                            image: item.userImage ? item.userImage : 'https://randomuser.me/api/portraits/men/78.jpg',
                            chatId: _chatID,
                            senderId: item.userId,
                            whoSender: 'userOne'
                        })

                    } else {
                        await firebase.firestore()
                            .collection('chats')
                            .add({
                                createdAt: new Date(),
                                lastedMessage: `Bắt đầu nói chuyện với ${item.name}`,
                                user_one_id: firebase.auth().currentUser.uid,
                                user_one_name: firebase.auth().currentUser.displayName,
                                user_one_image: firebase.auth().currentUser.photoURL ? firebase.auth().currentUser.photoURL : 'https://randomuser.me/api/portraits/men/78.jpg',
                                user_two_id: item.userId,
                                user_two_name: item.name,
                                user_two_image: item.userImage ? item.userImage : 'https://randomuser.me/api/portraits/men/78.jpg',
                            })
                            .then((docRefId) => props.navigation.navigate("Conversation", {
                                senderName: item.name,
                                image: item.userImage ? item.userImage : 'https://randomuser.me/api/portraits/men/78.jpg',
                                chatId: docRefId.id,
                                senderId: item.userId,
                                whoSender: 'userOne'
                            }));
                    }
                })


        }

        return (
            <TouchableOpacity
                onPress={() => handleNavigate()}
                style={{
                    flexDirection: 'row',
                    marginVertical: 10,
                    alignItems: 'center',
                    marginLeft: 10,
                }}
            >
                <Image
                    source={{
                        uri: item.userImage ? item.userImage : 'https://randomuser.me/api/portraits/men/65.jpg'
                    }}
                    resizeMode="contain"
                    style={{
                        width: 30,
                        height: 30,
                        borderRadius: 25,
                        marginRight: 10
                    }}

                />
                <Text style={{ ...FONTS.h5, color: COLORS.black }}>{item.name}</Text>
            </TouchableOpacity>
        )
    }
    const onBlur = () => {
        setKey('');
        setFocus(false);
        Keyboard.dismiss();
    }
    const searchUser = (key) => {
        setKey(key);
        setSearchedUser(
            listUser.filter(i =>
                i.name.toLowerCase().includes(key.toLowerCase())
                || i.email.toLocaleString().toLowerCase().includes(key.toLowerCase())
            )
        );
    }

    const onFocus = () => {
        setFocus(true);
    }
    return (
        <SafeAreaView style={{
            flex: 1,
            backgroundColor: COLORS.background,
            paddingTop: 30
        }}>
            <View style={styles.page_header}>
                <TouchableOpacity
                    onPress={() => props.navigation.goBack()}
                    style={styles.btn_goBack}
                >
                    <Image
                        source={icons.icon_back}
                        style={styles.btn_icon}
                        resizeMode="contain"
                    />
                </TouchableOpacity>
                <Text style={{ ...FONTS.h3, color: COLORS.black }}>Tin nhắn mới</Text>

            </View>
            <View style={{
                paddingHorizontal: SIZES.padding,
            }}>
                <View style={styles.search_container}>
                    <TextInput
                        placeholder={'Tìm tên hoặc email'}
                        onChangeText={(text) => searchUser(text)}
                        defaultValue={key}
                        onFocus={onFocus}
                        placeholderTextColor={COLORS.grayblue}
                        style={styles.search_text_input}
                    />
                    <TouchableOpacity
                        onPress={() => onBlur()}
                        style={{ alignSelf: 'center' }}>
                        <Icon name={focus ? 'close' : 'search'} color={COLORS.primary} size={20} />
                    </TouchableOpacity>
                </View>
                <View style={{
                    marginTop: 10,

                }}>
                    {
                        focus ? (
                            <View>
                                <Text style={{ ...FONTS.body1, color: COLORS.grayblue }}>Kết quả tìm kiếm cho {key}:</Text>
                                {
                                    searchedUser ? (
                                        <FlatList
                                            data={searchedUser}
                                            renderItem={_renderItem}
                                            extraData
                                            showsVerticalScrollIndicator={false}
                                            keyExtractor={(item) => item.id}
                                        />
                                    ) : null
                                }
                            </View>
                        ) : (
                            <View>
                                <Text style={{ ...FONTS.body1, color: COLORS.grayblue }}>Gợi ý cho bạn</Text>
                                {
                                    loading ? (
                                        <ActivityIndicator size="large" color={COLORS.primary} animating={true} />
                                    ) : (
                                        <FlatList
                                            data={listUser}
                                            renderItem={renderItem}
                                            extraData
                                            showsVerticalScrollIndicator={false}
                                            keyExtractor={(item) => item.id}
                                        />
                                    )
                                }
                            </View>
                        )
                    }
                </View>
            </View>
        </SafeAreaView>
    )
}
export default NewChat;
const styles = StyleSheet.create({
    page_header: {
        flexDirection: 'row',
        paddingHorizontal: SIZES.padding,
        marginVertical: 10,
        alignItems: 'center',
    },
    btn_goBack: {
        borderWidth: 1,
        padding: 5,
        borderColor: COLORS.grayblue,
        borderRadius: 5,
        marginRight: 20
    },
    btn_icon: {
        width: 20,
        height: 20,
        tintColor: COLORS.black
    },
    search_container: {
        flexDirection: 'row',
        marginBottom: 20,
        borderRadius: 8,
        height: 50,
        backgroundColor: '#fff',
        borderColor: COLORS.grayblue,
        borderWidth: 1
    },
    search_text_input: {
        paddingHorizontal: 20,
        ...FONTS.body1,
        color: COLORS.black,
        width: SIZES.width * 0.85
    }
})