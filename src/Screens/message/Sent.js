import React from 'react';
import {
    View,
    Text,
    Image
} from 'react-native';
import { COLORS, FONTS } from '../../shared/constants';
import { formatTime } from '../../shared/utils';
const Sent = ({data}) => {
    const time = formatTime(data.createdAt.toDate());
    return (
        <View style={{
            marginVertical:5,
            alignSelf:'flex-end'
        }}>
            <View style={{
                backgroundColor:'#2675ec',
                maxWidth:220,
                alignItems:'center',
                justifyContent:'center',
                paddingHorizontal:20,
                paddingVertical:10,
                borderTopLeftRadius:25,
                borderTopRightRadius:25,
                borderBottomLeftRadius:25,
            }}>
                <Text style={{...FONTS.body1,color:COLORS.white}}>{data.message}</Text>
            </View>
            <Text style={{...FONTS.body2,color:COLORS.grayblue,alignSelf:'flex-end',marginTop:5}}>{time}</Text>

        </View>
    )
}
export default Sent;