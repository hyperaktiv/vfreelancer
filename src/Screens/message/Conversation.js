import React, { useState, useLayoutEffect } from 'react';
import {
    SafeAreaView,
    Text,
    View,
    Image,
    TouchableOpacity,
    KeyboardAvoidingView,
    TextInput,
    Animated,
    TouchableWithoutFeedback,
    Keyboard,
    FlatList,
    Platform
} from 'react-native';

import { COLORS, FONTS, icons, SIZES } from '../../shared/constants';
import Sent from './Sent';
import Received from './Received';
import * as firebase from 'firebase';


const Conversation = (props) => {
    const { senderName, chatId, image, senderId, whoSender } = props.route.params;
    const [animation] = useState(new Animated.Value(0));
    const [toggleOpen, setToggleOpen] = useState(null);
    const [heighInput, setHeighInput] = useState(60);


    // const {user} = useContext(AuthContext);
    const [inputMessage, setInputMessage] = useState("");
    const [messages, setMessages] = useState([]);



    const onSend = () => {
        Keyboard.dismiss();
        firebase.firestore()
            .collection('chats')
            .doc(chatId)
            .collection('messages')
            .add({
                createdAt: new Date(),
                message: inputMessage,
                senderName: firebase.auth().currentUser.displayName,
                senderId: firebase.auth().currentUser.uid,
                messageType: "text",
                senderImage: firebase.auth().currentUser.photoURL ? firebase.auth().currentUser.photoURL : 'https://randomuser.me/api/portraits/men/78.jpg'
            })
            .catch(e => console.log(e));
        if(whoSender == 'userTwo') {
            firebase.firestore()
            .collection('chats')
            .doc(chatId)
            .update({
                lastedMessage: inputMessage,
                user_one_image: firebase.auth().currentUser.photoURL,
                user_one_name: firebase.auth().currentUser.displayName
            });
        } else {
            firebase.firestore()
            .collection('chats')
            .doc(chatId)
            .update({
                lastedMessage: inputMessage,
                user_two_image: firebase.auth().currentUser.photoURL,
                user_two_name: firebase.auth().currentUser.displayName
            });
        }
       
        setInputMessage("");
    };
    useLayoutEffect(() => {

        const unsubscribe = firebase.firestore()
            .collection('chats')
            .doc(chatId)
            .collection('messages')
            .orderBy('createdAt', 'desc')
            .onSnapshot((snapshot) => setMessages(
                snapshot.docs.map(doc => ({
                    id: doc.id,
                    data: doc.data()
                }))

            ))

        return unsubscribe;


    }, [])

    // toggle icon plus
    const toggleMenu = () => {
        const toValue = toggleOpen ? 0 : 1;
        Animated.spring(animation, {
            toValue,
            friction: 5,
            useNativeDriver: true
        }).start();
        setToggleOpen(!toggleOpen);
    };
    const rotation = {
        transform: [
            {
                rotate: animation.interpolate({
                    inputRange: [0, 1],
                    outputRange: ['0deg', '45deg']
                })
            }
        ]
    }
    const galleryStyle = {
        transform: [
            {
                scale: animation
            },
            {
                translateY: animation.interpolate({
                    inputRange: [0, 1],
                    outputRange: [0, -70]
                })
            }
        ]
    }
    const renderItem = ({ item }) => (
        item.data.senderId === firebase.auth().currentUser.uid
            ? (<Sent data={item.data} key={item.id} />)
            : (<Received data={item.data} key={item.id} />)
    )
    return (
        <SafeAreaView style={{
            flex: 1,
            backgroundColor: COLORS.background,
        }}>
            <View style={{
                height: 60,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                paddingHorizontal: SIZES.padding,

            }}>

                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center'
                }}>
                    <TouchableOpacity
                        onPress={() => props.navigation.goBack()}
                        style={{
                            marginHorizontal: 10,
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}
                    >
                        <Image
                            source={icons.icon_back}
                            style={{
                                width: 20,
                                height: 20,
                                tintColor: COLORS.primary
                            }}
                            resizeMode="contain"
                        />
                    </TouchableOpacity>
                    <Image
                        source={{
                            uri: `${image}`
                        }}
                        style={{
                            width: 40,
                            height: 40,
                            borderRadius: 16
                        }}
                        resizeMode="contain"
                    />
                    <View style={{
                        marginHorizontal: 10
                    }}>
                        <Text style={{ ...FONTS.h5, color: COLORS.black }}>{senderName}</Text>
                        <Text style={{ ...FONTS.body2, color: COLORS.primary }}>Online</Text>
                    </View>

                </View>
                <Image
                    source={icons.icon_setting}
                    style={{
                        width: 20,
                        height: 20,
                        tintColor: COLORS.primary
                    }}
                    resizeMode="contain"
                />
            </View>
            <View style={{
                flex: 1,
                marginHorizontal: SIZES.padding,
                marginBottom: 60
            }}>
                <FlatList
                    inverted
                    renderItem={renderItem}
                    data={messages}
                    extraData
                />
            </View>
            <KeyboardAvoidingView
                keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}
                behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
                style={{
                    flex: 1,
                    flexDirection: 'row',
                    alignSelf: 'center',
                    alignItems: 'center',
                    backgroundColor: '#fff',
                    width: '90%',
                    position: 'absolute',
                    bottom: 30,
                    paddingHorizontal: 20,
                    paddingVertical: 10,
                    borderRadius: 30,
                    borderColor: COLORS.primary,
                    borderWidth: 1
                }} >
                <View>
                    <TouchableWithoutFeedback>
                        <Animated.View style={[galleryStyle, {
                            position: 'absolute',
                            width: 50,
                            height: 50,
                            borderRadius: 50,
                            alignItems: 'center',
                            justifyContent: 'center',
                            marginLeft: -15,
                            backgroundColor: COLORS.primary,
                            borderColor: COLORS.primary
                        }]}>
                            <Image
                                source={icons.icon_gallery}
                                style={{
                                    width: 25,
                                    height: 25,
                                    tintColor: COLORS.white,

                                }}
                            />
                        </Animated.View>
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback onPress={() => toggleMenu()}>
                        <Animated.View style={[rotation, {
                            // position: 'absolute'
                        }]}>
                            <Image
                                source={icons.icon_plus}
                                style={{
                                    width: 25,
                                    height: 25,
                                    tintColor: COLORS.primary,

                                }}
                            />
                        </Animated.View>
                    </TouchableWithoutFeedback>

                </View>

                <TextInput
                    value={inputMessage}
                    onContentSizeChange={(event) => setHeighInput(event.nativeEvent.contentSize.height + 20)}
                    multiline
                    style={{
                        color: COLORS.black,
                        ...FONTS.body1,
                        paddingHorizontal: 10,
                        flex: 1
                    }}
                    onSubmitEditing={onSend}
                    onChangeText={(text) => setInputMessage(text)}
                    placeholder="Nhập tin nhắn" />
                <TouchableOpacity onPress={onSend}>
                    <Image
                        source={icons.icon_send}
                        style={{
                            width: 25,
                            height: 25,
                            tintColor: COLORS.primary
                        }}
                        resizeMode="contain"
                    />
                </TouchableOpacity>
            </KeyboardAvoidingView>

        </SafeAreaView>


    )
}
export default Conversation;

