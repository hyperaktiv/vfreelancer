import React, { useEffect, useState } from 'react';
import {
    View,
    Text,
    Image,
    Animated,
    TouchableOpacity,
    StyleSheet
} from 'react-native';
import { COLORS, FONTS, icons } from '../../shared/constants';
import Swipeable from 'react-native-gesture-handler/Swipeable';
import * as firebase from 'firebase';
const MessageItem = (props) => {
    const { item, navigation } = props;
    const [chatMessages, setChatMessages] = useState([{
        "lastedMessage": item.lastedMessage,
    }]);
    const rightSwipe = (progress, dragX) => {
        const scale = dragX.interpolate({
            inputRange: [-100, 0],
            outputRange: [1, 0],
            extrapolate: 'clamp'
        });
        return (
            <TouchableOpacity
                style={{
                    shadowColor: "#000",
                    shadowOffset: {
                        width: 0,
                        height: 1,
                    },
                    shadowOpacity: 0.20,
                    shadowRadius: 1.41,
                    elevation: 2,
                    backgroundColor: '#fff',
                    justifyContent: 'center',
                    alignItems: 'center',
                    width: 80,
                    borderRadius: 8,
                    marginVertical: 8,
                    marginHorizontal: 5
                }}
                onPress={() => console.log('delete')}>
                <Animated.Image
                    source={icons.icon_trash}
                    style={{
                        transform: [{ scale: scale }],
                        width: 25,
                        height: 25,
                        tintColor: 'red'
                    }} />
            </TouchableOpacity>

        )
    }

    return (
        <Swipeable
            renderRightActions={rightSwipe}
        >
            <TouchableOpacity
                style={styles.container}
                onPress={() => navigation.navigate("Conversation", {
                    senderName: item.senderName,
                    image: item.senderImage ? item.senderImage : 'https://randomuser.me/api/portraits/men/78.jpg',
                    chatId: item.chatId,
                    senderId: item.senderId,
                    whoSender: item.whoSender
                })}>

                <View style={{
                    flexDirection: 'row'
                }}>
                    <Image
                        source={{
                            uri: item.senderImage ? item.senderImage : 'https://randomuser.me/api/portraits/men/78.jpg',
                        }}
                        resizeMode="contain"
                        style={{
                            width: 55,
                            height: 55,
                            borderRadius: 12
                        }}
                    />
                    <View style={styles.user_infor}>
                        <Text style={{ ...FONTS.h5, color: COLORS.black }}>{item.senderName}</Text>
                        <Text style={{ ...FONTS.body1, color: COLORS.text }}>{item.lastedMessage}</Text>
                        {/* /{chatMessages[0].lastedMessage ? chatMessages[0].lastedMessage : item.lastedMessage} */}
                    </View>
                </View>
            </TouchableOpacity>
        </Swipeable>
    )
}
export default MessageItem;
const styles = StyleSheet.create({
    container: {
        marginHorizontal: 3,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 20,
        paddingVertical: 10,
        backgroundColor: '#fff',
        borderRadius: 8,
        marginVertical: 8,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
    },
    user_infor: {
        marginLeft: 10,
        justifyContent: 'center',
        alignItems: 'flex-start'
    }
})