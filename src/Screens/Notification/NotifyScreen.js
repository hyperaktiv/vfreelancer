import React, { useEffect, useState } from 'react'
import {
   SafeAreaView,
   View,
   TouchableOpacity,
   Text,
   Image,
   FlatList
} from 'react-native';
import NotifyComponent from './NotifyComponent';
import { COLORS, SIZES, icons, FONTS } from '../../shared/constants';
import { auth, firestore } from '../../firebase';
import { relativeTime } from '../../shared/utils';

const NotifyScreen = (props) => {
   const [notifications, setNotifications] = useState(null);
   const getNotification = async () => {
      let array = [];
      await firestore.collection('notifications')
         .where('userId', '==', auth.currentUser.uid)
         .get()
         .then((query) => {
            query.forEach(doc => {
               const { userId, message, seen, created_at } = doc.data();
               array.push({
                  id: doc.id,
                  message,
                  created_at:relativeTime(created_at.toDate()),
                  seen
               })
            });
            setNotifications(array);
         })
   }
   useEffect(() => {
      getNotification();
   }, [])
   return (
      <SafeAreaView style={{
         flex: 1,
         backgroundColor: COLORS.background,
      }}>
         <View style={{
            flexDirection: 'row',
            paddingHorizontal: SIZES.padding,
            justifyContent: 'space-between',
            alignItems: 'center',
         }}>
            <TouchableOpacity
               onPress={() => props.navigation.goBack()}
               style={{
                  borderWidth: 1,
                  padding: 5,
                  borderColor: COLORS.grayblue,
                  borderRadius: 5
               }}
            >
               <Image
                  source={icons.icon_back}
                  style={{
                     width: 20,
                     height: 20,
                     tintColor: COLORS.black
                  }}
                  resizeMode="contain"
               />

            </TouchableOpacity>
            <Text style={{ ...FONTS.h3, color: COLORS.black }}>Thông báo</Text>
            <TouchableOpacity
               style={{
                  borderWidth: 1,
                  padding: 5,
                  borderColor: COLORS.primary,
                  borderRadius: 5
               }}
            >
               <Image
                  source={icons.icon_checked}
                  style={{
                     width: 20,
                     height: 20,
                     tintColor: COLORS.primary
                  }}
                  resizeMode="contain"
               />
            </TouchableOpacity>
         </View>
         {/* body */}
         <View style={{
            flex: 1
         }}>
            <FlatList
               data={notifications}
               extraData={true}
               keyExtractor={item => `${item.id}`}
               renderItem={({ item }) => <NotifyComponent notify={item.message} seen={item.seen} time={item.created_at} />}
            />
         </View>


      </SafeAreaView>
   )
}

export default NotifyScreen
