import React from 'react'
import { View, Image, TouchableOpacity } from 'react-native'
import { icons, COLORS, SIZES } from '../../shared/constants';
import CustomText from '../../shared/CustomText';
const NotifyComponent = ({ notify = '', time = '', seen = false }) => {
   return (
      <TouchableOpacity style={{
         marginHorizontal: SIZES.padding,
         flexDirection: 'row',
         marginBottom: 5
      }}>
         <View style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
         }}>
            {/* red dot */}
            {seen == false ? (<View style={{
               height: 8,
               width: 8,
               borderRadius: 4,
               backgroundColor: 'red'
            }}></View>) : null}


            {/* notification type */}
            <View style={{
               width: 40,
               height: 40,
               justifyContent: 'center',
               alignItems: 'center',
               backgroundColor: COLORS.primary,
               margin: 12,
               borderRadius: 12
            }}>
               <Image
                  style={{
                     width: 24,
                     height: 24,
                     tintColor: COLORS.white
                  }}
                  resizeMode="contain"
                  source={icons.icon_notification}
               />
            </View>
         </View>
         <View style={{
            flex: 7,
            paddingVertical: 5
         }}>
            <CustomText>{notify}</CustomText>
            <CustomText body2 style={{ color: COLORS.grayblue, paddingTop: 3 }}>{time}</CustomText>
         </View>
      </TouchableOpacity>
   )
}

export default NotifyComponent
