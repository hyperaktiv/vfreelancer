import React from 'react';
import Provider from './src/navigators/Provider';
const App = () => {
  return (
    <Provider />
  )
}
export default App;